#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 20:03:21 2020

@author: cis
"""

import os
path = '/media/cis/2f57014e-7d77-4058-9059-42b5c8d054c8/home/cis/$$$ExtractedDigitalFiles-20200204T092814Z-001/$$$ExtractedDigitalFiles'
files = os.listdir(path)

for file in files:
    if ".pdf" not in file:
        os.remove(os.path.join(path, file))
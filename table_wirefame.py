#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 16:45:36 2020

@author: cis
"""

import pdftotext
import re
abc = ""
r = re.compile(r"^\s+", re.MULTILINE)
with open("/home/cis/Documents/10207 VOSS s1.PDF", "rb") as file:
    pdf = pdftotext.PDF(file)
    # pass
for i in range(1, 7):
    abc += pdf[i]
    
table = [{"Line":3, "Oper":"FRONT BUMPER", "Code":"", "Description":"", "Part Number":"", "Qty":"", "Extended":"", "Labor":"", "Paint":""},
         {"Line":4, "Oper":"S01", "Code":"Repl", "Description":"A/M NSF RT Headlamp assy w/o auto hi-beam", "Part Number":"SU2503151N", "Qty":1, "Extended":273.0, "Labor":0.4, "Paint":0.0},
         {"Line":5, "Oper":"S01", "Code":"", "Description":"Aim Headlamps", "Part Number":"", "Qty":0, "Extended":0.0, "Labor":0.5, "Paint":0.0},
         {"Line":6, "Oper":"S01", "Code":"R&I", "Description":"LT Headlamp assy w/o auto hi-beam", "Part Number":"84001AL01A", "Qty":0, "Extended":0.0, "Labor":0.4, "Paint":0.0},
         {"Line":7, "Oper":"GRILLE", "Code":"", "Description":"", "Part Number":"", "Qty":"", "Extended":"", "Labor":"", "Paint":""},
         {"Line":8, "Oper":"", "Code":"Repl", "Description":"A/M Grille Legacy chrome", "Part Number":"SU1200162", "Qty":1, "Extended":188.0, "Labor":"Incl.", "Paint":0.0},
         {"Line":9, "Oper":"S01", "Code":"Repl", "Description":"EMBLEM", "Part Number":"SU1200162", "Qty":1, "Extended":63.88, "Labor":0.2, "Paint":0.0},
         {"Line":10, "Oper":"FRONT DOOR", "Code":"", "Description":"", "Part Number":"", "Qty":"", "Extended":"", "Labor":"", "Paint":""},
         {"Line":11, "Oper":"", "Code":"Repl", "Description":"LT Upper molding", "Part Number":"61186AL01A", "Qty":1, "Extended":40.57, "Labor":0.3, "Paint":0.0},
         {"Line":12, "Oper":"", "Code":"Repl", "Description":"RT Upper molding", "Part Number":"61186AL00A", "Qty":1, "Extended":40.57, "Labor":0.3, "Paint":0.0},
         {"Line":13, "Oper":"S01", "Code":"Rpr", "Description":"RT Door shell Legacy", "Part Number":"60009AL10A9P", "Qty":0, "Extended":0.0, "Labor":2.0, "Paint":2.0},
         {"Line":14, "Oper":"S01", "Code":"Rpr", "Description":"LT Door shell Legacy", "Part Number":"60009AL11A9P", "Qty":0, "Extended":0.0, "Labor":4.0, "Paint":2.0},
         {"Line":15, "Oper":"S01", "Code":"", "Description":"Overlap Major Adj. Panel", "Part Number":"", "Qty":0, "Extended":0.0, "Labor":0.0, "Paint":-0.4},
         {"Line":16, "Oper":"S01", "Code":"R&I", "Description":"RT Door w'strip", "Part Number":"63511AL00A", "Qty":0, "Extended":0.0, "Labor":0.7, "Paint":0.0},
         {"Line":17, "Oper":"S01", "Code":"R&I", "Description":"LT Door w'strip", "Part Number":"63511AL01A", "Qty":0, "Extended":0.0, "Labor":0.7, "Paint":0.0},
         {"Line":18, "Oper":"S01", "Code":"R&I", "Description":"RT Lower w'strip", "Part Number":"63511AL10A", "Qty":0, "Extended":0.0, "Labor":0.3, "Paint":0.0},
         {"Line":19, "Oper":"S01", "Code":"R&I", "Description":"LT Lower w'strip", "Part Number":"63511AL10A", "Qty":0, "Extended":0.0, "Labor":0.3, "Paint":0.0},
         {"Line":20, "Oper":"S01", "Code":"Repl", "Description":"RT Belt molding", "Part Number":"61280AL00B", "Qty":1, "Extended":44.95, "Labor":0.3, "Paint":0.0},
         {"Line":21, "Oper":"S01", "Code":"Repl", "Description":"LT Belt molding", "Part Number":"61280AL01B", "Qty":1, "Extended":44.95, "Labor":0.3, "Paint":0.0},
         {"Line":22, "Oper":"S01", "Code":"Repl", "Description":"Molding kit Subaru accessory lapis blue", "Part Number":"J101SAL802E3", "Qty":1, "Extended":249.95, "Labor":1.2, "Paint":1.6},
         {"Line":23, "Oper":"S01", "Code":"R&I", "Description":"RT R&I mirror", "Part Number":"91036AL21B", "Qty":0, "Extended":0.0, "Labor":0.3, "Paint":0.0},
         {"Line":24, "Oper":"S01", "Code":"R&I", "Description":"LT R&I mirror", "Part Number":"91036AL20B", "Qty":0, "Extended":0.0, "Labor":0.3, "Paint":0.0},
         {"Line":25, "Oper":"S01", "Code":"Repl", "Description":"RT Signal lamp", "Part Number":"84401AJ000", "Qty":1, "Extended":92.33, "Labor":0.0, "Paint":0.0},
         {"Line":26, "Oper":"S01", "Code":"Repl", "Description":"LT Signal lamp", "Part Number":"84401AJ010", "Qty":1, "Extended":92.33, "Labor":0.0, "Paint":0.0},
         {"Line":27, "Oper":"S01", "Code":"R&I", "Description":"RT Door glass Subaru", "Part Number":"61011AL00B", "Qty":0, "Extended":0.0, "Labor":0.4, "Paint":0.0},
         {"Line":28, "Oper":"S01", "Code":"R&I", "Description":"LT Door glass Subaru", "Part Number":"61011AL01B", "Qty":0, "Extended":0.0, "Labor":0.4, "Paint":0.0},
         {"Line":29, "Oper":"S01", "Code":"R&I", "Description":"RT Fixed glass Subaru", "Part Number":"61011AL20A", "Qty":0, "Extended":0.0, "Labor":0.3, "Paint":0.0},
         {"Line":30, "Oper":"S01", "Code":"R&I", "Description":"LT Fixed glass Subaru", "Part Number":"61011AL21A", "Qty":0, "Extended":0.0, "Labor":0.3, "Paint":0.0},
         {"Line":31, "Oper":"S01", "Code":"R&I", "Description":"RT Glass w'strip", "Part Number":"61284AL000", "Qty":0, "Extended":0.0, "Labor":"Incl.", "Paint":0.0},
         {"Line":32, "Oper":"S01", "Code":"R&I", "Description":"LT Glass w'strip", "Part Number":"61284AL010", "Qty":0, "Extended":0.0, "Labor":"Incl.", "Paint":0.0},{"Line":31, "Oper":"S01", "Code":"R&I", "Description":"RT Glass w'strip", "Part Number":"61284AL000", "Qty":0, "Extended":0.0, "Labor":"Incl.", "Paint":0.0},
         {"Line":33, "Oper":"S01", "Code":"R&I", "Description":"RT Front guide", "Part Number":"61240AL10B", "Qty":0, "Extended":0.0, "Labor":0.1, "Paint":0.0}]

























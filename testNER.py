#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 19:52:55 2020

@author: cis
"""

import spacy
from spacy.matcher import Matcher
# from spacy.attrs import POS

nlp = spacy.load("en_core_web_sm")

matcher = Matcher(nlp.vocab)

pattern_1 = [{"LOWER": "workfile"}, {"LOWER": "id"}, {"IS_PUNCT": True}, {"TEXT": {"REGEX": "^\w"}}]
pattern_2 = [{"LOWER": "job"}, {"LOWER": "number"}, {"IS_PUNCT": True}, {"TEXT": {"REGEX": "[-\w]*"}}]
# pattern 2 yet to be implemented......
pattern_3 = [{"LOWER": "insured"}, {"IS_PUNCT": True}, {"DEP": "appos"}, {"IS_PUNCT": True}, {"DEP": "compound"}]
pattern_4 = [{"LOWER": "owner"}, {"IS_PUNCT": True}, {"DEP": "appos"}, {"IS_PUNCT": True}, {"DEP": "compound"}]
pattern_5 = [{"LOWER": "written"}, {"LOWER": "by"}, {"IS_PUNCT": True}, {"TEXT": {"REGEX": "\w*"}}]
# pattern 5 yet to be implemented......
pattern_6 = [{"LOWER": "policy"}, {"DEP": "appos"}, {"IS_PUNCT": True}, {"TEXT": {"REGEX": "^\w"}}]
pattern_7 = [{"LOWER": "claim"}, {"TEXT": {"REGEX": "^\W"}}, {"IS_PUNCT": True}, {"TEXT": {"REGEX": "^[\w-]+"}}]
pattern_8 = [{"LOWER": "vin"}, {"IS_PUNCT": True}, {"TEXT": {"REGEX": "^\w"}}]
pattern_9 = [{"LOWER": "license"}, {"IS_PUNCT": True}, {"TEXT": {"REGEX": "^\w"}}]
pattern_10 = [{"LOWER": "odometer"}, {"IS_PUNCT": True}, {"TEXT": {"REGEX": "^\d*\,\d+|\d+"}}]
pattern_11 = [{"LOWER": "exterior"}, {"POS": "PROPN"}, {"IS_PUNCT": True}, {"POS": "PROPN"}]
pattern_12 = [{"LOWER": "state"}, {"IS_PUNCT": True}, {"TEXT": {"REGEX": "^\w"}}]
pattern_13 = [{"LOWER": "adjuster"}, {"IS_PUNCT": True}, {"TEXT": {"REGEX": "^\w*"}}]
pattern_14 = [{"LOWER": "point"}, {"LOWER": "of"}, {"LOWER": "impact"}, {"IS_PUNCT": True}, {"TEXT": {"REGEX": "^\w*"}}]
# ptn = "\d*\.\d+|\d+"    (\d+(?:-\d+)*)$   ^(\d+(?:-\d+|\d\.\d+)*)$
'''
abc = "Francisco Enriquez"
p5 regex = re.findall(r"^\w+\s\w+\,\s\w+|\w+\s\w+", abc)
>>> ['Francisco Enriquez']
'''

# matcher.add("WorkfileID:", None, pattern_1)
matcher.add("jobnumber:", None, pattern_2)
matcher.add("insured:", None, pattern_3)
matcher.add("owner:", None, pattern_4)
matcher.add("writtenBy:", None, pattern_5)
matcher.add("policy:", None, pattern_6)
matcher.add("claim:", None, pattern_7)
matcher.add("vin:", None, pattern_8)
matcher.add("license:", None, pattern_9)
matcher.add("odometer:", None, pattern_10)
matcher.add("exteriorColor:", None, pattern_11)
matcher.add("state:", None, pattern_12)
matcher.add("adjuster:", None, pattern_13)
matcher.add("pointofimpact:", None, pattern_14)

doc = ''' ADVANCED APPRAISAL SERVICE -
MARLBORO,MA

Workfile ID: APUJ2V1V

2 MOUNT ROYAL AVE
MARLBOROUGH, MA 01752

Phone: (508) 485-2212, FAX:(508) 485-8691

For:

COMMERCE INSURANCE
MAPFRE | Insurance Claims Questions Call

Supplement of Record 1 with Summary

Job Number: 16-33242

Owner: WHITNEY, PAMELA Job Number: 19-11710
Written By: Francisco Enriquez, 15860

Adjuster: MARCHESSAULT, MARY, (800) 221-1605 Business

Insured: CUEVAS, YAZMIN Policy #: GYZ882 Claim #: A0PWT88-NYRVK9

Type of Loss: AD - Other Date of Loss: 12/28/2018 1:00 PM Days to Repair: 0

Point of Impact: 07 Left Rear

Owner: Inspection Location: Repair Facility:

WHITNEY, PAMELA 290 AUTO BODY / CUSTOMS 290 AUTO BODY / CUSTOMS

6 BOURNE ST #2 1 STOWELL AVE 1 STOWELL AVE

WORCESTER, MA 01606-0000 WORCESTER, MA 01606 WORCESTER, MA 01606

(831) 821-9767 Other Repair Facility (508) 340-4977 Fax

VEHICLE

2018 BUIC Encore Preferred FWD 4D UTV 4-1.4L Turbocharged Gasoline Electronic Fuel Injection WHITE

VIN: KL4CJASB5JB536599 Production Date: Interior Color:

License: 7CHR80 Odometer: 14,655 Exterior Color: WHITE

State: MA Condition:

TRANSMISSION Air Conditioning Stereo Hands Free Device

Automatic Transmission Intermittent Wipers Search/Seek ROOF

Overdrive Tilt Wheel Auxiliary Audio Connection Luggage/Roof Rack

POWER Cruise Control Satellite Radio SEATS

Power Steering Rear Defogger SAFETY Cloth Seats

Power Brakes Keyless Entry Drivers Side Air Bag Bucket Seats

Power Windows Alarm Passenger Air Bag Reclining/Lounge Seats

Power Locks Message Center Anti-Lock Brakes (4) WHEELS

Power Mirrors Steering Wheel Touch Controls 4 Wheel Disc Brakes Aluminum/Alloy Wheels

Heated Mirrors Rear Window Wiper Traction Control PAINT

Power Driver Seat Telescopic Wheel Stability Control Clear Coat Paint

DECOR Backup Camera w/Parking Sensors Front Side Impact Air Bags OTHER

Dual Mirrors RADIO Head/Curtain Air Bags Signal Integrated Mirrors

Privacy Glass AM Radio Rear Side Impact Air Bags

CONVENIENCE FM Radio Communications System '''

doc = nlp(doc)
# import pdb; pdb.set_trace()
matches = matcher(doc)

for match_id, start, end in matches:
    # import pdb; pdb.set_trace()
    string_id = nlp.vocab.strings[match_id]  # Get string representation
    span = doc[start:end]  # The matched span
    print(match_id, string_id, start, end, span.text)

# for token in doc:
#     print(token.text, token.dep_, token.head.text, token.head.pos_,
#             [child for child in token.children])
    
# matches = matcher(doc)

# for match_id, start, end in matches:
#     string_id = nlp.vocab.strings[match_id]  # Get string representation
#     span = doc[start:end]  # The matched span
#     # listing[string_id] = span.text
#     print(match_id, string_id, start, end, span.text)
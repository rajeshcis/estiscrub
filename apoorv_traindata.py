#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 13:11:58 2020

@author: aman
"""


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 15:38:55 2020

@author: cis
"""


LABEL = ["InsCo", "IndApprCo", "Estimate Type", "Workfile_id"]

# training data
# Note: If you're using an existing model, make sure to mix in examples of
# other entity types that spaCy correctly recognized before. Otherwise, your
# model might learn the new type, but "forget" what it previously knew.
# https://explosion.ai/blog/pseudo-rehearsal-catastrophic-forgetting
TRAIN_DATA = [
    (
         'GEICO GEICO RI/MA EMAIL SUPPLEMENTS TO: R8ADSUPPMARI@GEICO.COM Visit us at geico.com 300 Cross Point Parkway Getzville, NY 14068 Phone: (508) 455-7332 Claim #: 0546447810101026-01 Fax: (855) 890-4788 Workfile ID: 4b996034 Supplement of Record 2 with Summary Written By: MICHAEL HARNOIS, License Number: 016038, 2/12/2018 3:49:41 PM Adjuster: HARNOIS, MICHAEL, (508) 455-7332 Cellular Insured: Iris Moore Owner Policy #: 4413613136 Claim #: 0546447810101026-01 Type of Loss: Collision Date of Loss: 01/10/2018 08:10 AM Days to Repair: 15 Point of Impact: 12 Front Deductible: 500.00 Owner (Insured): Inspection Location: Appraiser Information: Repair Facility: Iris Moore 290 autobody mharnois@geico.com 290 AUTO BODY INC 108 Reservoir St 290 autobody (508) 455-7332 73 WEST BOYLSTON DR Shrewsbury, MA 01545 1 Stowell Ave WORCESTER, MA 01606 (508) 845-9077 Evening Worcester, MA 01606 300510528 Federal ID Other (508) 363-2902 Day VEHICLE 2014 MAZD 3 Grand Touring Automatic 4D H/B 4-2.5L Gasoline Gasoline Direct Injection brown VIN: JM1BM1M32E1135487 Production Date: Interior Color: License: 19774 Odometer: 41378 Exterior Color: brown State: MA Condition: TRANSMISSION Intermittent Wipers Auxiliary Audio Connection Reclining/Lounge Seats Automatic Transmission Tilt Wheel Premium Radio Leather Seats POWER Cruise Control Satellite Radio Heated Seats Power Steering Rear Defogger SAFETY WHEELS Power Brakes Keyless Entry Drivers Side Air Bag Aluminum/Alloy Wheels Power Windows Steering Wheel Touch Controls Passenger Air Bag PAINT Power Locks Rear Window Wiper Anti-Lock Brakes (4) Clear Coat Paint Power Mirrors Telescopic Wheel 4 Wheel Disc Brakes OTHER Heated Mirrors Climate Control Front Side Impact Air Bags Fog Lamps Power Driver Seat Navigation System Head/Curtain Air Bags Traction Control DECOR Backup Camera w/Parking Sensors Hands Free Device Stability Control Dual Mirrors RADIO Blind Spot Detection Rear Spoiler Tinted Glass AM Radio Heads Up Display Signal Integrated Mirrors Console/Storage FM Radio ROOF Xenon Headlamps Overhead Console Stereo Electric Glass Sunroof Power Trunk/Gate Release 2/12/2018 3:50:21 PM 119288 | 1.6.09.03151 Page 1 ',
         {"entities": [(0, 150, LABEL[0]), (180, 199, LABEL[0]), (200, 221, LABEL[3]), (222, 257, LABEL[2])]},
    ),
    (
         "LM GENERAL INSURANCE COMPANY Westborough Claim Office SUPPLEMENTS ONLY CALL 508-948-5156 ALL OTHER QUESTIONS CALL 800-251-7447 114 TURNPIKE RD Westborough, MA 01581 Claim #: 037808846-0001 Phone: (800) 251-7447 Workfile ID: 3e44d3b9 Supplement of Record 1 with Summary Written By: LYNN JOHNS, License Number: 015318, 7/25/2018 3:18:12 PM Adjuster: BRUDZIENSKI, JESSICA, (800) 252-5730 Business Insured: NANCY Owner Policy #: MARI Claim #: 037808846-0001 VILLAVICENCIO Type of Loss: COMP - Date of Loss: 07/08/2018 12:00 AM Days to Repair: 2 Comprehensive Coverage Point of Impact: 12 Front Deductible: 1000.00 Owner (Insured): Inspection Location: Appraiser Information: Repair Facility: NANCY VILLAVICENCIO 290 AUTO BODY lynn.johns@libertymutual.com 290 AUTO BODY 28 ELM CT 1 STOWELL AVE (508) 948-5156 1 STOWELL AVE MILLBURY, MA 01527-2614 WORCESTER, MA 01606 WORCESTER, MA 01606 (508) 762-2263 Evening Repair Facility (508) 363-2902 Business nlee1975@yahoo.com (508) 363-2902 Business (508) 796-1642 Fax 300510528 Federal ID GM@290AUTOBODY.COM VEHICLE 2011 NISS Maxima S w/Continuously Variable Transmission 4D SED 6-3.5L Gasoline SMPI MAROON VIN: 1N4AA5APXBC847660 Production Date: Interior Color: License: 5PJW70 Odometer: UNK Exterior Color: MAROON State: MA Condition: Good TRANSMISSION Overhead Console AM Radio ROOF Automatic Transmission CONVENIENCE FM Radio Electric Glass Sunroof POWER Air Conditioning Stereo SEATS Power Steering Intermittent Wipers Search/Seek Cloth Seats Power Brakes Tilt Wheel Auxiliary Audio Connection Bucket Seats Power Windows Cruise Control CD Changer/Stacker WHEELS Power Locks Rear Defogger SAFETY Aluminum/Alloy Wheels Power Mirrors Keyless Entry Drivers Side Air Bag PAINT Power Driver Seat Alarm Passenger Air Bag Clear Coat Paint Power Passenger Seat Message Center Anti-Lock Brakes (4) OTHER DECOR Steering Wheel Touch Controls 4 Wheel Disc Brakes Fog Lamps Dual Mirrors Telescopic Wheel Front Side Impact Air Bags Traction Control Tinted Glass Climate Control Head/Curtain Air Bags Stability Control Console/Storage RADIO Hands Free Device Power Trunk/Gate Release 7/25/2018 3:19:22 PM 130702 | 1.7.03.03220 Page 1 ",
         {"entities": [(0, 164, LABEL[0]), (189, 210, LABEL[0]), (211, 232, LABEL[3]), (233, 268, LABEL[2])]},
    ),
    (
         "LM GENERAL INSURANCE COMPANY Albany Claim Office RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW 175 Berkeley Street Boston, MA 02116 Phone: (800) 251-7447 Claim #: 039603226-0002 Fax: (800) 507-3707 Workfile ID: 3852b03e Supplement of Record 1 with Summary Written By: NATHAN WIKTOR, License Number: 016334, 5/3/2019 9:16:26 AM Adjuster: SIMLER, NICHOLAS, (800) 252-5730 Business Insured: HAROLD SIMON Owner Policy #: Claim #: 039603226-0002 Type of Loss: Liability Date of Loss: 03/29/2019 12:00 AM Days to Repair: 2 Point of Impact: 06 Rear Deductible: Owner (Claimant): Inspection Location: Appraiser Information: Repair Facility: JUSTIN SABOO Liberty Mutual nathan.wiktor@libertymutual.com 290 Auto Body 854 WEST BOYLSTON ST MA 01606 (413) 244-3561 73 W. Boylston DR APT 2 Desk Worcester, MA 01606 Worcester, MA 01606 claims@290autobody.com (978) 518-6932 Cellular VEHICLE 2017 JEEP Compass Latitude 4WD *Ltd Avail* 4D UTV 4-2.4L Gasoline Sequential MPI BLUE VIN: 1C4NJDEB2HD110850 Production Date: 08/2016 Interior Color: License: 6CE491 Odometer: 51192 Exterior Color: BLUE State: MA Condition: Fair TRANSMISSION Air Conditioning Auxiliary Audio Connection Bucket Seats Automatic Transmission Intermittent Wipers Satellite Radio Reclining/Lounge Seats 4 Wheel Drive Tilt Wheel SAFETY Leather Seats POWER Cruise Control Drivers Side Air Bag Heated Seats Power Steering Rear Defogger Passenger Air Bag WHEELS Power Brakes Keyless Entry Anti-Lock Brakes (4) Aluminum/Alloy Wheels Power Windows Steering Wheel Touch Controls 4 Wheel Disc Brakes PAINT Power Locks Rear Window Wiper Traction Control Clear Coat Paint Power Mirrors Navigation System Stability Control OTHER Heated Mirrors Backup Camera w/Parking Sensors Front Side Impact Air Bags Fog Lamps Power Driver Seat Remote Starter Head/Curtain Air Bags Rear Spoiler DECOR RADIO Hands Free Device California Emissions Dual Mirrors AM Radio ROOF TRUCK Privacy Glass FM Radio Luggage/Roof Rack Rear Step Bumper Console/Storage Stereo Electric Glass Sunroof CONVENIENCE Search/Seek SEATS 5/3/2019 9:23:29 AM 130917 | 1.7.06.04050 Page 1 ",
         {"entities": [(0, 157, LABEL[0]), (182, 202, LABEL[0]), (202, 223, LABEL[3]), (224, 259, LABEL[2])]},
    ),
    (
         "ADVANCED APPRAISAL SERVICE - Workfile ID: fc003e65 MARLBORO,MA 2 MOUNT ROYAL AVE MARLBOROUGH, MA 01752 Phone: (508) 485-2212, FAX:(508) 485-8691 For: COMMERCE INSURANCE MAPFRE | Insurance Claims Questions Call Phone: (800) 221-1605 Estimate of Record Owner: GONZALEZ, STEVEN Job Number: 18-27171 Written By: NEAL TROMBLY, 15473 Adjuster: OSTERGARD, ROBIN, (800) 221-1605 Business Insured: GONZALEZ, STEVEN Policy #: BNS203 Claim #: A0PMVY6-NPJAH7 Type of Loss: AC - unknown Date of Loss: 7/26/2018 1:00 PM Days to Repair: 0 Point of Impact: 05 Right Rear Owner: Inspection Location: Repair Facility: GONZALEZ, STEVEN 290 AUTO BODY / CUSTOMS 290 AUTO BODY / CUSTOMS 135 ELM ST APT B 1 STOWELL AVE 1 STOWELL AVE MILLBURY, MA 01527 WORCESTER, MA 01606 WORCESTER, MA 01606 (774) 253-9553 Business Repair Facility (508) 340-4977 Fax VEHICLE 2011 NISS Rogue S AWD 4D UTV 4-2.5L Gasoline Direct Injection PURPLE VIN: JN8AS5MV2BW282986 Production Date: Interior Color: License: 24ND88 Odometer: 84,000 Exterior Color: PURPLE State: MA Condition: Good TRANSMISSION Console/Storage FM Radio Front Side Impact Air Bags Automatic Transmission CONVENIENCE Stereo Head/Curtain Air Bags 4 Wheel Drive Air Conditioning Search/Seek Positraction POWER Intermittent Wipers CD Player SEATS Power Steering Tilt Wheel Auxiliary Audio Connection Cloth Seats Power Brakes Cruise Control SAFETY Bucket Seats Power Windows Rear Defogger Drivers Side Air Bag WHEELS Power Locks Keyless Entry Passenger Air Bag Wheel Covers Power Mirrors Alarm Anti-Lock Brakes (4) PAINT DECOR Rear Window Wiper 4 Wheel Disc Brakes Clear Coat Paint Dual Mirrors RADIO Traction Control OTHER Body Side Moldings AM Radio Stability Control Rear Spoiler 8/10/2018 3:38:00 PM 052031 Page 1 ",
         {"entities": [(0, 26, LABEL[0]), (29, 50, LABEL[3]), (51, 144, LABEL[0]), (145, 231, LABEL[1]), (232, 250, LABEL[2])]},
    ),
    (
         "GARY BUXTON AND ASSOCIATES Workfile ID: 4c232a7f P.O. Box 5898 HOLLISTON, MA 01746 Phone: (508) 353-2803, FAX:(508) 429-3301 garybuxton@comcast.net For: GARY BUXTON AND ASSOCIATES ACADIA INSURANCE Phone: (888) 786-1170 Estimate of Record Owner: 290 AUTO BODY INC Job Number: 73874 Written By: Gary Buxton, 08911 Adjuster: RANO, BRIAN Insured: 290 AUTO BODY INC Policy #: Claim #: PC28018 Type of Loss: Collision Date of Loss: 6/7/2018 12:00 AM Days to Repair: 3 Point of Impact: 02 Right Front Pillar (Right Side) Owner: Inspection Location: Repair Facility: 290 AUTO BODY INC 290 AUTO BODY 290 AUTO BODY 73 WEST BOYLSTON DRIVE 35 LAGRANGE STREET 35 LAGRANGE STREET WORCESTER, MA 01606 WORCESTER, MA 01610 WORCESTER, MA 01610 (774) 239-0652 Business Repair Facility (508) 363-2902 Business (508) 363-2902 Business (508) 340-4977 Fax VEHICLE 2008 BMW 3 Series 328xi AWD 4D SED 6-3.0L Gasoline MPI BLUE VIN: WBAVC93558K038748 Production Date: Interior Color: License: 1070A Odometer: UNK Exterior Color: BLUE State: MA Condition: Good TRANSMISSION Wood Interior Trim AM Radio SEATS Overdrive CONVENIENCE FM Radio Bucket Seats 6 Speed Transmission Air Conditioning Stereo Leather Seats 4 Wheel Drive Intermittent Wipers Search/Seek WHEELS POWER Tilt Wheel CD Player Aluminum/Alloy Wheels Power Steering Cruise Control Auxiliary Audio Connection PAINT Power Brakes Rear Defogger SAFETY Clear Coat Paint Power Windows Keyless Entry Drivers Side Air Bag OTHER Power Locks Alarm Passenger Air Bag Fog Lamps Power Mirrors Message Center Anti-Lock Brakes (4) Traction Control Heated Mirrors Steering Wheel Touch Controls 4 Wheel Disc Brakes Stability Control DECOR Telescopic Wheel Front Side Impact Air Bags Xenon Headlamps Dual Mirrors Climate Control Head/Curtain Air Bags Headlamp Washers Tinted Glass Intelligent Cruise ROOF Power Trunk/Gate Release 10/24/2018 10:34:47 AM 032469 Page 1",
         {"entities": [(0, 26, LABEL[0]), (27, 48, LABEL[3]), (49, 147, LABEL[0]), (148, 218, LABEL[1]), (219, 237, LABEL[2])]},
    ),
    (
         "TAFT APPRAISAL SERVICE 148 UXBRIDGE RD MENDON, MA 01756 Phone: (508) 473-2846, FAX:(508) 473-0903",
         {"entities": [(0, 22, LABEL[0]), (23, 45, LABEL[2]), (47, 55, LABEL[3]), (56, 77, LABEL[4]),
                       (79, 97, LABEL[5])]},
    ),
    (
         "OCCIDENTAL/WILSHIRE CORAL SPRINGS, FL Phone: (800) 525-7486, FAX:(919) 834-0855",
         {"entities": [(0, 33, LABEL[0]), (38, 59, LABEL[4]), (61, 79, LABEL[5])]},
    ),
    (
         "SAFETY INSURANCE COMPANY SAFETY INSURANCE COMPANY-BOSTON Phone: (800) 951-2100",
         {"entities": [(0, 49, LABEL[0]), (50, 56, LABEL[3]), (57, 78, LABEL[4])]},
    ),
    (
         "ALLSTATE INSURANCE COMPANY New England Auto 55 Capital Blvd. 3rd Floor Rocky Hill, CT 06067 Phone: (888) 233-2675",
         {"entities": [(0, 26, LABEL[0]), (27, 81, LABEL[2]), (83, 90, LABEL[3]), (92, 113, LABEL[4])]},
    ),
    (
         "TRAVELERS New England Claim Department - 452 FOR SUPPLEMENTS PLEASE CALL (888)-299-7456 PROMPT 2 P.O. Box 1450 Middleboro, MA 02344 Phone: (800) 422-3340 Fax: (877) 786-5584",
         {"entities": [(0, 44, LABEL[0]), (68, 87, LABEL[4]), (97, 110, LABEL[9]), (111, 121, LABEL[1]),
                       (123, 131, LABEL[3]), (132, 153, LABEL[4]), (154, 173, LABEL[6])]},
    ),
    (
         "LIBERTY MUTUAL INSURANCE COMPANY Lake Mary Claim Office RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW 255 Primera Boulevard Suite 300 Lake Mary, FL 32746 Phone: (800) 637-0757",
         {"entities": [(0, 32, LABEL[0]), (33, 55, LABEL[2]), (106, 147, LABEL[2]), (149, 157, LABEL[3]),
                       (158, 179, LABEL[4])]},
    ),
    (
         "SAFECO INSURANCE COMPANY OF AMERICA PLSA-MA RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW Sharon.Sandman@Safeco.com 62 MAPLE AVENUE KEENE, NH 03431 Phone: (508) 309-9901",
         {"entities": [(0, 35, LABEL[0]), (94, 119, LABEL[7]), (120, 141, LABEL[2]), (143, 151, LABEL[3]),
                       (152, 173, LABEL[4])]},
    ),
    (
         "GEICO GEICO RI/MA EMAIL SUPPLEMENTS TO: R8ADSUPPMARI@GEICO.COM Visit us at geico.com 300 Cross Point Parkway Getzville, NY 14068 Phone: (401) 215-6202",
         {"entities": [(0, 17, LABEL[0]), (40, 62, LABEL[7]), (75, 84, LABEL[8]), (85, 118, LABEL[2]),
                       (120, 128, LABEL[3]), (129, 150, LABEL[4])]},
    ),
    (
         "LIBERTY MUTUAL INSURANCE COMPANY Westborough Claim Office RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW 114 Turnpike Road Westborough, MA 01581 Phone: (800) 251-7447",
         {"entities": [(0, 32, LABEL[0]), (33, 57, LABEL[2]), (108, 137, LABEL[2]), (139, 146, LABEL[3]),
                       (148, 168, LABEL[4])]},
    ),
    (
         "AMICA MUTUAL INSURANCE COMPANY CENTRAL MASSACHUSETTS OFFICE PO Box 9690 Providence, RI 02940 Phone: (888) 702-6422 Fax: (866) 381-3239",
         {"entities": [(0, 30, LABEL[0]), (31, 59, LABEL[1]), (60, 71, LABEL[9]), (84, 92, LABEL[3]),
                       (93, 114, LABEL[4]), (115, 134, LABEL[5])]},
    ),
    (
         "ARBELLA MUTUAL INSURANCE COMPANY 1100 CROWN COLONY DR PO BOX 699225, QUINCY, MA 02269-9225 (800) 272-3552 Fax: (617) 773-4760",
         {"entities": [(0, 32, LABEL[0]), (33, 53, LABEL[2]), (54, 67, LABEL[9]), (69, 75, LABEL[1]),
                       (77, 90, LABEL[3]), (91, 105, LABEL[4]), (106, 125, LABEL[5])]},
    ),
    (
         "LIBERTY MUTUAL INSURANCE COMPANY Westborough Claim Office RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW 114 Turnpike Road Westborough, MA 01581 Phone: (800) 251-7447",
         {"entities": [(0, 32, LABEL[0]), (33, 53, LABEL[2]), (54, 67, LABEL[9]), (69, 75, LABEL[1]),
                       (77, 90, LABEL[3]), (91, 105, LABEL[4]), (106, 125, LABEL[5])]},
    )
]

# LABEL = ["ORG", "CITY", "STREET", "STATE", "PHONE", "FAX", "THEME", "EMAIL", "WEBSITE", "POBOX"]
#         ["0",     "1",     "2",     "3",      "4",   "5",    "6",     "7",      "8",      "9"]

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 02:24:12 2020

@author: aman
"""

LABEL = ["InsCo", "Estimate Type", "Workfile_id"]

# training data
# Note: If you're using an existing model, make sure to mix in examples of
# other entity types that spaCy correctly recognized before. Otherwise, your
# model might learn the new type, but "forget" what it previously knew.
# https://explosion.ai/blog/pseudo-rehearsal-catastrophic-forgetting
TRAIN_DATA = [
    (
    'MAPFRE INSURANCE Commerce Insurance Company - MA For Claims Questions: Call 1-800-221-1605 11 Gore Rd Webster, MA 01570 Claim #: RRPK20-1 Phone: (800) 221-1605 Workfile ID: 62e1246c Estimate of Record Written By: MATTHEW P GODIN, License Number: 14333, 7/2/2019 11:58:43 AM Adjuster: SKOG, CASEY Insured: BRIAN W HANSEN Owner Policy #: DBW574 Claim #: RRPK20-1 Type of Loss: Collision Date of Loss: 06/30/2019 09:00 AM Days to Repair: 0 Point of Impact: 19 All Over Deductible: Owner (Insured): Inspe',
    {'entities': [(0, 17, LABEL[0]), (160, 181, LABEL[2]), (182, 273, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance Claims Questions call 800 221 1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Webster, MA 01570 Claim #: A0R39RG-PPKHX0 Phone: (800) 221-1605 Workfile ID: 6552ae5f Estimate of Record Written By: DAVID RASZKA, License Number: 16401, 7/15/2019 3:54:00 PM Adjuster: YOUNG, AMANDA, (800) 221-1605 x15391 Business Insured: ALEXIS GARCIA Owner Policy #: JGT034 Claim #: A0R39RG-PPKHX0 Type of Loss: Collision Date of Loss: 07/03/2019 12:00 AM Days to R',
    {'entities': [(0, 19, LABEL[0]), (196, 217, LABEL[2]), (218, 326, LABEL[1])]},
    ),
    (
    'LM GENERAL INSURANCE COMPANY Lake Mary Claim Office SUPPLEMENTS ONLY CALL 508-948-7000 ALL OTHER QUESTIONS CALL 800-251-7447 255 Primera Boulevard, Suite 300 Lake Mary, FL 32746 Phone: (800) 637-0757 Claim #: 040315140-0001 Fax: (877) 202-4607 Workfile ID: 4effd64c Estimate of Record Written By: SHAUN CHAPLIN, License Number: 016136, 7/25/2019 11:51:52 AM Adjuster: RODRIGUEZ, MARIA, (800) 637-0757 x7276061 Business Insured: NANCY MUNOZ Owner Policy #: MARI Claim #: 040315140-0001 Type of Loss: C',
    {'entities': [(0, 29, LABEL[0]), (244, 265, LABEL[2]), (266, 357, LABEL[1])]},
    ),
    (
    'METLIFE AUTO & HOME Auto Adjusting Unit - MA MA License #10729 P.O. Box 6040 Scranton, PA 18506 Phone: (800) 854-6011 x4736 Claim #: SLK57294 1 Fax: (866) 958-1610 Workfile ID: 68bafd5b Estimate of Record Written By: ROBERT FOSTER, License Number: 10729, 8/2/2019 9:49:04 AM Adjuster: Aebly, Bill, (800) 854-6011 x4723 Business Insured: JASON KRAKU Owner Policy #: Claim #: SLK57294 1 Type of Loss: Comprehensive Date of Loss: 11/21/2018 05:47 PM Days to Repair: 19 Point of Impact: 02 Right Front Pi',
    {'entities': [(0, 20, LABEL[0]), (164, 185, LABEL[2]), (186, 274, LABEL[1])]},
    ),
    (
    'ADVANCED APPRAISAL SERVICE - Workfile ID: 1a057cfe MARLBORO,MA 2 MOUNT ROYAL AVE MARLBOROUGH, MA 01752 Phone: (508) 485-2212, FAX:(508) 485-8691 For: MAPFRE INSURANCE Commerce Insurance Company - MA Phone: (800) 221-1605 Estimate of Record Owner: NEMES, AMIE Job Number: 19-25854 Written By: JOSEPH MOODY, 15363 Adjuster: ALKHAMISI, MARI, (800) 221-1605 Evening Insured: NEMES, AMIE Policy #: GCQ557 Claim #: RTNP92-1 Type of Loss: Collision Date of Loss: 7/24/2019 10:30 AM Days to Repair: 0 Point o',
    {'entities': [(60, 63, LABEL[0]), (29, 50, LABEL[2]), (221, 256, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA/RI Request a Supplement at Partners.Geico.com Visit Us Online At GEICO.COM 300 Crosspoint Parkway Getzville, NY 14068 Phone: (508) 958-1843 Claim #: 0604314140101028-01 Fax: (516) 213-7460 Workfile ID: 9d796de1 Estimate of Record Written By: ROBERT LYNCH, License Number: 016573, 8/16/2019 8:59:04 AM Adjuster: Lynch, Robert, (774) 479-8834 Business Insured: Naja Smith Owner Policy #: 4503378640 Claim #: 0604314140101028-01 Type of Loss: Comprehensive Date of Loss: 07/23/2019 07:30 ',
    {'entities': [(0, 6, LABEL[0]), (204, 225, LABEL[2]), (226, 315, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA/RI For Supplement Requests Copy The Link Below: Partners.geico.com 300 Crosspoint Parkway Getzville, NY 14068 Phone: (866) 345-0278 Claim #: 0583879090101017-01 Fax: (716) 898-0542 Workfile ID: 01b4db27 Estimate of Record Written By: ADAM EASTTY, License Number: 015878, 8/24/2019 11:41:37 AM Adjuster: ABBZ Insured: Tianyu Zhu Owner Policy #: 4471468761 Claim #: 0583879090101017-01 Type of Loss: Other Date of Loss: 08/06/2019 05:45 PM Days to Repair: 2 Point of Impact: 06 Rear Dedu',
    {'entities': [(0, 6, LABEL[0]), (196, 217, LABEL[2]), (218, 307, LABEL[1])]},
    ),
    (
    'MAPFRE INSURANCE Commerce Insurance Company - MA For Claims Questions: Call 1-800-221-1605 CT License# 800016803 11 Gore Rd Webster, MA 01570 Claim #: RVXW69-1 Phone: (800) 221-1605 Workfile ID: 499c730b Estimate of Record Written By: MATTHEW LUCIAN, License Number: 16553, 9/9/2019 11:10:07 AM Adjuster: RICHARDSON, JOHN, (877) 372-9836 Evening Insured: MATTHEW DALY Owner Policy #: GQY871 Claim #: RVXW69-1 Type of Loss: Collision Date of Loss: 08/30/2019 05:02 AM Days to Repair: 2 Point of Impact',
    {'entities': [(0, 17, LABEL[0]), (182, 203, LABEL[2]), (204, 294, LABEL[1])]},
    ),
    (
    'MAPFRE INSURANCE Commerce Insurance Company - MA For Claims Questions: Call 1-800-221-1605 11 Gore Rd Webster, MA 01570 Claim #: RWNA03-1 Phone: (800) 221-1605 Workfile ID: 3409db4c Estimate of Record Written By: ANDRE GUILMETTE, License Number: 16593, 9/17/2019 11:15:04 AM Adjuster: KENNEY, SARA, (800) 221-1605 x15222 Business Insured: YESMARY JIMENEZ Owner Policy #: HZM350 Claim #: RWNA03-1 Type of Loss: APO - All Other Date of Loss: 09/16/2019 05:20 AM Days to Repair: 0 Point of Impact: 17 Le',
    {'entities': [(0, 17, LABEL[0]), (160, 181, LABEL[2]), (182, 274, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA /RI Request a supplement at partners.geico.com 300 CROSSPOINT PARKWAY GETZVILLE, NY 14068 Phone: (617) 435-9984 Claim #: 0548107000101074-01 Fax: (855) 351-1481 Workfile ID: 1eecc508 Estimate of Record Written By: AMANDA AU, License Number: 016351, 9/26/2019 2:48:06 PM Adjuster: AI93 Insured: Alexander Jimenez Owner Policy #: 4416183327 Claim #: 0548107000101074-01 Type of Loss: Comprehensive Date of Loss: 09/26/2019 09:00 AM Days to Repair: 21 Point of Impact: 29 Vandalized Deduc',
    {'entities': [(0, 6, LABEL[0]), (176, 197, LABEL[2]), (198, 231, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA/RI Request a Supplement at Partners.Geico.com Visit Us Online At GEICO.COM 300 Crosspoint Parkway Getzville, NY 14068 Phone: (508) 958-1843 Claim #: 0666364240000001-01 Fax: (516) 213-7460 Workfile ID: 976ec849 Estimate of Record Written By: ROBERT LYNCH, License Number: 016573, 11/1/2019 8:48:45 AM Adjuster: Lynch, Robert, (774) 479-8834 Business Insured: Alicia Lucena Owner Policy #: 4601997978 Claim #: 0666364240000001-01 Type of Loss: Collision Date of Loss: 10/24/2019 06:45 P',
    {'entities': [(0, 6, LABEL[0]), (204, 225, LABEL[2]), (226, 315, LABEL[1])]}
    ),
    (
    'TRAVELERS Auto Express Claim Center (244) Email Supplements: supplementrequest@travelers.com By Phone: (888) 299-7456 P.O. BOX 430 Buffalo, NY 14240 Claim #: IBF3347001 Phone: (877) 411-0768 Workfile ID: 156c7f13 Estimate of Record Written By: MARK MACCARTNEY, License Number: 015708, 10/4/2019 9:02:12 AM Adjuster: MEB Insured: AMY BISSONNETTE Owner Policy #: PT5010A6002999562321 Claim #: IBF3347001 Type of Loss: Collision Date of Loss: 09/18/2019 12:00 AM Days to Repair: 4 Point of Impact: 12 Fr',
    {'entities': [(0, 10, LABEL[0]), (191, 212, LABEL[2]), (213, 331, LABEL[1])]},
    ),
    (
    'AMICA MUTUAL INSURANCE COMPANY S. E. MASS REGIONAL Claim #: 60003680250-1-1 Workfile ID: 2998218c Estimate of Record Written By: WILLIAM HOMMEL, License Number: 12048, 11/1/2019 11:14:39 AM Adjuster: FITZPATRICK, KIMBERLY, (800) 592-6422 x47204 Business Insured: Domenic Palmerino Owner Policy #: 4001009567 Claim #: 60003680250-1-1 Type of Loss: Collision Date of Loss: 10/24/2019 05:00 PM Days to Repair: 3 Point of Impact: 08 Left Qtr Post Deductible: 300.00 (Left Side) Owner (Insured): Inspectio',
    {'entities': [(0, 31, LABEL[0]), (76, 97, LABEL[2]), (98, 189, LABEL[1])]},
    ),
    (
    'GEICO GEICO RI/MA Request a supplement at partners.geico.com Visit us at geico.com 300 Cross Point Parkway Getzville, NY 14068 Phone: (401) 316-6392 Claim #: 0461464840101043-01 Fax: (844) 891-9108 Workfile ID: 00ba8115 Estimate of Record Written By: NICHOLAS MATTHEWS, 10/24/2019 11:22:24 AM Adjuster: ABT7 Insured: Stanley Kellman Owner Policy #: 4289923890 Claim #: 0461464840101043-01 Type of Loss: Comprehensive Date of Loss: 10/17/2019 05:35 AM Days to Repair: 3 Point of Impact: 13 Rollover De',
    {'entities': [(0, 6, LABEL[0]), (198, 219, LABEL[2]), (220, 292, LABEL[1])]},
    ),
    (
    'MAPFRE INSURANCE Commerce Insurance Company - MA For Claims Questions: Call 1-800-221-1605 11 Gore Rd Webster, MA 01570 Claim #: RRPJ75-1 Phone: (800) 221-1605 Workfile ID: e86af97b Estimate of Record Written By: DAVID RASZKA, License Number: 16401, 11/8/2019 9:12:40 AM Adjuster: MILLS, JACOB, (800) 221-1605 x15317 Business Insured: JAMIE SHANNON Owner Policy #: GZD502 Claim #: RRPJ75-1 Type of Loss: Collision Date of Loss: 06/30/2019 09:30 AM Days to Repair: 0 Point of Impact: 12 Front Deductib',
    {'entities': [(0, 17, LABEL[0]), (160, 181, LABEL[2]), (182, 270, LABEL[1])]},
    ),
    (
    'THE HANOVER INSURANCE COMPANY The Hanover Insurance Group-01 Worcester Claim Office 440 Lincoln Street Office # 800-628-0250, Worcester, MA 01653 Phone: (508) 335-7218 Claim #: 19-00-488254-1-1 Fax: (508) 635-8690 Workfile ID: d95a4953 Estimate of Record Written By: RYAN WALSH, License Number: 15990, 7/18/2019 10:29:50 AM Adjuster: PICKERING, JOSHUA, (800) 628-0250 Evening Insured: Brian Carroll Owner Policy #: Claim #: 19-00-488254-1-1 Type of Loss: Liability Date of Loss: 06/30/2019 02:15 PM D',
    {'entities': [(0, 30, LABEL[0]), (214, 235, LABEL[2]), (236, 323, LABEL[1])]},
    ),
    (
    'ADVANCED APPRAISAL SERVICE - Workfile ID: 5553da07 MARLBORO,MA 2 MOUNT ROYAL AVE MARLBOROUGH, MA 01752 Phone: (508) 485-2212, FAX:(508) 485-8691 For: MAPFRE INSURANCE Commerce Insurance Company - MA Phone: (800) 221-1605 Estimate of Record Owner: LAYTE, JAMES Job Number: 19-27606 Written By: NORM GOSSELIN, 13209 Adjuster: JOHNSON, KARIN, (800) 221-1605 Evening Insured: LAYTE, JAMES Policy #: CYD348 Claim #: RVTH30-1 Type of Loss: Collision Date of Loss: 8/24/2019 10:00 AM Days to Repair: 0 Point',
    {'entities': [(60, 63, LABEL[0]), (29, 50, LABEL[2]), (221, 257, LABEL[1])]},
    ),
    (
    'GEICO MA Toll Free 800-841-3000 EMAIL SUPPLEMENTS TO: R8ADSUPPNE@GEICO.COM VISIT US ONLINE AT GEICO.COM 300 Crosspoint Pkwy Claim #: 0369101350101037-01 Getzville, NY 14068 Workfile ID: 8da243a4 Estimate of Record Written By: AIMEE WALIGORA, License Number: 15475, 12/31/2014 8:08:58 AM Adjuster: Waligora, Aimee Insured: Alex Banegas Owner Policy #: 4159650037 Claim #: 0369101350101037-01 Type of Loss: Collision Date of Loss: 12/16/2014 07:00 PM Days to Repair: 10 Point of Impact: 11 Left Front D',
    {'entities': [(0, 6, LABEL[0]), (173, 194, LABEL[2]), (195, 286, LABEL[1])]},
    ),
    (
    'GEICO MA Toll Free 800-841-3000 EMAIL SUPPLEMENTS TO: R8ADSUPPNE@GEICO.COM VISIT US ONLINE AT GEICO.COM 300 Crosspoint Pkwy Claim #: 0515438100101021-01 Getzville, NY 14068 Workfile ID: 692dcaa1 Estimate of Record Written By: AIMEE WALIGORA, License Number: 15475, 3/19/2015 11:15:11 AM Adjuster: Waligora, Aimee Insured: Anthony Dand Owner Policy #: 4366984575 Claim #: 0515438100101021-01 Type of Loss: Collision Date of Loss: 03/11/2015 09:00 PM Days to Repair: 7 Point of Impact: 11 Left Front De',
    {'entities': [(0, 6, LABEL[0]), (173, 194, LABEL[2]), (195, 286, LABEL[1])]},
    ),
    (
    'ALLSTATE INSURANCE COMPANY New England Auto 55 Capital Blvd. 3rd Floor Rocky Hill, CT 06067 Claim #: 000360006852B02 Phone: (800) 726-2235 Workfile ID: 198e690e Estimate of Record Written By: JAMES CRANDALL, License Number: 015856, 3/5/2015 1:26:18 PM Adjuster: CRANDALL, JAMES Insured: TEFTA HOXHALLARI Owner Policy #: Claim #: 000360006852B02 Type of Loss: Liability Date of Loss: 03/03/2015 12:00 PM Days to Repair: 4 Point of Impact: 01 Right Front Deductible: Owner (Claimant): Inspection Locati',
    {'entities': [(0, 27, LABEL[0]), (139, 160, LABEL[2]), (161, 275, LABEL[1])]},
    ),
    (
    'ALLSTATE INSURANCE COMPANY New England Auto 55 Capital Blvd. 3rd Floor Rocky Hill, CT 06067 Claim #: 000360006852B02 Phone: (800) 726-2235 Workfile ID: 198e690e Estimate of Record Written By: JAMES CRANDALL, License Number: 015856, 3/5/2015 1:26:18 PM Adjuster: CRANDALL, JAMES Insured: TEFTA HOXHALLARI Owner Policy #: Claim #: 000360006852B02 Type of Loss: Liability Date of Loss: 03/03/2015 12:00 PM Days to Repair: 4 Point of Impact: 01 Right Front Deductible: Owner (Claimant): Inspection Locati',
    {'entities': [(0, 27, LABEL[0]), (139, 160, LABEL[2]), (161, 275, LABEL[1])]},
    ),
    (
    'SAFECO INSURANCE COMPANY PLSA RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW Claim #: 183069595002-201 Phone: (413) 351-5180 Workfile ID: 5aeb6e95 Estimate of Record Written By: AMY BERNARD, License Number: MA014374, 6/16/2015 1:52:48 PM Adjuster: FRANK BLOUGH Insured: JENNIFER ASANTE Owner Policy #: Claim #: 183069595002-201 Type of Loss: APD - Auto Prop Date of Loss: 06/14/2015 09:40 AM Days to Repair: 5 DMG Point of Impact: 06 Rear Deductible: Owner (Claimant): Inspection Location: Apprais',
    {'entities': [(0, 80, LABEL[0]), (128, 149, LABEL[2]), (150, 183, LABEL[1])]},
    ),
    (
    'AMICA MUTUAL INSURANCE COMPANY S. E. MASS REGIONAL Claim #: 60002175737-1-1 Workfile ID: fd279c55 Estimate of Record Written By: WILLIAM HOMMEL, License Number: 12048, 6/24/2015 1:13:24 PM Adjuster: RAPOZA, MELISSA, (800) 592-6422 Day Insured: Daniel Yi Owner Policy #: 4509061986 Claim #: 60002175737-1-1 Type of Loss: Collision Date of Loss: 05/22/2015 04:15 PM Days to Repair: 9 Point of Impact: 04 Right Qtr Post Deductible: 1000.00 (Right Side) Owner (Insured): Inspection Location: Appraiser In',
    {'entities': [(0, 31, LABEL[0]), (76, 97, LABEL[2]), (98, 136, LABEL[1])]},
    ),
    (
    'AMICA MUTUAL INSURANCE COMPANY CENTRAL MASSACHUSETTS OFFICE PO Box 9690 Providence, RI 02940 Phone: (888) 702-6422 Claim #: 60002173657-2-1 Fax: (866) 381-3239 Workfile ID: 204801e3 Estimate of Record Written By: WILLIAM HOMMEL, License Number: 12048, 6/23/2015 9:50:52 AM Adjuster: BICKFORD, KARYN, (888) 702-6422 x46320 Day Insured: Ayla Lari Owner Policy #: Claim #: 60002173657-2-1 Type of Loss: PD - Property Date of Loss: 06/20/2015 05:15 PM Days to Repair: 11 Damage Point of Impact: 04 Right ',
    {'entities': [(0, 60, LABEL[0]), (160, 181, LABEL[2]), (182, 272, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0MXXNX-HWWRX3 Webster, MA 01570 Workfile ID: 7b49f38a Estimate of Record Written By: MICHAEL COLLINS, License Number: 16026, 5/27/2015 8:52:05 AM Adjuster: JENNIFER SPECTER, (800) 221-1605 x15382 Business Insured: NGUYEN VU Owner Policy #: BVL609 Claim #: A0MXXNX-HWWRX3 Type of Loss: Collision Date of Loss: 04/28/2015 12:00 PM Days to Repair: 0 Poin',
    {'entities': [(0, 19, LABEL[0]), (181, 202, LABEL[2]), (203, 294, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0MZVNT-JAHWT6 Webster, MA 01570 Workfile ID: 6d72dba2 Estimate of Record Written By: WILLIAM LANNON, License Number: 015814, 7/7/2015 4:28:11 PM Adjuster: CATHERINE CARDIN, (800) 221-1605 x15592 Business Insured: KEVIN KARANJA Owner Policy #: DGQ481 MA Claim #: A0MZVNT-JAHWT6 Type of Loss: Collision Date of Loss: 07/07/2015 12:00 PM Days to Repair: ',
    {'entities': [(0, 19, LABEL[0]), (181, 202, LABEL[2]), (203, 241, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0MY73P-HXJPN7 Webster, MA 01570 Workfile ID: f6f4f1aa Estimate of Record Written By: MICHAEL COLLINS, License Number: 16026, 7/2/2015 10:12:20 AM Adjuster: BICKERTON, COURTNEY, (800) 221-1605 Business Insured: NGUYEN VU Owner Policy #: Claim #: A0MY73P-HXJPN7 Type of Loss: Collision Date of Loss: 05/19/2015 12:00 PM Days to Repair: 0 Point of Impact',
    {'entities': [(0, 19, LABEL[0]), (181, 202, LABEL[2]), (203, 294, LABEL[1])]},
    ),
    (
    'USAA 1010 Please visit us @ USAA.com PO Box 33490 San Antonio, TX 78265 Claim #: 007478896000000010002 Phone: (800) 531-8722 Workfile ID: d7e3aa61 Estimate of Record Written By: RAYMOND CHAPIN, License Number: 014258, 9/25/2015 11:20:49 AM Adjuster: USAA, Auto Claims, (800) 531-8722 Business Insured: CHRISTINE WHITE Owner Policy #: 007478896 Claim #: 007478896000000010002 Type of Loss: Liability Date of Loss: 09/23/2015 12:00 AM Days to Repair: 7 Point of Impact: 10 Left Front Pillar Deductible:',
    {'entities': [(0, 5, LABEL[0]), (125, 146, LABEL[2]), (147, 239, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA/RI Email Supplements to:R8ADSuppMARI@geico.com Visit Us Online At GEICO.COM 300 Crosspoint Parkway Getzville, NY 14068 Phone: (603) 234-8466 Claim #: 0336950650101018-01 Fax: (855) 884-9187 Workfile ID: 94351c71 Estimate of Record Written By: ADAM EASTTY, License Number: 015878, 9/18/2015 11:52:44 AM Adjuster: Eastty, Adam, (603) 234-8466 Business Insured: William Gill Owner Policy #: 4114951132 Claim #: 0336950650101018-01 Type of Loss: Collision Date of Loss: 09/07/2015 01:00 AM',
    {'entities': [(0, 6, LABEL[0]), (205, 226, LABEL[2]), (227, 316, LABEL[1])]},
    ),
    (
    'GEICO GEICO- Northern New England EMAIL SUPPLEMENTS TO: R8ADSUPPMARI@GEICO.COM VISIT US ONLINE AT GEICO.COM P.O. BOX 792 STERLING, MA 01564 Phone: (800) 841-3000 Claim #: 0292540620101024-01 Fax: (855) 275-1772 Workfile ID: db5e23b3 Estimate of Record Written By: ROBERT BARRETT, License Number: 15703, 5/29/2014 10:40:57 AM Adjuster: Barrett, Robert, (413) 588-2095 Cellular Insured: JOSEPH Policy #: 4054911021 Claim #: 0292540620101024-01 REGENSBURGER Type of Loss: Collision Date of Loss: 05/19/2', 
    {'entities': [(0, 6, LABEL[0]), (211, 232, LABEL[2]), (233, 324, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0N3925-JKHAV9 Webster, MA 01570 Workfile ID: 6a405366 Estimate of Record Written By: ANTHONY BONAVITA, License Number: 10975, 10/21/2015 7:48:54 AM Adjuster: BEGIN, DENISE, (800) 221-1605 Business Insured: EDWIN CARTAGENA Owner Policy #: Claim #: A0N3925-JKHAV9 Type of Loss: Collision Date of Loss: 09/28/2015 12:00 PM Days to Repair: 0 Point of Impa'
    {'entities': [(0, 19, LABEL[0]), (181, 202, LABEL[2]), (203, 296, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0N5A61-JNPKN9 Webster, MA 01570 Workfile ID: b584ab8a Estimate of Record Written By: ANTHONY BONAVITA, License Number: 10975, 11/28/2015 11:31:10 AM Adjuster: WATERS, WOODROW, (800) 221-1605 Business Insured: JENNIFER GARCIA Owner Policy #: Claim #: A0N5A61-JNPKN9 Type of Loss: Collision Date of Loss: 11/21/2015 12:00 PM Days to Repair: 0 Point of I'
    {'entities': [(0, 19, LABEL[0]), (181, 202, LABEL[2]), (203, 297, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0N11Z6-JCJHC7 Webster, MA 01570 Workfile ID: 4a3a4215 Estimate of Record Written By: PAUL GEOTIS, License Number: 2685, 9/17/2015 11:52:54 AM Adjuster: LINDSAY O'HARA, (800) 221-1605 x15503 Business Insured: GINELLE PAYNE Owner Policy #: BQV792 Claim #: A0N11Z6-JCJHC7 Type of Loss: Collision Date of Loss: 07/28/2015 12:00 PM Days to Repair: 0 Point of Impact'
    {'entities': [(0, 19, LABEL[0]), (172, 193, LABEL[2]), (194, 281, LABEL[1])]},
    ),
    (
    'SAFECO INSURANCE COMPANY OF AMERICA PLSA-MA RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW Sharon.Sandman@Safeco.com 62 MAPLE AVENUE KEENE, NH 03431 Claim #: 384786116039-205 Phone: (508) 309-9901 Workfile ID: 3eac7749 Estimate of Record Written By: SHARON SANDMAN, License Number: 13593, 2/10/2016 7:49:57 AM Adjuster: NANCY L STAINES, (856) 435-2883 Business Insured: LORDINA APPIAH Owner Policy #: Claim #: 384786116039-205 Type of Loss: APD - Auto Prop Date of Loss: 12/11/2015 12:00 PM Days t'
    {'entities': [(0, 36, LABEL[0]), (200, 221, LABEL[2]), (222, 312, LABEL[1])]},
    ),
    (
    'AMICA MUTUAL INSURANCE COMPANY S. E. MASS REGIONAL PO Box 9690 Providence, RI 02940 Phone: (800) 592-6422 Claim #: 60002322104-1-1 Fax: (888) 999-8656 Workfile ID: 273fb16f Estimate of Record Written By: GARY LEDERER, License Number: 007629, 12/7/2015 8:05:55 AM Adjuster: CRUZ, VERONIKA Insured: Owner Policy #: Claim #: 60002322104-1-1 Type of Loss: PD - Property Date of Loss: 11/28/2015 10:29 AM Days to Repair: 0 Damage Point of Impact: 07 Left Rear Deductible: Owner (Claimant): Inspection Loca'
    {'entities': [(0, 31, LABEL[0]), (151, 172, LABEL[2]), (173, 262, LABEL[1])]},
    ),
    (
    'SAFECO INSURANCE COMPANY PLSA RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW AMY.BERNARD@SAFECO.COM P.O. Box 515097 LOS ANGELES, CA 90051 Phone: (413) 351-5180 Claim #: 391757316039-201 Fax: (888) 268-8840 Workfile ID: b09b1554 Estimate of Record Written By: AMY BERNARD, License Number: MA014374, 1/6/2016 3:57:46 PM Adjuster: KHAMSAVANH T PHIATHEP Insured: CRYSTAL DECKER Owner Policy #: K2916254 Claim #: 391757316039-201 Type of Loss: Collision Date of Loss: 01/02/2015 08:00 AM Days to Repair'
    {'entities': [(0, 80, LABEL[0]), (209, 230, LABEL[2]), (231, 335, LABEL[1])]},
    ),
    (
    'ALLSTATE INSURANCE COMPANY New England Auto 55 Capital Blvd. 3rd Floor Rocky Hill, CT 06067 Claim #: 000399064880D01 Phone: (888) 233-2675 Workfile ID: 91073849 Estimate of Record Written By: TODD HOWARTH, License Number: 016099, 1/22/2016 9:44:49 AM Adjuster: HOWARTH, TODD Insured: SELVIN PAGAN Owner Policy #: 000925573846 Claim #: 000399064880D01 Type of Loss: Collision Date of Loss: 01/20/2016 12:00 PM Days to Repair: 7 Point of Impact: 07 Left Rear Deductible: 1000.00 Owner (Insured): Inspec'
    {'entities': [(0, 27, LABEL[0]), (139, 160, LABEL[2]), (161, 250, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance Claims Questions call 800 221 1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0N71H9-JRMHC6 Webster, MA 01570 Workfile ID: 2ef85e0f Estimate of Record Written By: CHAD WHITCRAFT, License Number: 15227, 1/18/2016 9:48:20 AM Adjuster: PAULO REGO, (800) 221-1605 x15230 Business Insured: DIMITROULA Owner Policy #: Claim #: A0N71H9-JRMHC6 NIFOROS Type of Loss: Collision Date of Loss: 01/11/2016 12:00 PM Days to Repair: 0 Point of Impact:'
    {'entities': [(0, 19, LABEL[0]), (174, 195, LABEL[2]), (196, 286, LABEL[1])]},
    ),
    (
    'SAFECO INSURANCE COMPANY PLSA RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW AMY.BERNARD@SAFECO.COM P.O. Box 515097 LOS ANGELES, CA 90051 Phone: (413) 351-5180 Claim #: 340257716034-202 Fax: (888) 268-8840 Workfile ID: 981cfc41 Estimate of Record Written By: AMY BERNARD, License Number: MA014374, 2/24/2016 3:09:38 PM Adjuster: JESSICA HOOPER Insured: TANIA MONTANEZ Owner Policy #: K2773269 Claim #: 340257716034-202 Type of Loss: Collision Date of Loss: 02/19/2016 12:30 PM Days to Repair: 3 Po'
    {'entities': [(0, 80, LABEL[0]), (209, 230, LABEL[2]), (231, 264, LABEL[1])]},
    ),
    (
    'SAFECO INSURANCE COMPANY OF AMERICA PLSA-MA RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW Sharon.Sandman@Safeco.com 62 MAPLE AVENUE KEENE, NH 03431 Claim #: 567661995047-202 Phone: (508) 309-9901 Workfile ID: aeb77399 Estimate of Record Written By: SHARON SANDMAN, License Number: 13593, 8/5/2015 11:31:15 AM Adjuster: JESSICA LYNN Insured: TANIA MONTANEZ Owner Policy #: K2773269 Claim #: 567661995047-202 Type of Loss: Comprehensive Date of Loss: 02/02/2015 12:01 AM Days to Repair: 5 Point of '
    {'entities': [(0, 36, LABEL[0]), (200, 221, LABEL[2]), (222, 312, LABEL[1])]},
    ),
    (
    'ALLSTATE INSURANCE COMPANY New England Auto 55 Capital Blvd. 3rd Floor Rocky Hill, Cl- 06067 Claim #: 000400882668H0i Phone: (800) 726-2235 Workfile ID: d649d264 Estimate of Record Written JAMES CMNDALL, License Number: 015856, 2lLIl20l6 10:56:31 AM Adjuster: CMNDALL, JAMES Insured: CHRISTOPHER FOX Owner Policy #: 000925481412 Claim #: 000400882668H01 Type of Loss: Collision Date of Loss: 02105/20t6 12:00 PM Days to Repair: 3 Point of Impact: 11 Left Front Deductible: 500.00 Owner (Insured): Loc'
    {'entities': [(0, 27, LABEL[0]), (140, 161, LABEL[2]), (162, 272, LABEL[1])]},
    ),
    (
    'CITIZENS INS CO OF AMERICA The Hanover Insurance Group-01 Worcester Claim Office Office # 800-628-0250 440 Lincoln St. WORCESTER, MA 01653 Phone: (508) 751-2190 Claim #: 15-00-735253-1-1 Fax: (508) 926-4970 Workfile ID: 73a07628 Estimate of Record Written By: JOE STRANIERI, License Number: 006890, 4/8/2016 8:34:42 AM Adjuster: CRONIN, JOSEPH Insured: Daniel Bondzie Owner Policy #: APNA359613 Claim #: 15-00-735253-1-1 Type of Loss: Collision Date of Loss: 04/04/2016 12:00 AM Days to Repair: 0 Poi'
    {'entities': [(0, 27, LABEL[0]), (207, 228, LABEL[2]), (229, 318, LABEL[1])]},
    ),
    (
    'TRAVELERS New England Claim Department-244 For Supplement Please Call 888-299-7456 Prompt 2 P.O.Box 4947 Orlando, FL 32802 Phone: (877) 411-0768 Claim #: L1T9442001 Fax: (877) 786-5584 Workfile ID: 2ab01515 Estimate of Record Written By: MIKE MARGALETTA, License Number: 013926, 7/19/2016 11:32:06 AM Adjuster: Vazquez, Michelle, (407) 388-7076 Business Insured: KENNETH REED Owner Policy #: PT5010V9948371332321 Claim #: L1T9442001 Type of Loss: Collision Date of Loss: 07/01/2016 02:00 PM Days to R'
    {'entities': [(0, 10, LABEL[0]), (185, 206, LABEL[2]), (207, 300, LABEL[1])]},
    ),
    (
    'LM GENERAL INSURANCE COMPANY Irving Claim Office RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW 2120 West Walnut Hill Lane Suite 200 Irving, TX 75016 Phone: (800) 313-8844 Claim #: 034373654-0001 Fax: (866) 864-6081 Workfile ID: a7380834 Estimate of Record Written By: RYAN HEYBECK, License Number: 015392, 9/23/2016 11:01:12 AM Adjuster: DZIURZYNSKI, PATSY Insured: AMIE NEMES Owner Policy #: OEMC Claim #: 034373654-0001 Type of Loss: COLL - Collision Date of Loss: 09/02/2016 12:00 AM Days to R'
    {'entities': [(0, 29, LABEL[0]), (219, 240, LABEL[2]), (241, 372, LABEL[1])]},
    ),
    (
    'SAFECO INSURANCE COMPANY OF AMERICA PLSA-MA RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW Sharon.Sandman@Safeco.com P.O. BOX 515097 LOS ANGELES, CA 90051 Claim #: 693520836036-203 Phone: (508) 309-9901 Workfile ID: a1a19810 Estimate of Record Written By: SHARON SANDMAN, License Number: 13593, 10/17/2016 11:43:39 AM Adjuster: MATTHEW CARR, (856) 435-2918 Business Insured: EIZLA YOUSSEF Owner Policy #: K2897126 Claim #: 693520836036-203 Type of Loss: Collision Date of Loss: 10/03/2016 01:00 PM'
    {'entities': [(0, 36, LABEL[0]), (206, 227, LABEL[2]), (228, 320, LABEL[1])]},
    ),
    (
    'ADVANCED APPRAISAL SERVICE - Workfile ID: 1a95b993 MARLBORO,MA 2 MOUNT ROYAL AVE MARLBOROUGH, MA 01752 Phone: (508) 485-2212, FAX:(508) 485-8691 For: COMMERCE INSURANCE MAPFRE | Insurance Claims Questions Call Phone: (800) 221-1605 Estimate of Record Owner: BELBA, WILLIAM Job Number: 16-31934 Written By: JOSEPH MOODY, 15363 Adjuster: STEFAN, PETER, (800) 221-1605 Business Insured: BELBA, WILLIAM Policy #: CGT208 Claim #: A0NNKA7-KNMWY9 Type of Loss: AD - unknown Date of Loss: 10/13/2016 1:00 PM '
    {'entities': [(60, 63, LABEL[0]), (29, 50, LABEL[2]), (232, 272, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0NNJ8A-KNNTN2 Webster, MA 01570 Workfile ID: a2ffb648 Estimate of Record Written By: CHRISTOPHER COGOLI, License Number: 15855, 10/17/2016 12:00:31 PM Adjuster: WILLIAMS, STEPHANIE, (800) 221-1605 x15106 Business Insured: JORGE ACEVEDO Owner Policy #: CSM436 MA Claim #: A0NNJ8A-KNNTN2 Type of Loss: Collision Date of Loss: 10/14/2016 12:00 AM Days to'
    {'entities': [(0, 19, LABEL[0]), (181, 202, LABEL[2]), (203, 317, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0NNG9T-KNKPR2 Webster, MA 01570 Workfile ID: 3c3b8ada Estimate of Record Written By: CHRISTOPHER COGOLI, License Number: 15855, 10/17/2016 12:20:47 PM Adjuster: HEATH, JAMIE, (800) 221-1605 x15701 Business Insured: STEVEN GROCCIA Owner Policy #: V52811 MA Claim #: A0NNG9T-KNKPR2 Type of Loss: Collision Date of Loss: 10/06/2016 12:00 PM Days to Repai'
    {'entities': [(0, 19, LABEL[0]), (181, 202, LABEL[2]), (203, 320, LABEL[1])]},
    ),
    (
    'ADVANCED APPRAISAL SERVICE - Workfile ID: cd197030 MARLBORO,MA 2 MOUNT ROYAL AVE MARLBOROUGH, MA 01752 Phone: (508) 485-2212, FAX:(508) 485-8691 For: SAFETY INSURANCE COMPANY SAFETY INSURANCE COMPANY-BOSTON Phone: (800) 951-2100 Estimate of Record Owner: KUEKAN, JAMES W Job Number: 16-33242 Written By: LORI GREGOIRE, 016296 Adjuster: Smith, Jacqueline, (800) 951-2100 Business Insured: KUEKAN, JAMES W Policy #: 3931971 Claim #: 2628568-01 Type of Loss: Collision Date of Loss: 10/28/2016 9:30 AM D'
    {'entities': [(60, 63, LABEL[0]), (29, 50, LABEL[2]), (229, 266, LABEL[1])]},
    ),
    (
    'TRAVELERS Keep and Close 888-299-7456 PROMPT 2 PO BOX 1450 Middleboro, MA 02344 Phone: (800) 422-3340 Claim #: L1S0884001 Fax: (877) 786-5584 Workfile ID: f849bf20 Estimate of Record Written By: JAMES MINEO, 11/17/2016 11:55:49 AM Adjuster: FAUSTIN, CANDACE, (407) 388-7078 Business Insured: ADRIAN ANDERSON Owner Policy #: PT5010A9965608012321 Claim #: L1S0884001 Type of Loss: Collision Date of Loss: 11/10/2016 05:45 AM Days to Repair: 13 Point of Impact: 01 Right Front Deductible: 500.00 Owner ('
    {'entities': [(0, 10, LABEL[0]), (142, 163, LABEL[2]), (164, 230, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: MWPP51-KPMJM6 Webster, MA 01570 Workfile ID: 36fde87a Estimate of Record Written By: PAUL GEOTIS, License Number: 2685, 11/11/2016 8:57:20 AM Adjuster: Angela Mancini, (800) 221-1605 x15693 Business Insured: SHELLEY GRAY Owner Policy #: CKS393 Claim #: MWPP51-KPMJM6 Type of Loss: Collision Date of Loss: 11/04/2016 12:00 AM Days to Repair: 0 Point of Impact: 0'
    {'entities': [(0, 19, LABEL[0]), (171, 192, LABEL[2]), (193, 280, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY Westborough Claim Office RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW 114 Turnpike Road Westborough, MA 01581 Claim #: 034642837-0001 Phone: (800) 251-7447 Workfile ID: 5ac29619 Estimate of Record Written By: RYAN WALSH, 11/15/2016 10:48:33 AM Adjuster: BOMBARD, SARA Insured: FRANCIS TOWEY Owner Policy #: OEMC Claim #: 034642837-0001 Type of Loss: COLL - Collision Date of Loss: 11/09/2016 12:00 AM Days to Repair: 0 Coverage Point of Impact: 06 Rear Deductibl'
    {'entities': [(0, 33, LABEL[0]), (194, 215, LABEL[2]), (216, 281, LABEL[1])]},
    ),
    (
    'SAFECO INSURANCE COMPANY OF AMERICA PLSA-MA RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW Sharon.Sandman@Safeco.com 62 MAPLE AVENUE KEENE, NH 03431 Claim #: 220230336038-202 Phone: (508) 309-9901 Workfile ID: e9711335 Estimate of Record Written By: SHARON SANDMAN, License Number: 13593, 8/19/2016 9:44:21 AM Adjuster: RAYMOND PALENA, (856) 435-2984 Business Insured: TANIA MONTANEZ Owner Policy #: K2773269 Claim #: 220230336038-202 Type of Loss: Collision Date of Loss: 08/09/2016 05:00 PM Days'
    {'entities': [(0, 36, LABEL[0]), (200, 221, LABEL[2]), (222, 312, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0NMJCV-KMKJT9 Webster, MA 01570 Workfile ID: 8181fd04 Estimate of Record Written By: ANTHONY BONAVITA, License Number: 10975, 9/27/2016 8:39:40 AM Adjuster: JULIEN, AMY, (800) 221-1605 Business Insured: ALEJANDRO Owner Policy #: Claim #: A0NMJCV-KMKJT9 FONTANEZ Type of Loss: Collision Date of Loss: 09/17/2016 12:00 PM Days to Repair: 0 Point of Impa'
    {'entities': [(0, 19, LABEL[0]), (181, 202, LABEL[2]), (203, 316, LABEL[1])]},
    ),
    (
    'ALLSTATE INSURANCE COMPANY New England Auto 55 Capital Blvd. 3rd Floor Rocky Hill, CT 06067 Claim #: 000435981790D01 Phone: (888) 233-2675 Workfile ID: d3cf2a1e Estimate of Record Written By: DONNA ASBURY, License Number: 012879, 11/17/2016 11:46:54 AM Adjuster: Asbury, Donna Insured: ITISHREE MISHRA Owner Policy #: 000925346518 Claim #: 000435981790D01 Type of Loss: Collision Date of Loss: 11/14/2016 12:00 PM Days to Repair: 4 Point of Impact: 02 Right Front Pillar Deductible: 500.00 (Right Sid'
    {'entities': [(0, 27, LABEL[0]), (139, 160, LABEL[2]), (161, 252, LABEL[1])]},
    ),
    (
    'ALLSTATE INSURANCE COMPANY New England Auto 55 Capital Blvd. 3rd Floor Rocky Hill, CT 06067 Claim #: 000435981790D01 Phone: (888) 233-2675 Workfile ID: d3cf2a1e Estimate of Record Written By: DONNA ASBURY, License Number: 012879, 11/17/2016 11:46:54 AM Adjuster: Asbury, Donna Insured: ITISHREE MISHRA Owner Policy #: 000925346518 Claim #: 000435981790D01 Type of Loss: Collision Date of Loss: 11/14/2016 12:00 PM Days to Repair: 4 Point of Impact: 02 Right Front Pillar Deductible: 500.00 (Right Sid'
    {'entities': [(0, 27, LABEL[0]), (139, 160, LABEL[2]), (161, 252, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0NPZ2R-KRAHA5 Webster, MA 01570 Workfile ID: 8aeae0ce Estimate of Record Written By: PAUL GEOTIS, License Number: 2685, 11/29/2016 6:36:12 AM Adjuster: GONZALEZ, PAIGE, (800) 221-1605 x15323 Business Insured: TARA ROCHE Owner Policy #: XV7893 Claim #: A0NPZ2R-KRAHA5 Type of Loss: Collision Date of Loss: 11/19/2016 12:00 PM Days to Repair: 0 Point of Impact: '
    {'entities': [(0, 19, LABEL[0]), (172, 193, LABEL[2]), (194, 281, LABEL[1])]},
    ),
    (
    'GEICO CINCINNATI FIELD OFFICE R3 For supplement requests copy the link below partners.geico.com/gvbps/logon.aspx ONE GEICO CENTER MACON, GA 31295 Phone: (513) 518-6812 Claim #: 0497207490101020-02 Fax: (866) 956-5747 Workfile ID: c6288b85 Estimate of Record Written By: DAVID DAY, License Number: 95401, 2/27/2016 11:37:29 AM Adjuster: F96Z Insured: John Otoole Owner Policy #: Claim #: 0497207490101020-02 Type of Loss: Liability Date of Loss: 12/30/2015 09:30 AM Days to Repair: 7 Point of Impact: '
    {'entities': [(0, 6, LABEL[0]), (217, 238, LABEL[2]), (239, 325, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY Westborough Claim Office RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW 114 Turnpike Road Westborough, MA 01581 Claim #: 034790811-0001 Phone: (800) 251-7447 Workfile ID: 5bd28691 Estimate of Record Written By: RICHARD WIKTOR, License Number: 015477, 12/27/2016 10:16:10 AM Adjuster: BOONE, LEONARD, (800) 637-0757 Business Insured: KATHRYN WAGNER Owner Policy #: MARI Claim #: 034790811-0001 Type of Loss: COLL - Collision Date of Loss: 12/16/2016 12:00 AM Days t'
    {'entities': [(0, 33, LABEL[0]), (194, 215, LABEL[2]), (216, 309, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY Lake Mary Claim Office RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW 255 Primera Boulevard Suite 300 Lake Mary, FL 32746 Phone: (800) 637-0757 Claim #: 034853735-0001 Fax: (877) 202-4607 Workfile ID: 8c908f3e Estimate of Record Written By: TODD HEYBECK, License Number: 015392, 1/4/2017 10:33:39 AM Adjuster: CHAMBERS, ELIZABETH, (800) 637-0757 Business Insured: JIMMY SANTANA Owner Policy #: MARI Claim #: 034853735-0001 Type of Loss: COLL - Collision Date of Lo'
    {'entities': [(0, 33, LABEL[0]), (224, 245, LABEL[2]), (246, 350, LABEL[1])]},
    ),
    (
    'CITIZENS INS CO OF AMERICA The Hanover Insurance Group-01 Worcester Claim Office Office # 800-628-0250 440 Lincoln St Worcester, MA 01653 Phone: (508) 847-8260 Claim #: 15-00-898195-2-1 Fax: (508) 926-5947 Workfile ID: 08c26c3c Estimate of Record Written By: ELVIN RIVERA, License Number: 014492, 1/16/2017 11:41:17 AM Adjuster: BECKMAN, LISA ANNE Insured: Eric Ostroff Owner Policy #: Claim #: 15-00-898195-2-1 Type of Loss: Liability Date of Loss: 01/07/2017 12:00 AM Days to Repair: 2 Point of Imp'
    {'entities': [(0, 27, LABEL[0]), (206, 227, LABEL[2]), (228, 318, LABEL[1])]},
    ),
    (
    'GEICO GEICO RI/MA EMAIL SUPPLEMENTS TO: R8ADSUPPMARI@GEICO.COM Visit us at geico.com 300 Cross Point Parkway Getzville, NY 14068 Phone: (401) 302-3689 Claim #: 0546447810101018-01 Fax: (855) 665-4554 Workfile ID: b7ae13e0 Estimate of Record Written By: JOHN RING, License Number: 2279690, 1/20/2017 11:14:44 AM Adjuster: RING, JOHN, (401) 302-3689 Cellular Insured: Iris Moore Owner Policy #: 4413613136 Claim #: 0546447810101018-01 Type of Loss: Collision Date of Loss: 01/19/2017 05:35 PM Days to R'
    {'entities': [(0, 6, LABEL[0]), (200, 221, LABEL[2]), (222, 310, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY 0150 Claim #: 035017577-0002 Workfile ID: 87269e54 Estimate of Record Written By: TODD HEYBECK, License Number: 015392, 2/14/2017 11:34:58 AM Adjuster: VO, TOMMY, (800) 566-0323 Business Insured: RICHARD REID Owner Policy #: Claim #: 035017577-0002 Type of Loss: Liability Date of Loss: 02/03/2017 12:00 AM Days to Repair: 0 Point of Impact: 12 Front Deductible: Owner (Claimant): Inspection Location: Appraiser Information: Repair Facility: Jelissa Viciso-Frias 290 '
    {'entities': [(0, 33, LABEL[0]), (62, 83, LABEL[2]), (84, 174, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY Lake Mary Claim Office RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW 255 Primera Boulevard Suite 300 Lake Mary, FL 32746 Phone: (800) 637-0757 Claim #: 035160526-0001 Fax: (877) 202-4607 Workfile ID: ab1bd878 Estimate of Record Written By: RAUL SANTIAGO, License Number: 133716, 3/7/2017 11:32:34 AM Adjuster: BARRETT, MATTHEW, (800) 637-0757 Business Insured: RYAN AMENTA Owner Policy #: MARI Claim #: 035160526-0001 Type of Loss: COLL - Collision Date of Loss: '
    {'entities': [(0, 33, LABEL[0]), (224, 245, LABEL[2]), (246, 336, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA For Supplement Requests copy the link below: //partners.geico.com/gvbps/logon.aspx 300 Crosspoint Parkway Getzville, NY 14068 Phone: (774) 479-6773 Claim #: 0565675240101010-01 Fax: (716) 898-0542 Workfile ID: 38cbf6f7 Estimate of Record Written By: ROWLAND GEMMA, License Number: 016543, 5/23/2018 10:54:32 AM Adjuster: Gemma, Rowland, (774) 479-6773 Cellular Insured: Rosea Owner Policy #: 4443173630 Claim #: 0565675240101010-01 Doos-Ghobrial Type of Loss: Collision Date of Loss: 1'
    {'entities': [(0, 15, LABEL[0]), (212, 233, LABEL[2]), (234, 325, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY Albany Claim Office RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW 3 Lear Jet Lane Suite 100 Latham, NY 12110 Phone: (800) 252-5730 Claim #: 035209103-0002 Fax: (800) 632-3704 Workfile ID: d61cb528 Estimate of Record Written By: TODD HEYBECK, License Number: 015392, 3/21/2017 11:29:48 AM Adjuster: HOLLAND, TIFFANI, (800) 252-5730 x77860 Business Insured: MICHAEL HANAM Owner Policy #: Claim #: 035209103-0002 Type of Loss: Liability Date of Loss: 03/14/2017 12:0'
    {'entities': [(0, 33, LABEL[0]), (212, 233, LABEL[2]), (234, 324, LABEL[1])]},
    ),
    (
    'ADVANCED APPRAISAL SERVICE - Workfile ID: 8180125b MARLBORO,MA 2 MOUNT ROYAL AVE MARLBOROUGH, MA 01752 Phone: (508) 485-2212, FAX:(508) 485-8691 For: VERMONT MUTUAL INSURANCE COMPANY MONTPELIER Estimate of Record Owner: CAMPBELL, DANIEL Job Number: 17-16136 Written By: MICHAEL SIMPSON, 13094 Adjuster: NELSON, CALEB, (802) 223-2341 x7267 Business Insured: CAMPBELL, DANIEL Policy #: MA17072827 Claim #: MPA71369 Type of Loss: Collision Date of Loss: 3/17/2017 12:00 PM Days to Repair: 0 Point of Imp'
    {'entities': [(60, 63, LABEL[0]), (29, 50, LABEL[2]), (194, 223, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA/RI Email Supplements to:R8ADSuppMARI@geico.com Visit Us Online At GEICO.COM 300 Crosspoint Parkway Getzville, NY 14068 Phone: (603) 234-8466 Claim #: 0307615840101088-01 Fax: (855) 884-9187 Workfile ID: 0738794a Estimate of Record Written By: ADAM EASTTY, License Number: 015878, 3/29/2017 10:10:34 AM Adjuster: F714 Insured: Rajendra Seku Owner Policy #: 4074786973 Claim #: 0307615840101088-01 Type of Loss: Collision Date of Loss: 03/26/2017 04:45 PM Days to Repair: 2 Point of Impa'
    {'entities': [(0, 6, LABEL[0]), (205, 226, LABEL[2]), (227, 316, LABEL[1])]},
    ),
    (
    'SAFECO INSURANCE COMPANY PLSA SUPPLEMENTS ONLY CALL 508 439 7849 ALL OTHER QUESTIONS CALL 800-332-3226 P.O. Box 515097 Los Angeles, CA 90051 Claim #: 505156556010-202 Phone: (508) 439-7849 Workfile ID: 827d42b9 Estimate of Record Written By: MATTHEW KIRKER, License Number: 013334, 4/26/2017 9:28:47 AM Adjuster: MAKENA L TRACEY Insured: TANIA MONTANEZ Owner Policy #: K2773269 Claim #: 505156556010-202 Type of Loss: Comprehensive Date of Loss: 12/02/2016 01:30 PM Days to Repair: 4 Point of Impact:'
    {'entities': [(0, 30, LABEL[0]), (189, 210, LABEL[2]), (211, 302, LABEL[1])]},
    ),
    (
    'AMICA MUTUAL INSURANCE COMPANY CENTRAL MASSACHUSETTS OFFICE PO Box 9690 Providence, RI 02940 Phone: (888) 702-6422 Claim #: 60002738323-1-1 Fax: (866) 381-3239 Workfile ID: a9992b16 Estimate of Record Written By: GARY LEDERER, License Number: 007629, 3/14/2017 10:58:36 AM Adjuster: ROBY, KAYLIN, (888) 702-6422 Business Insured: Brittany Owner Policy #: 4709061977 Claim #: 60002738323-1-1 Harris-Latorre Type of Loss: Collision Date of Loss: 03/10/2017 04:00 PM Days to Repair: 0 Point of Impact: 0'
    {'entities': [(0, 60, LABEL[0]), (160, 181, LABEL[2]), (182, 272, LABEL[1])]},
    ),
    (
    'AMICA MUTUAL INSURANCE COMPANY CENTRAL MASSACHUSETTS OFFICE PO Box 9690 Providence, RI 02940 Phone: (888) 702-6422 Claim #: 60002798964-1-1 Fax: (866) 381-3239 Workfile ID: c4eb5277 Estimate of Record Written By: GARY LEDERER, License Number: 007629, 5/9/2017 8:49:30 AM Adjuster: NADKARNI, ANU, (888) 702-6422 x46313 Business Insured: Carmen Owner Policy #: Claim #: 60002798964-1-1 Martinez-Ortiz Type of Loss: PD - Property Date of Loss: 05/05/2017 05:30 PM Days to Repair: 0 Damage Point of Impac'
    {'entities': [(0, 60, LABEL[0]), (160, 181, LABEL[2]), (182, 270, LABEL[1])]},
    ),
    (
    'GEICO REGION 08 For Supplement Requests copy the link below: //partners.geico.com/gvbps/Logon.aspx 300 CROSSPOINT PARKWAY GETZVILLE, NY 14068 Phone: (800) 841-3000 Claim #: 0399172860101092-01 Fax: (855) 221-2039 Workfile ID: 41323d82 Estimate of Record Written By: CANDY WILLIAMS, 4/27/2017 11:29:15 AM Adjuster: F649 Insured: James Barbato Owner Policy #: 4200816017 Claim #: 0399172860101092-01 Type of Loss: Collision Date of Loss: 12/02/2016 02:30 PM Days to Repair: 1 Point of Impact: 12 Front '
    {'entities': [(0, 6, LABEL[0]), (213, 234, LABEL[2]), (235, 303, LABEL[1])]},
    ),
    (
    'TRAVELERS New England Claim Department - 452 FOR SUPPLEMENTS PLEASE CALL (888)-299-7456 PROMPT 2 P.O. Box 1450 Middleboro, MA 02344 Phone: (800) 422-3340 Claim #: H0W1197001 Fax: (877) 786-5584 Workfile ID: c5d5d1a7 Estimate of Record Written By: MARK WAGNER, License Number: 12947, 2/9/2017 9:41:16 AM Adjuster: WAGNER, MARK, (508) 889-5240 Cellular Insured: KARA SELEN Owner Policy #: PT5010A9968203732321 Claim #: H0W1197001 Type of Loss: Collision Date of Loss: 02/03/2017 08:30 PM Days to Repair'
    {'entities': [(0, 10, LABEL[0]), (194, 215, LABEL[2]), (216, 302, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY Westborough Claim Office SUPPLEMENTS ONLY CALL 508-948-8259 ALL OTHER QUESTIONS CALL 800-251-7447 114 Turnpike Road Claim #: 035641567-0001 Westborough, MA 01581 Workfile ID: 6d6ce193 Estimate of Record Written By: TED GAUDET, License Number: 15211, 6/8/2017 10:29:45 AM Adjuster: LAJOIE, JULIE, (800) 251-7447 Business Insured: JAMES WILLIAMS jr Owner Policy #: MARI Claim #: 035641567-0001 Type of Loss: COLL - Collision Date of Loss: 05/11/2017 12:00 AM Days to Re'
    {'entities': [(0, 33, LABEL[0]), (195, 216, LABEL[2]), (217, 303, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY Westborough Claim Office SUPPLEMENTS ONLY CALL 508-948-8259 ALL OTHER QUESTIONS CALL 800-251-7447 114 Turnpike Road Claim #: 035641409-0001 Westborough, MA 01581 Workfile ID: 691acaee Estimate of Record Written By: TED GAUDET, License Number: 15211, 6/8/2017 10:57:01 AM Adjuster: FLICK, JOSHUA, (800) 637-0757 x7270889 Business Insured: JAMES WILLIAMS jr Owner Policy #: MARI Claim #: 035641409-0001 Type of Loss: COLL - Collision Date of Loss: 09/11/2016 12:00 AM D'
    {'entities': [(0, 33, LABEL[0]), (195, 216, LABEL[2]), (217, 303, LABEL[1])]},
    ),
    (
    'LM GENERAL INSURANCE COMPANY Westborough Claim Office SUPPLEMENTS ONLY CALL 508-948-5156 ALL OTHER QUESTIONS CALL 800-838-8844 114 TURNPIKE RD Westborough, MA 01581 Claim #: 035708152-0002 Phone: (800) 251-7447 Workfile ID: e19fdf90 Estimate of Record Written By: LYNN JOHNS, License Number: 015318, 6/30/2017 7:37:36 AM Adjuster: PAUL, ASHLEY, (800) 251-7447 Business Insured: DANIA DWYER Owner Policy #: MARI Claim #: 035708152-0002 Type of Loss: COLL - Collision Date of Loss: 06/02/2017 12:00 AM '
    {'entities': [(0, 29, LABEL[0]), (211, 232, LABEL[2]), (233, 320, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY Westborough Claim Office SUPPLEMENTS ONLY CALL 508-948-5156 ALL OTHER QUESTIONS CALL 800-838-8844 114 TURNPIKE RD Westborough, MA 01581 Claim #: 034062503-0001 Phone: (800) 251-7447 Workfile ID: f674edb0 Estimate of Record Written By: LYNN JOHNS, License Number: 015318, 6/29/2017 10:41:25 AM Adjuster: Johns, Lynn Insured: JAMES WILLIAMS, Owner Policy #: Claim #: 034062503-0001 JR Type of Loss: Collision Date of Loss: 07/13/2016 12:00 AM Days to Repair: 8 Point of'
    {'entities': [(0, 33, LABEL[0]), (215, 236, LABEL[2]), (237, 370, LABEL[1])]},
    ),
    (
    'TRAVELERS New England Claim Department - 452 FOR SUPPLEMENTS PLEASE CALL (888) 299-7456 PROMPT 2 P.O. BOX 1450 Middleboro, MA 02344 Phone: (800) 422-3340 Claim #: H0W5942001 Fax: (877) 786-5584 Workfile ID: 662faf9c Estimate of Record Written By: DAVID CAHILLANE, License Number: MA13389, 6/26/2017 10:58:42 AM Adjuster: GIANADDA, THOMAS, (716) 855-5614 Business Insured: STANISLAV Owner Policy #: PT5010A9959116872321 Claim #: H0W5942001 SHESTAK Type of Loss: Collision Date of Loss: 06/14/2017 03:1'
    {'entities': [(0, 10, LABEL[0]), (194, 215, LABEL[2]), (216, 310, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA For Supplement requests copy the link below - partners.geico.com/gvbps/Logon.aspx 300 Crosspoint Parkway Getzville, NY 14068 Phone: (774) 479-9656 Claim #: 0556813660101019-03 Fax: (855) 306-3307 Workfile ID: 3c984d0c Estimate of Record Written By: DANIEL HANSEN, License Number: 016188, 6/30/2017 11:28:59 AM Adjuster: ABV7 Insured: Timothy Delaney Owner Policy #: Claim #: 0556813660101019-03 Type of Loss: Liability Date of Loss: 06/18/2017 03:00 PM Days to Repair: 3 Point of Impac'
    {'entities': [(0, 15, LABEL[0]), (211, 232, LABEL[2]), (233, 324, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY Westborough Claim Office RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW 114 Turnpike Road Westborough, MA 01581 Claim #: 035823283-0001 Phone: (800) 251-7447 Workfile ID: 1bdd4330 Estimate of Record Written By: TODD HEYBECK, License Number: 015392, 7/11/2017 9:09:04 AM Adjuster: BENSON, JONATHAN, (800) 251-7447 Business Insured: GEORGANNE Owner Policy #: MARI Claim #: 035823283-0001 BRONZO Type of Loss: COLL - Collision Date of Loss: 07/08/2017 12:00 AM Days t'
    {'entities': [(0, 33, LABEL[0]), (194, 215, LABEL[2]), (216, 305, LABEL[1])]},
    ),
    (
    'GEICO GEICO RI/MA EMAIL SUPPLEMENTS TO: R8ADSUPPMARI@GEICO.COM Visit us at geico.com 300 Cross Point Parkway Getzville, NY 14068 Phone: (774) 452-5833 Claim #: 0589492120101017-01 Fax: (844) 841-4461 Workfile ID: db228595 Estimate of Record Written By: MATTHEW TRETHEWAY, License Number: 016237, 8/5/2017 9:55:37 AM Adjuster: TRETHEWAY, MATTHEW, (774) 452-5833 Cellular Insured: Matthew Wenners Owner Policy #: 4480223637 Claim #: 0589492120101017-01 Type of Loss: Collision Date of Loss: 08/01/2017 '
    {'entities': [(0, 6, LABEL[0]), (200, 221, LABEL[2]), (222, 315, LABEL[1])]},
    ),
    (
    'ADVANCED APPRAISAL SERVICE - Workfile ID: 3f6ff106 MARLBORO,MA 2 MOUNT ROYAL AVE MARLBOROUGH, MA 01752 Phone: (508) 485-2212, FAX:(508) 485-8691 For: COMMERCE INSURANCE MAPFRE | Insurance Claims Questions Call Phone: (800) 221-1605 Estimate of Record Owner: BARBATO, JAMES Job Number: 17-23828 Written By: Francisco Enriquez, 15860 Adjuster: ROGERS, JOSEPH, (800) 221-1605 Business Insured: BARBATO, JAMES Policy #: FVD136 Claim #: A0P1TZX-MKCRV6 Type of Loss: AD - unknown Date of Loss: 7/9/2017 1:0'
    {'entities': [(60, 63, LABEL[0]), (29, 50, LABEL[2]), (232, 270, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0P2MJ0-MMCMW2 Webster, MA 01570 Workfile ID: ec94c9a7 Estimate of Record Written By: CHRISTOPHER COGOLI, License Number: 15855, 9/14/2017 9:33:58 AM Adjuster: KOCHANOWSKI, DANIEL, (800) 221-1605 x15593 Business Insured: GENA SEARS Owner Policy #: GQK590 MA Claim #: A0P2MJ0-MMCMW2 Type of Loss: Collision Date of Loss: 07/26/2017 12:00 PM Days to Repa'
    {'entities': [(0, 19, LABEL[0]), (181, 202, LABEL[2]), (203, 297, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY Westborough Claim Office RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW 114 Turnpike Road Westborough, MA 01581 Claim #: 035536430-0001 Phone: (800) 251-7447 Workfile ID: e014fbda Estimate of Record Written By: NATHAN WIKTOR, License Number: 016334, 5/22/2017 10:25:19 AM Adjuster: SMITH, JOSHUA, (800) 637-0757 Business Insured: NELLY COLON Owner Policy #: MARI Claim #: 035536430-0001 Type of Loss: COLL - Collision Date of Loss: 02/28/2017 12:00 AM Days to Repa'
    {'entities': [(0, 33, LABEL[0]), (194, 215, LABEL[2]), (216, 307, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA/RI Email Supplements to:R8ADSuppMARI@geico.com Visit Us Online At GEICO.COM 300 Crosspoint Parkway Getzville, NY 14068 Claim #: 0522884020101016-01 Phone: (302) 235-9474 Workfile ID: ce71c89c Estimate of Record Written By: JARED MCDOUGALL, License Number: 016478, 11/3/2017 10:13:38 AM Adjuster: CGZG Insured: Anna Otoole Owner Policy #: 4377888765 Claim #: 0522884020101016-01 Type of Loss: Comprehensive Date of Loss: 10/24/2017 12:00 PM Days to Repair: 5 Point of Impact: 02 Right F'
    {'entities': [(0, 6, LABEL[0]), (185, 206, LABEL[2]), (207, 300, LABEL[1])]},
    ),
    (
    'CITIZENS INS CO OF AMERICA The Hanover Insurance Group-01 Worcester Claim Office Office # 800-628-0250 440 Lincoln St. WORCESTER, MA 01653 Phone: (508) 751-2190 Claim #: 19-00-063310-1-1 Fax: (508) 926-4970 Workfile ID: 9ce2fe67 Estimate of Record Written By: JOE STRANIERI, License Number: 006890, 10/17/2017 10:02:04 AM Adjuster: 010EY Insured: Brianna Guenther Owner Policy #: Claim #: 19-00-063310-1-1 Type of Loss: Collision Date of Loss: 09/19/2017 03:20 PM Days to Repair: 6 Point of Impact: 1'
    {'entities': [(0, 27, LABEL[0]), (207, 228, LABEL[2]), (229, 321, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA/RI EMAIL SUPPLEMENTS TO R8ADSUPPMARI@GEICO.COM Visit us Online @ www.geico.com 300 Crosspoint Pkwy Getzville, NY 14068 Claim #: 0540767940101059-01 Phone: (747) 203-3709 Workfile ID: 597090ff Estimate of Record Written By: RONARDI CONTRERAS, License Number: 016090, 10/18/2017 9:39:27 AM Adjuster: ABFQ Insured: Kevin Yeung Owner Policy #: 4405011737 Claim #: 0540767940101059-01 Type of Loss: Comprehensive Date of Loss: 10/13/2017 05:45 PM Days to Repair: 5 Point of Impact: 04 Right'
    {'entities': [(0, 6, LABEL[0]), (185, 206, LABEL[2]), (207, 302, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance Claims Questions call 800 221 1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Webster, MA 01570 Claim #: A0P6914-MTKHR9 Phone: (800) 221-1605 Workfile ID: 2a711b08 Estimate of Record Written By: DAVID RASZKA, License Number: 16401, 11/14/2017 9:46:39 AM Adjuster: STONE, NOELLE, (800) 221-1605 x15498 Business Insured: MICHELLE NYE Owner Policy #: QP6609 Claim #: A0P6914-MTKHR9 Type of Loss: Collision Date of Loss: 11/03/2017 12:00 PM Days to R'
    {'entities': [(0, 19, LABEL[0]), (196, 217, LABEL[2]), (218, 307, LABEL[1])]},
    ),
    (
    'ADVANCED APPRAISAL SERVICE - Workfile ID: 264d227c MARLBORO,MA 2 MOUNT ROYAL AVE MARLBOROUGH, MA 01752 Phone: (508) 485-2212, FAX:(508) 485-8691 For: VERMONT MUTUAL INSURANCE COMPANY MONTPELIER Estimate of Record Owner: MARTIN, JAMIE Job Number: 17-34388 Written By: Francisco Enriquez, 15860 Adjuster: TREMBLAY, KARYN, (800) 435-0397 x7270 Business Insured: MARTIN, JAMIE Policy #: MA17091705 Claim #: M0004556 Type of Loss: Collision Date of Loss: 11/30/2017 12:00 PM Days to Repair: 0 Point of Imp'
    {'entities': [(60, 63, LABEL[0]), (29, 50, LABEL[2]), (194, 231, LABEL[1])]},
    ),
    (
    'METLIFE AUTO & HOME Auto Adjusting Unit - MA MA License # 6451 P.O. Box 6040 Scranton, PA 18506 Phone: (800) 854-6011 x4727 Claim #: SLH19740 2 Fax: (866) 958-1547 Workfile ID: ad145e3d Estimate of Record Written By: ALBERT CALEF, License Number: 6451, 12/4/2017 8:51:07 AM Adjuster: Baris, Paige, (800) 854-6011 x5023 Business Insured: ERIN Owner Policy #: 808383583 Claim #: SLH19740 2 AMOAKO-ATTA Type of Loss: Collision Date of Loss: 11/30/2017 12:45 PM Days to Repair: 2 Point of Impact: 06 Rear'
    {'entities': [(0, 20, LABEL[0]), (164, 185, LABEL[2]), (186, 273, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance Claims Questions call 800 221 1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Webster, MA 01570 Claim #: A0P7GV6-MVPRW9 Phone: (800) 221-1605 Workfile ID: 753d74fd Estimate of Record Written By: DAVID RASZKA, License Number: 16401, 12/5/2017 11:26:32 AM Adjuster: BREAU, DEREK, (800) 221-1605 x15864 Business Insured: WILLIAM HAGERTY Owner Policy #: Claim #: A0P7GV6-MVPRW9 Type of Loss: AD - Property Date of Loss: 12/03/2017 12:00 PM Days to Re'
    {'entities': [(0, 19, LABEL[0]), (196, 217, LABEL[2]), (218, 307, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance Claims Questions call 800 221 1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Webster, MA 01570 Claim #: A0P8NA5-MWWVA0 Phone: (800) 221-1605 Workfile ID: b3e8fef7 Estimate of Record Written By: DAVID RASZKA, License Number: 16401, 1/9/2018 10:58:23 AM Adjuster: JULIAN, SHERI, (800) 221-1605 x15607 Business Insured: GLORIA Owner Policy #: GHG746 Claim #: A0P8NA5-MWWVA0 BUCHANNAN Type of Loss: Collision Date of Loss: 01/02/2018 12:00 PM Days t'
    {'entities': [(0, 19, LABEL[0]), (196, 217, LABEL[2]), (218, 306, LABEL[1])]},
    ),
    (
    'USAA 1010 Please visit us @ USAA.com PO Box 33490 San Antonio, TX 78265 Claim #: 019724947000000011001 Phone: (800) 531-8722 Workfile ID: fee94bd2 Estimate of Record Written By: RAYMOND CHAPIN, License Number: 014258, 1/16/2018 9:22:43 AM Insured: JORDAN BERG Owner Policy #: 019724947 Claim #: 019724947000000011001 POWERS Type of Loss: Collision Date of Loss: 01/11/2018 12:00 AM Days to Repair: 5 Point of Impact: 06 Rear Deductible: 500.00 Owner (Insured): Inspection Location: Appraiser Informat'
    {'entities': [(0, 5, LABEL[0]), (125, 146, LABEL[2]), (147, 238, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance Claims Questions call 800 221 1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Webster, MA 01570 Claim #: A0P81VT-MVRKY3 Phone: (800) 221-1605 Workfile ID: ff73afdd Estimate of Record Written By: DAVID RASZKA, License Number: 16401, 12/18/2017 3:49:04 PM Adjuster: WILLIAMS, STEPHANIE, (800) 221-1605 x15106 Business Insured: COURTLAND Owner Policy #: WK5947 Claim #: A0P81VT-MVRKY3 KEELIN Type of Loss: Collision Date of Loss: 12/03/2017 12:00 PM'
    {'entities': [(0, 19, LABEL[0]), (196, 217, LABEL[2]), (218, 325, LABEL[1])]},
    ),
    (
    'GEICO MA - FAX SUPPLEMENT TO: 877-863-0809 EMAIL SUPPLEMENTS TO: R8ADSUPPMARI@GEICO.COM VISIT US ONLINE AT GEICO.COM 300 Crosspoint Parkway Getzville, NY 14068 Phone: (800) 341-3000 Claim #: 0549015370101087-01 Fax: (716) 898-0542 Workfile ID: 68437080 Estimate of Record Written By: DANIEL CROSS, 2/21/2018 8:54:20 AM Adjuster: ABVH Insured: James Fleury Owner Policy #: 4417570555 Claim #: 0549015370101087-01 Type of Loss: Collision Date of Loss: 02/08/2018 10:30 PM Days to Repair: 11 Point of Im'
    {'entities': [(0, 6, LABEL[0]), (231, 252, LABEL[2]), (253, 318, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA/RI Email Supplements to:R8ADSuppMARI@geico.com Visit Us Online At GEICO.COM 300 Crosspoint Parkway Getzville, NY 14068 Claim #: 0611545120101014-03 Phone: (302) 235-9474 Workfile ID: 5b62e350 Estimate of Record Written By: JARED MCDOUGALL, License Number: 016478, 3/5/2018 11:51:42 AM Adjuster: CGZG Insured: Della Newell Owner Policy #: Claim #: 0611545120101014-03 Type of Loss: Liability Date of Loss: 01/01/2018 05:15 PM Days to Repair: 4 Point of Impact: 07 Left Rear Deductible: '
    {'entities': [(0, 6, LABEL[0]), (185, 206, LABEL[2]), (207, 299, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance Claims Questions call 800 221 1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Webster, MA 01570 Claim #: A0PC9MP-NAXNN6 Phone: (800) 221-1605 Workfile ID: ca3e00d3 Estimate of Record Written By: DAVID RASZKA, License Number: 16401, 3/28/2018 11:01:36 AM Adjuster: LONGTIN, ROZLIN, (800) 221-1605 x15007 Business Insured: STEVEN GONZALEZ Owner Policy #: BNS203 Claim #: A0PC9MP-NAXNN6 Type of Loss: AFWH - Wind Hail Date of Loss: 03/07/2018 12:00 '
    {'entities': [(0, 19, LABEL[0]), (196, 217, LABEL[2]), (218, 307, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance Claims Questions call 800 221 1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Webster, MA 01570 Claim #: A0PC9MP-NAXNN6 Phone: (800) 221-1605 Workfile ID: ca3e00d3 Estimate of Record Written By: DAVID RASZKA, License Number: 16401, 3/28/2018 11:01:36 AM Adjuster: LONGTIN, ROZLIN, (800) 221-1605 x15007 Business Insured: STEVEN GONZALEZ Owner Policy #: BNS203 Claim #: A0PC9MP-NAXNN6 Type of Loss: AFWH - Wind Hail Date of Loss: 03/07/2018 12:00 '
    {'entities': [(0, 19, LABEL[0]), (196, 217, LABEL[2]), (218, 307, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: NAXM88 Webster, MA 01570 Workfile ID: 82253729 Estimate of Record Written By: MATTHEW P GODIN, License Number: 14333, 2/13/2017 10:57:31 AM Insured: JANICE ORTIZ Owner Policy #: Claim #: NAXM88 Type of Loss: Collision Date of Loss: 01/31/2017 12:00 AM Days to Repair: 0 Point of Impact: 06 Rear Deductible: Owner (Insured): Inspection Location: Apprais'
    {'entities': [(0, 19, LABEL[0]), (173, 194, LABEL[2]), (195, 287, LABEL[1])]},
    ),
    (
    'ADVANCED APPRAISAL SERVICE - Workfile ID: dc6cccf2 MARLBORO,MA 2 MOUNT ROYAL AVE MARLBOROUGH, MA 01752 Phone: (508) 485-2212, FAX:(508) 485-8691 For: COMMERCE INSURANCE MAPFRE | Insurance Claims Questions Call Phone: (800) 221-1605 Estimate of Record Owner: SANTOS, AMARILIS Job Number: 18-17706 Written By: JOSEPH MOODY, 15363 Adjuster: WHITE, HANNAH, (800) 221-1605 Business Insured: SANTOS, AMARILIS Policy #: HCP880 Claim #: A0PG7WN-NCYYP5 Type of Loss: AC - unknown Date of Loss: 3/31/2018 1:00 '
    {'entities': [(60, 63, LABEL[0]), (29, 50, LABEL[2]), (232, 268, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA For Supplement Requests copy the link below: //partners.geico.com/gvbps/logon.aspx 300 Crosspoint Parkway Getzville, NY 14068 Phone: (774) 479-6773 Claim #: 0608663410101019-02 Fax: (716) 898-0542 Workfile ID: 6bb9a700 Estimate of Record Written By: ROWLAND GEMMA, License Number: 016543, 4/23/2018 10:24:34 AM Adjuster: Gemma, Rowland, (774) 479-6773 Cellular Insured: Paul Sullivan Owner Policy #: Claim #: 0608663410101019-02 Type of Loss: Liability Date of Loss: 12/25/2017 11:30 A'
    {'entities': [(0, 15, LABEL[0]), (212, 233, LABEL[2]), (234, 325, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA For Supplement Requests copy the link below: //partners.geico.com/gvbps/logon.aspx 300 Crosspoint Parkway Getzville, NY 14068 Phone: (774) 479-6773 Claim #: 0527955500101034-01 Fax: (716) 898-0542 Workfile ID: cffbae14 Estimate of Record Written By: ROWLAND GEMMA, License Number: 016543, 4/23/2018 9:23:01 AM Adjuster: Gemma, Rowland, (774) 479-6773 Cellular Insured: Marcelina Molina Owner Policy #: 4524065648 Claim #: 0527955500101034-01 Type of Loss: Collision Date of Loss: 04/20'
    {'entities': [(0, 15, LABEL[0]), (212, 233, LABEL[2]), (234, 324, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY Westborough Claim Office RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW 114 Turnpike Road Westborough, MA 01581 Claim #: 037409560-0001 Phone: (800) 251-7447 Workfile ID: 1a1ab2d6 Estimate of Record Written By: TODD HEYBECK, License Number: 015392, 5/14/2018 12:00:44 PM Adjuster: LAMOUREUX, ARLENE, (800) 251-7447 Business Insured: CLAUDETTE Owner Policy #: MARI Claim #: 037409560-0001 MOUSSA Type of Loss: COLL - Collision Date of Loss: 05/04/2018 12:00 AM Days'
    {'entities': [(0, 33, LABEL[0]), (194, 215, LABEL[2]), (216, 320, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA For Supplement Requests copy the link below: //partners.geico.com/gvbps/logon.aspx 300 Crosspoint Parkway Getzville, NY 14068 Phone: (774) 479-6773 Claim #: 0592464490101021-01 Fax: (716) 898-0542 Workfile ID: e286eab3 Estimate of Record Written By: ROWLAND GEMMA, License Number: 016543, 5/11/2018 7:52:48 AM Adjuster: Gemma, Rowland, (774) 479-6773 Cellular Insured: Israel Cruz Owner Policy #: 4484843059 Claim #: 0592464490101021-01 Type of Loss: Collision Date of Loss: 04/24/2018'
    {'entities': [(0, 15, LABEL[0]), (212, 233, LABEL[2]), (234, 324, LABEL[1])]},
    ),
    (
    'ADVANCED APPRAISAL SERVICE - Workfile ID: dc4e725f MARLBORO,MA 2 MOUNT ROYAL AVE MARLBOROUGH, MA 01752 Phone: (508) 485-2212, FAX:(508) 485-8691 For: SAFETY INSURANCE COMPANY SAFETY INSURANCE COMPANY-BOSTON Phone: (800) 951-2100 Estimate of Record Owner: KUEKAN, JAMES W Job Number: 18-20787 Written By: JOSEPH MOODY, 15363 Adjuster: McGonigle, Kara, (617) 951-0600 x2025 Business Insured: KUEKAN, JAMES W Policy #: 3931971 Claim #: 2754197-01 Type of Loss: Collision Date of Loss: 5/10/2018 3:30 PM '
    {'entities': [(60, 63, LABEL[0]), (29, 50, LABEL[2]), (229, 266, LABEL[1])]},
    ),
    (
    'GEICO MA - FAX SUPPLEMENT TO: 877-863-0809 EMAIL SUPPLEMENTS TO: R8ADSUPPMARI@GEICO.COM VISIT US ONLINE AT GEICO.COM 300 Crosspoint Parkway Getzville, NY 14068 Phone: (800) 341-3000 Claim #: 0626883260101012-01 Fax: (716) 898-0542 Workfile ID: 9c531ce5 Estimate of Record Written By: DANIEL CROSS, 6/1/2018 10:07:53 AM Adjuster: ABTP Insured: Jasmin Natera Owner Policy #: 4539512485 Claim #: 0626883260101012-01 Type of Loss: Collision Date of Loss: 05/29/2018 05:00 PM Days to Repair: 5 Point of Im'
    {'entities': [(0, 6, LABEL[0]), (231, 252, LABEL[2]), (253, 318, LABEL[1])]},
    ),
    (
    'LM GENERAL INSURANCE COMPANY Westborough Claim Office SUPPLEMENTS ONLY CALL 508-948-5156 ALL OTHER QUESTIONS CALL 800-251-7447 114 TURNPIKE RD Westborough, MA 01581 Claim #: 037808846-0001 Phone: (800) 251-7447 Workfile ID: 3e44d3b9 Estimate of Record Written By: LYNN JOHNS, License Number: 015318, 7/13/2018 11:24:32 AM Adjuster: BRUDZIENSKI, JESSICA, (800) 252-5730 Business Insured: NANCY Owner Policy #: MARI Claim #: 037808846-0001 VILLAVICENCIO Type of Loss: COMP - Date of Loss: 07/08/2018 12'
    {'entities': [(0, 29, LABEL[0]), (211, 232, LABEL[2]), (233, 321, LABEL[1])]},
    ),
    (
    'METLIFE AUTO & HOME Auto Adjusting Unit - MA State Lic # P.O. Box 6040 Scranton, PA 18505 Claim #: SLJ31333 1 Phone: (800) 854-6011 Workfile ID: ffd8eee3 Estimate of Record Written By: ALBERT CALEF, License Number: 6451, 7/17/2018 10:10:05 AM Adjuster: Carroll, Linda Insured: JANINE PERRON Owner Policy #: Claim #: SLJ31333 1 Type of Loss: Collision Date of Loss: 07/13/2018 03:00 PM Days to Repair: 9 Point of Impact: 06 Rear Deductible: 1000.00 Owner (Insured): Inspection Location: Appraiser Info'
    {'entities': [(0, 20, LABEL[0]), (132, 153, LABEL[2]), (154, 242, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY Westborough Claim Office SUPPLEMENTS ONLY CALL 508-868-5675 ALL OTHER QUESTIONS CALL 844-525-2467 114 Turnpike Road Westborough, MA 01581 Claim #: 035372549-0001 Phone: (800) 251-7447 Workfile ID: 9f8256da Estimate of Record Written By: CHRISTOPHER LARSEN, License Number: 15603, 7/24/2018 11:46:54 AM Adjuster: SANTIAGO-AGAN, SUJEILY, (800) 637-0757 Business Insured: PETER WAGNER Owner Policy #: MARI Claim #: 035372549-0001 Type of Loss: COLL - Collision Date of L'
    {'entities': [(0, 33, LABEL[0]), (217, 238, LABEL[2]), (239, 334, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0PJYT9-NMANR8 Webster, MA 01570 Workfile ID: 43b4c3a3 Estimate of Record Written By: MATTHEW P GODIN, License Number: 14333, 6/9/2018 11:19:27 AM Adjuster: MELANSON, ELIZABETH, (800) 221-1605 Business Insured: MATTHEW BROWN Owner Policy #: Claim #: A0PJYT9-NMANR8 Type of Loss: Collision Date of Loss: 06/04/2018 12:00 PM Days to Repair: 0 Point of Im'
    {'entities': [(0, 19, LABEL[0]), (181, 202, LABEL[2]), (203, 294, LABEL[1])]},
    ),
    (
    'TRAVELERS New England Claim Department - 244 FOR SUPPLEMENT PLEASE CALL 888-299-7456 PROMPT 2 P.O.Box 4947 Orlando, FL 32802 Phone: (877) 411-0768 Claim #: H4Z9175001 Fax: (877) 786-5584 Workfile ID: a6c301c2 Estimate of Record Written By: MARK WAGNER, License Number: 12947, 5/24/2018 11:37:40 AM Adjuster: SOUTHWICK, COURTNEY, (860) 756-9690 Business Insured: MEGI STOJA Owner Policy #: PT5010A9958822192321 Claim #: H4Z9175001 Type of Loss: Collision Date of Loss: 05/14/2018 09:45 AM Days to Repa'
    {'entities': [(0, 10, LABEL[0]), (187, 208, LABEL[2]), (209, 297, LABEL[1])]},
    ),
    (
    'SAFECO INSURANCE COMPANY OF AMERICA 0327 Claim #: 038003998-0001 Workfile ID: e4b18648 Estimate of Record Written By: TODD HEYBECK, License Number: 015392, 8/15/2018 8:40:18 AM Adjuster: KINKLE, LAUREN, (856) 335-7281 Business Insured: Donald Lundstrom Owner Policy #: MARI Claim #: 038003998-0001 Type of Loss: COLL - Collision Date of Loss: 08/09/2018 12:00 AM Days to Repair: 5 Coverage Point of Impact: 07 Left Rear Deductible: 500.00 Owner (Insured): Inspection Location: Appraiser Information: '
    {'entities': [(0, 36, LABEL[0]), (65, 86, LABEL[2]), (87, 176, LABEL[1])]},
    ),
    (
    'ADVANCED APPRAISAL SERVICE - Workfile ID: 75f41426 MARLBORO,MA 2 MOUNT ROYAL AVE MARLBOROUGH, MA 01752 Phone: (508) 485-2212, FAX:(508) 485-8691 For: COMMERCE INSURANCE MAPFRE | Insurance Claims Questions Call Phone: (800) 221-1605 Estimate of Record Owner: LUCKY, TAMARA Job Number: 18-28549 Written By: NEAL TROMBLY, 15473 Adjuster: GRILLO, MARK, (800) 221-1605 Business Insured: LUCKY, TAMARA Policy #: HGQ802 Claim #: A0PN9PZ-NPVHP0 Type of Loss: AC - unknown Date of Loss: 8/2/2018 1:00 PM Days '
    {'entities': [(60, 63, LABEL[0]), (29, 50, LABEL[2]), (232, 268, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY Westborough Claim Office 114 Turnpike Road Westborough, MA 01581 Claim #: 038060426-0001 Phone: (413) 244-3561 Workfile ID: 4d8c441d Estimate of Record Written By: NATHAN WIKTOR, License Number: 016334, 8/27/2018 10:29:34 AM Adjuster: LEDFORD, KATHLEEN, (508) 621-1462 Business Insured: YESSENIA GARCIA Owner Policy #: MARI Claim #: 038060426-0001 Type of Loss: Collision Date of Loss: 08/16/2018 12:00 AM Days to Repair: 0 Point of Impact: 03 Right T-Bone Deductible'
    {'entities': [(0, 33, LABEL[0]), (144, 165, LABEL[2]), (166, 257, LABEL[1])]},
    ),
    (
    'LM GENERAL INSURANCE COMPANY Westborough Claim Office SUPPLEMENTS ONLY CALL 508-948-5156 ALL OTHER QUESTIONS CALL 800-251-7447 114 TURNPIKE RD Westborough, MA 01581 Claim #: 038124804-0001 Phone: (800) 251-7447 Workfile ID: 45bb8a7b Estimate of Record Written By: LYNN JOHNS, License Number: 015318, 9/6/2018 2:36:53 PM Adjuster: MORAN, JAMES, (508) 621-1454 Business Insured: JOAQUIN Owner Policy #: MARI Claim #: 038124804-0001 MONSERRATE Type of Loss: COLL - Collision Date of Loss: 08/30/2018 12:'
    {'entities': [(0, 29, LABEL[0]), (211, 232, LABEL[2]), (233, 340, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA Request a supplement at partners.geico.com 300 Crosspoint Parkway Getzville, NY 14068 Phone: (774) 479-6773 Claim #: 0620406250101010-01 Fax: (716) 898-0542 Workfile ID: 064450db Estimate of Record Written By: ROWLAND GEMMA, License Number: 016543, 9/14/2018 9:07:35 AM Adjuster: Gemma, Rowland, (774) 479-6773 Cellular Insured: Ariana Ryan Owner Policy #: 4529044804 Claim #: 0620406250101010-01 Type of Loss: Collision Date of Loss: 08/21/2018 12:00 PM Days to Repair: 17 Point of Im'
    {'entities': [(0, 15, LABEL[0]), (172, 193, LABEL[2]), (194, 284, LABEL[1])]},
    ),
    (
    'TRAVELERS Auto Express Claim Center-244 Email Supplements:supplementrequest@travelers. com By Phone: 888-299-7456 PROMPT 2 PO BOX 4947 Orlando, FL 30328 Phone: (877) 411-0768 Claim #: H3R3334001 Fax: (877) 786-5584 Workfile ID: a8f9149f Estimate of Record Written By: JAMES MINEO, 9/13/2018 3:06:59 PM Adjuster: PASTORMERLO, KIETH, (860) 756-9698 Evening Insured: NICHOLAS SPRING Owner Policy #: PT5010A6019811212321 Claim #: H3R3334001 Type of Loss: Collision Date of Loss: 09/11/2018 12:00 AM Days '
    {'entities': [(0, 10, LABEL[0]), (215, 236, LABEL[2]), (237, 271, LABEL[1])]},
    ),
    (
    'LM GENERAL INSURANCE COMPANY Westborough Claim Office SUPPLEMENTS ONLY CALL 508-868-5675 ALL OTHER QUESTIONS CALL 844-525-2467 114 Turnpike Road Westborough, MA 01581 Claim #: 038230653-0001 Phone: (800) 251-7447 Workfile ID: 5717f8b0 Estimate of Record Written By: CHRISTOPHER LARSEN, License Number: 15603, 9/19/2018 11:05:44 AM Adjuster: BUFFONE, SARA, (508) 621-1435 Business Insured: TITHYA PUCH Owner Policy #: MARI Claim #: 038230653-0001 Type of Loss: Collision Date of Loss: 09/17/2018 12:00'
    {'entities': [(0, 29, LABEL[0]), (213, 234, LABEL[2]), (235, 330, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA Request a supplement at partners.geico.com 300 Crosspoint Parkway Getzville, NY 14068 Phone: (774) 479-6773 Claim #: 0572147360101098-01 Fax: (716) 898-0542 Workfile ID: d8cc3fca Estimate of Record Written By: ROWLAND GEMMA, License Number: 016543, 9/24/2018 10:40:50 AM Adjuster: Gemma, Rowland, (774) 479-6773 Cellular Insured: Caitlin Shultz Owner Policy #: 4453145478 Claim #: 0572147360101098-01 Type of Loss: Collision Date of Loss: 09/13/2018 07:45 AM Days to Repair: 12 Point o'
    {'entities': [(0, 15, LABEL[0]), (172, 193, LABEL[2]), (194, 285, LABEL[1])]},
    ),
    (
    'LM GENERAL INSURANCE COMPANY Westborough Claim Office SUPPLEMENTS ONLY CALL 508-439-7561 ALL OTHER QUESTIONS CALL 800-252-5730 114 Turnpike Road Westborough, MA 01581 Claim #: 038399695-0001 Phone: (800) 252-5730 Workfile ID: 286f555c Estimate of Record Written By: EGES METOHU, License Number: 015775, 10/17/2018 10:23:29 AM Adjuster: FLEMING, STEVEN, (800) 251-7447 x7270729 Business Insured: ANGEL PIMENTEL Owner Policy #: MARI Claim #: 038399695-0001 Type of Loss: COLL - Collision Date of Loss: '
    {'entities': [(0, 29, LABEL[0]), (213, 234, LABEL[2]), (235, 325, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY Albany Claim Office SUPPLEMENTS ONLY CALL 508-439-7561 ALL OTHER QUESTIONS CALL 800-252-5730 114 Turnpike Road Westborough, MA 01581 Claim #: 038576545-0001 Phone: (800) 252-5730 Workfile ID: 85d6679c Estimate of Record Written By: EGES METOHU, License Number: 015775, 11/19/2018 11:47:30 AM Adjuster: TOLEMAN, CHRISTINE, (800) 252-5730 Business Insured: NADIA GIRGIS Owner Policy #: MARI Claim #: 038576545-0001 Type of Loss: COLL - Collision Date of Loss: 07/27/201'
    {'entities': [(0, 33, LABEL[0]), (212, 233, LABEL[2]), (234, 324, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance Claims Questions call 800 221 1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Webster, MA 01570 Claim #: A0PRJM1-NVHXC8 Phone: (800) 221-1605 Workfile ID: 8a01975e Estimate of Record Written By: DAVID RASZKA, License Number: 16401, 10/9/2018 10:55:43 AM Adjuster: MELVILLE, ALIYA-MARIE, (800) 221-1605 x15546 Business Insured: BRIANNA LIU Owner Policy #: FQK884 Claim #: A0PRJM1-NVHXC8 Type of Loss: AD - Property Date of Loss: 10/05/2018 12:00 A'
    {'entities': [(0, 19, LABEL[0]), (196, 217, LABEL[2]), (218, 307, LABEL[1])]},
    ),
    (
    'TRAVELERS New England Claim Department- 452 Email Supplements:supplementrequest@travelers. com By Phone: (888) 299-7456 (Prompt 2) P.O. Box 430 Buffalo, NY 01420 Phone: (800) 422-3340 Claim #: H8M4461001 Fax: (877) 786-5584 Workfile ID: 4533614a Estimate of Record Written By: MANNY GUZMAN, License Number: MA16433, 11/19/2018 11:19:54 AM Adjuster: SCAGLIARINI, TARA, (508) 946-6617 Business Insured: AMY LAVOIE Owner Policy #: PT5010A9952289672321 Claim #: H8M4461001 Type of Loss: Collision Date of'
    {'entities': [(0, 10, LABEL[0]), (224, 245, LABEL[2]), (246, 338, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA/RI REQUEST A SUPPLEMENT AT PARTNERS.GEICO.COM VISIT US ONLINE AT GEICO.COM 300 Crosspoint Pkwy Getzville, NY 14068 Phone: (774) 498-1769 Claim #: 0519656780101039-01 Fax: (866) 230-3992 Workfile ID: a9e2cf5a Estimate of Record Written By: DANIEL CROSS, License Number: 016568, 11/20/2018 9:03:02 AM Adjuster: ABTC Insured: Dimas Owner Policy #: 4373125972 Claim #: 0519656780101039-01 Echeverriamoreno Type of Loss: Collision Date of Loss: 11/15/2018 08:30 PM Days to Repair: 3 Point o'
    {'entities': [(0, 6, LABEL[0]), (201, 222, LABEL[2]), (223, 313, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA Request a supplement at partners.geico.com 300 Crosspoint Parkway Getzville, NY 14068 Phone: (774) 479-6773 Claim #: 0375008060101052-01 Fax: (716) 898-0542 Workfile ID: a3faf4d5 Estimate of Record Written By: ROWLAND GEMMA, License Number: 016543, 12/5/2018 10:34:37 AM Adjuster: Gemma, Rowland, (774) 479-6773 Cellular Insured: Diomer Ortiz Owner Policy #: 4489132706 Claim #: 0375008060101052-01 Type of Loss: Collision Date of Loss: 11/27/2018 06:00 PM Days to Repair: 7 Point of I'
    {'entities': [(0, 15, LABEL[0]), (172, 193, LABEL[2]), (194, 285, LABEL[1])]},
    ),
    (
    'TRAVELERS Auto Express Claim Center -244 Email Supplements:supplementrequest@travelers. com By Phone: (888) 299-7456 (Prompt 2) P.O. Box 4947 Orlando, FL 32802 Phone: (800) 422-3340 Claim #: H3R5142001 Fax: (877) 786-5584 Workfile ID: 943becf3 Estimate of Record Written By: MANNY GUZMAN, License Number: MA16433, 11/21/2018 9:34:01 AM Adjuster: MASTROIANNI, NICK, (860) 756-9626 Business Insured: MATTHEW Owner Policy #: PT5010A6017152802321 Claim #: H3R5142001 ROSSETTI Type of Loss: Collision Date'
    {'entities': [(0, 10, LABEL[0]), (222, 243, LABEL[2]), (244, 335, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY Westborough Claim Office Send supp to Matthew.Andreola@LibertyMutual.com P.O. Box 515097 Los Angeles, CA 90051 Claim #: 038811757-0002 Phone: (800) 251-7447 Workfile ID: 2a698452 Estimate of Record Written By: MATTHEW ANDREOLA, 12/17/2018 11:36:07 AM Adjuster: ZICHERMAN, TATYIANNA, (800) 251-7447 Business Insured: CAROLE WILLIAMS Owner Policy #: Claim #: 038811757-0002 Type of Loss: Liability Date of Loss: 12/12/2018 12:00 AM Days to Repair: 0 Point of Impact: 08'
    {'entities': [(0, 33, LABEL[0]), (190, 211, LABEL[2]), (212, 283, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA/RI Use Our Online Portal for Supplement Requests: partners.geico.com 300 Crosspoint Parkway Getzville, NY 14068 Claim #: 0451655090101010-01 Phone: (302) 235-9474 Workfile ID: ddc9f066 Estimate of Record Written By: JARED MCDOUGALL, License Number: 016478, 12/18/2018 11:58:09 AM Adjuster: CGZG Insured: Sahil Bhuptani Owner Policy #: 4495456578 Claim #: 0451655090101010-01 Type of Loss: Collision Date of Loss: 07/03/2018 04:15 PM Days to Repair: 10 Point of Impact: 07 Left Rear Ded'
    {'entities': [(0, 6, LABEL[0]), (178, 199, LABEL[2]), (200, 294, LABEL[1])]},
    ),
    (
    'METLIFE AUTO & HOME Auto Adjusting Unit - MA MA License #10729 P.O. Box 6040 Scranton, PA 18506 Phone: (800) 854-6011 x4736 Claim #: SLK91534 1 Fax: (866) 958-1610 Workfile ID: 6dbfeaa1 Estimate of Record Written By: ROBERT FOSTER, License Number: 10729, 1/2/2019 9:14:17 AM Adjuster: Pauquette, AIS, Stephanie, (800) 854-6011 x8587 Business Insured: JAMES AMPONG Owner Policy #: Claim #: SLK91534 1 Type of Loss: Collision Date of Loss: 12/28/2018 07:30 AM Days to Repair: 2 Point of Impact: 11 Left'
    {'entities': [(0, 20, LABEL[0]), (164, 185, LABEL[2]), (186, 274, LABEL[1])]},
    ),
    (
    'AMICA MUTUAL INSURANCE COMPANY CENTRAL MASSACHUSETTS OFFICE PO Box 9690 Providence, RI 02940 Phone: (888) 702-6422 Claim #: 60003410263-1-1 Fax: (866) 381-3239 Workfile ID: db344d54 Estimate of Record Written By: GARY LEDERER, License Number: 007629, 1/8/2019 9:32:14 AM Adjuster: ARCHAMBAULT, CHERYL, (877) 512-6422 x50185 Business Insured: Catherine Lui Owner Policy #: 99072822CU Claim #: 60003410263-1-1 Type of Loss: Collision Date of Loss: 01/03/2019 03:30 PM Days to Repair: 0 Point of Impact:'
    {'entities': [(0, 60, LABEL[0]), (160, 181, LABEL[2]), (182, 287, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0PX3J3-NYXMH8 Webster, MA 01570 Workfile ID: 8649c916 Estimate of Record Written By: MATTHEW P GODIN, License Number: 14333, 1/9/2019 10:30:43 AM Adjuster: GIAMPIETRO, VICTORIA, (800) 221-1605 Business Insured: ASHLEY SCIONTI Owner Policy #: Claim #: A0PX3J3-NYXMH8 Type of Loss: Collision Date of Loss: 01/04/2019 12:00 PM Days to Repair: 0 Point of '
    {'entities': [(0, 19, LABEL[0]), (181, 202, LABEL[2]), (203, 309, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0PXWVM-PAKKT9 Webster, MA 01570 Workfile ID: d82ad2be Estimate of Record Written By: MATTHEW P GODIN, License Number: 14333, 1/29/2019 10:30:39 AM Adjuster: HEATH, JAMIE, (800) 221-1605 Business Insured: TIFFANY LOUNICI Owner Policy #: Claim #: A0PXWVM-PAKKT9 Type of Loss: Collision Date of Loss: 01/11/2019 12:00 PM Days to Repair: 0 Point of Impact'
    {'entities': [(0, 19, LABEL[0]), (181, 202, LABEL[2]), (203, 316, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0PXWVM-PAKKT9 Webster, MA 01570 Workfile ID: d82ad2be Estimate of Record Written By: MATTHEW P GODIN, License Number: 14333, 1/29/2019 10:30:39 AM Adjuster: HEATH, JAMIE, (800) 221-1605 Business Insured: TIFFANY LOUNICI Owner Policy #: Claim #: A0PXWVM-PAKKT9 Type of Loss: Collision Date of Loss: 01/11/2019 12:00 PM Days to Repair: 0 Point of Impact'
    {'entities': [(0, 19, LABEL[0]), (181, 202, LABEL[2]), (203, 316, LABEL[1])]},
    ),
    (
    'USAA GENERAL INDEMNITY COMPANY Unit 1010 Please Visit us at USAA.com PO Box 33490 San Antonio, TX 78265 Phone: (800) 531-8722 Claim #: 031079855000000012001 Fax: (800) 531-8669 Workfile ID: 68da614f Estimate of Record Written By: RAYMOND CHAPIN, License Number: 014258, 1/8/2019 9:24:53 AM Insured: SGT KERVIN LIMA Owner Policy #: 031079855 Claim #: 031079855000000012001 Type of Loss: Liability Date of Loss: 01/03/2019 12:00 AM Days to Repair: 10 Point of Impact: 06 Rear Deductible: Owner (Insured'
    {'entities': [(0, 31, LABEL[0]), (177, 198, LABEL[2]), (199, 289, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 11 Gore Rd Webster, MA 01570 Claim #: A0PWRK6-NYNCM4 Phone: (800) 221-1605 Workfile ID: 333809ab Estimate of Record Written By: CAMIROLIS LANDESTOY, License Number: 16526, 1/2/2019 2:38:47 PM Adjuster: ROY, VANESSA, (800) 221-1605 x15771 Business Insured: RENE FUENTES Owner Policy #: Claim #: A0PWRK6-NYNCM4 Type of Loss: Collision Date of Loss: 12/21/2018 12:00 PM Days to Repair: 0 Point of Impact: 06 Rear D'
    {'entities': [(0, 19, LABEL[0]), (164, 185, LABEL[2]), (186, 220, LABEL[1])]},
    ),
    (
    'LM GENERAL INSURANCE COMPANY Westborough Claim Office Supplements: Michael.Williams04@libertymutual.com PO BOX 515097 LOS ANGELES, CA 90051 Claim #: 039135103-0001 Phone: (800) 332-3226 Workfile ID: d7952e37 Estimate of Record Written By: MICHAEL WILLIAMS, License Number: 016375, 1/30/2019 3:00:20 PM Adjuster: KIND, GARY, (800) 251-7447 Business Insured: BRIGHT BOATENG Owner Policy #: MARI Claim #: 039135103-0001 Type of Loss: COLL - Collision Date of Loss: 01/30/2019 12:00 AM Days to Repair: 2 '
    {'entities': [(0, 29, LABEL[0]), (186, 207, LABEL[2]), (208, 254, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA /RI Request a supplement at partners.geico.com partners.geico.com/gvbps/Logon.aspx 300 CROSSPOINT PARKWAY GETZVILLE, NY 14068 Phone: (617) 435-9984 Claim #: 0637558190101012-01 Fax: (855) 351-1481 Workfile ID: 7f4da877 Estimate of Record Written By: AMANDA AU, License Number: 016351, 11/8/2018 3:06:48 PM Adjuster: CDE8 Insured: Philippe Elie Owner Policy #: 4556289736 Claim #: 0637558190101012-01 Type of Loss: Collision Date of Loss: 08/26/2018 10:00 AM Days to Repair: 7 Point of '
    {'entities': [(0, 6, LABEL[0]), (212, 233, LABEL[2]), (234, 267, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA/RI For supplement requests, copy the link below: partners.geico.com 300 Crosspoint Pkwy Claim #: 0645043390101021-01 Getzville, NY 14068 Workfile ID: aa5c581d Estimate of Record Written By: BRIAN DOGNAZZI, License Number: 016653, 2/23/2019 11:12:44 AM Adjuster: ABY7 Insured: Thanh Cao Owner Policy #: 4568219408 Claim #: 0645043390101021-01 Type of Loss: Comprehensive Date of Loss: 02/14/2019 10:00 AM Days to Repair: 5 Point of Impact: 06 Rear Deductible: 500.00 Owner (Insured): In'
    {'entities': [(0, 6, LABEL[0]), (152, 173, LABEL[2]), (174, 266, LABEL[1])]},
    ),
    (
    'UNITED SERVICES AUTOMOBILE ASSOCIATION Unit 1010 Please Visit us at USAA.com PO Box 33490 San Antonio, TX 78265 Phone: (800) 531-8722 Claim #: 005396138000000024001 Fax: (800) 531-8669 Workfile ID: 4af79b55 Estimate of Record Written By: RAYMOND CHAPIN, License Number: 014258, 3/14/2019 8:22:21 AM Insured: COL CARL Owner Policy #: 005396138 Claim #: 005396138000000024001 GRAMSTORFF Type of Loss: Collision Date of Loss: 02/28/2019 12:00 AM Days to Repair: 3 Point of Impact: 06 Rear Deductible: 50'
    {'entities': [(0, 39, LABEL[0]), (185, 206, LABEL[2]), (207, 298, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 11 Gore Rd Webster, MA 01570 Claim #: A0PZX96-PHYHW7 Phone: (800) 221-1605 Workfile ID: 40458a7d Estimate of Record Written By: CAMIROLIS LANDESTOY, License Number: 16526, 3/20/2019 12:13:32 PM Adjuster: Webster_Main, (800) 221-1605 x15746 Business Insured: STEVEN GROCCIA Owner Policy #: V52811 Claim #: A0PZX96-PHYHW7 Type of Loss: Collision Date of Loss: 03/18/2019 12:00 PM Days to Repair: 0 Point of Impact'
    {'entities': [(0, 19, LABEL[0]), (164, 185, LABEL[2]), (186, 220, LABEL[1])]},
    ),
    (
    'LM GENERAL INSURANCE COMPANY Westborough Claim Office SUPPLEMENTS ONLY CALL 413-270-4294 ALL OTHER QUESTIONS CALL 800-252-5730 114 Turnpike Road Westborough, MA 01581 Claim #: 039495905-0001 Phone: (800) 251-7447 Workfile ID: c5591740 Estimate of Record Written By: RAUL SANTIAGO, License Number: 016490, 3/22/2019 10:27:22 AM Adjuster: BUFFONE, SARA, (800) 251-7447 Business Insured: ARMANDO VALOY Owner Policy #: MARI Claim #: 039495905-0001 Type of Loss: COLL - Collision Date of Loss: 03/19/2019 '
    {'entities': [(0, 29, LABEL[0]), (213, 234, LABEL[2]), (235, 326, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA/RI REQUEST A SUPPLEMENT AT partners.geico.com VISIT US ONLINE AT GEICO.COM 300 Cross Point Parkway Getzville, NY 14068 Phone: (508) 649-9544 Claim #: 0617122560101039-03 Fax: (855) 406-8211 Workfile ID: 145f46b1 Estimate of Record Written By: SCOTT MATTINGLY, License Number: 016676, 3/23/2019 10:00:48 AM Adjuster: ABYV Insured: James Kinsella Owner Policy #: Claim #: 0617122560101039-03 Type of Loss: Liability Date of Loss: 03/08/2019 03:00 AM Days to Repair: 7 Point of Impact: 02'
    {'entities': [(0, 6, LABEL[0]), (205, 226, LABEL[2]), (227, 320, LABEL[1])]},
    ),
    (
    'METLIFE AUTO & HOME Auto Adjusting Unit - MA MA License #10729 P.O. Box 6040 Scranton, PA 18506 Phone: (800) 854-6011 x4736 Claim #: SLL76469 3 Fax: (866) 958-1610 Workfile ID: 2f544f0b Estimate of Record Written By: ROBERT FOSTER, License Number: 10729, 6/14/2019 10:21:26 AM Adjuster: Lewis, Courtney, (800) 854-6011 x4648 Business Insured: Richard Oworae Owner Policy #: Claim #: SLL76469 3 Type of Loss: Liability Date of Loss: 03/31/2019 09:30 PM Days to Repair: 9 Point of Impact: 03 Right T-Bo'
    {'entities': [(0, 20, LABEL[0]), (164, 185, LABEL[2]), (186, 276, LABEL[1])]},
    ),
    (
    'GEICO REGION 08 For Supplement Requests copy the link below: //partners.geico.com/gvbps/Logon.aspx 300 CROSSPOINT PARKWAY GETZVILLE, NY 14068 Phone: (800) 841-3000 Claim #: 0545887900101041-01 Fax: (855) 221-2039 Workfile ID: c496964f Estimate of Record Written By: ROWLAND GEMMA, License Number: 016543, 4/16/2019 11:12:07 AM Adjuster: CGZE Insured: Justin Saboo Owner Policy #: 4412752687 Claim #: 0545887900101041-01 Type of Loss: Collision Date of Loss: 12/29/2017 10:30 PM Days to Repair: 10 Poi'
    {'entities': [(0, 6, LABEL[0]), (213, 234, LABEL[2]), (235, 326, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA/RI For Supplement Requests Copy The Link Below: Partners.geico.com 300 Crosspoint Parkway Getzville, NY 14068 Phone: (866) 345-0278 Claim #: 0489542430101072-02 Fax: (716) 898-0542 Workfile ID: f1f5eaf3 Estimate of Record Written By: ADAM EASTTY, License Number: 015878, 4/24/2019 3:27:21 PM Adjuster: F461 Insured: Hueming Mui Owner Policy #: Claim #: 0489542430101072-02 Type of Loss: Liability Date of Loss: 02/24/2019 08:35 AM Days to Repair: 3 Point of Impact: 12 Front Deductible'
    {'entities': [(0, 6, LABEL[0]), (196, 217, LABEL[2]), (218, 253, LABEL[1])]},
    ),
    (
    'THE CITIZENS INS CO OF AMERICA The Hanover Insurance Group-10 Syracuse Claim Office Office # 800-628-0250 251 Salina Meadows Parkway North Syracuse, NY 13212 Phone: (508) 667-7695 Claim #: 19-00-213228-1-1 Fax: (508) 635-8878 Workfile ID: 979fe45a Estimate of Record Written By: RICHARD COLE, License Number: 016107, 9/7/2018 9:58:26 AM Adjuster: WILSEY, CATHERINE Insured: Max Peloquin Owner Policy #: APNA431443 Claim #: 19-00-213228-1-1 Type of Loss: Collision Date of Loss: 05/10/2018 12:00 AM Da'
    {'entities': [(0, 31, LABEL[0]), (226, 247, LABEL[2]), (248, 336, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA/RI REQUEST A SUPPLEMENT AT partners.geico.com VISIT US ONLINE AT GEICO.COM 300 Cross Point Parkway Getzville, NY 14068 Phone: (508) 649-9544 Claim #: 0412612720101058-01 Fax: (855) 406-8211 Workfile ID: 3811055f Estimate of Record Written By: SCOTT MATTINGLY, License Number: 016676, 6/1/2019 11:41:17 AM Adjuster: AXX4 Insured: Yong-suk Jang Owner Policy #: 4505727976 Claim #: 0412612720101058-01 Type of Loss: Collision Date of Loss: 05/28/2019 03:00 PM Days to Repair: 2 Point of I'
    {'entities': [(0, 6, LABEL[0]), (205, 226, LABEL[2]), (227, 319, LABEL[1])]},
    ),
    (
    'GEICO REGION 08 For Supplement Requests copy the link below: //partners.geico.com/gvbps/Logon.aspx 300 CROSSPOINT PARKWAY GETZVILLE, NY 14068 Phone: (800) 841-3000 Claim #: 0596206760101082-04 Fax: (855) 221-2039 Workfile ID: 7314601d Estimate of Record Written By: ROWLAND GEMMA, License Number: 016543, 6/7/2019 8:38:30 AM Adjuster: CGZE Insured: Linda Comforti Owner Policy #: Claim #: 0596206760101082-04 Type of Loss: Liability Date of Loss: 05/31/2019 02:45 PM Days to Repair: 7 Point of Impact'
    {'entities': [(0, 6, LABEL[0]), (213, 234, LABEL[2]), (235, 324, LABEL[1])]},
    ),
    (
    'COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0R20JM-PMKXW0 Webster, MA 01570 Workfile ID: bc47c4ac Estimate of Record Written By: MATTHEW P GODIN, License Number: 14333, 5/20/2019 10:49:37 AM Adjuster: GIACOBBI, TRACI, (800) 221-1605 Business Insured: DEVON ODONNELL Owner Policy #: Claim #: A0R20JM-PMKXW0 Type of Loss: Collision Date of Loss: 05/17/2019 12:00 PM Days to Repair: 0 Point of Impa'
    {'entities': [(0, 19, LABEL[0]), (181, 202, LABEL[2]), (203, 295, LABEL[1])]},
    ),
    (
    'GEICO REGION 08 For Supplement Requests copy the link below: //partners.geico.com/gvbps/Logon.aspx 300 CROSSPOINT PARKWAY GETZVILLE, NY 14068 Phone: (800) 841-3000 Claim #: 0647298110101028-03 Fax: (855) 221-2039 Workfile ID: 8a97d6a1 Estimate of Record Written By: ROWLAND GEMMA, License Number: 016543, 6/11/2019 8:58:04 AM Adjuster: CGZE Insured: Sonia Ligsay Owner Policy #: Claim #: 0647298110101028-03 Type of Loss: Liability Date of Loss: 05/06/2019 07:20 PM Days to Repair: 10 Point of Impact'
    {'entities': [(0, 6, LABEL[0]), (213, 234, LABEL[2]), (235, 325, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA For Supplement Requests Copy The Link Below: partners.geico.com 300 Crosspoint Parkway Getzville, NY 14068 Phone: (774) 615-3431 Claim #: 0604314140101010-01 Fax: (855) 351-4261 Workfile ID: bd86d68b Estimate of Record Written By: DAVID CERUTI, License Number: 016536, 6/15/2019 9:29:56 AM Adjuster: F366 Insured: Naja Smith Owner Policy #: 4503378640 Claim #: 0604314140101010-01 Type of Loss: Collision Date of Loss: 06/02/2019 01:30 PM Days to Repair: 3 Point of Impact: 06 Rear Ded'
    {'entities': [(0, 15, LABEL[0]), (193, 214, LABEL[2]), (215, 304, LABEL[1])]},
    ),
    (
    'LIBERTY MUTUAL INSURANCE COMPANY Westborough Claim Office RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW 175 Berkeley Street Boston, MA 02116 Phone: (800) 251-7447 Claim #: 040263214-0001 Fax: (800) 507-3707 Workfile ID: 364f9faf Estimate of Record Written By: NATHAN WIKTOR, License Number: 016334, 6/28/2019 11:00:51 AM Adjuster: DERUSSO, ANTHONY, (800) 252-5730 x76490 Business Insured: DANIEL ROSARIO Owner Policy #: MARI Claim #: 040263214-0001 Type of Loss: COLL - Collision Date of Loss: 06'
    {'entities': [(0, 33, LABEL[0]), (211, 232, LABEL[2]), (233, 324, LABEL[1])]},
    ),
    (
    'TRAVELERS New England Claim Center (452) Email Supplements: supplementrequest@travelers.com By Phone: (888) 299-7456 P.O. BOX 430 Buffalo, NY 14240 Claim #: IBU0340001 Phone: (877) 411-0768 Workfile ID: 57295d23 Estimate of Record Written By: TRACY CRABTREE, License Number: MA14378, 6/19/2019 10:32:40 AM Adjuster: Kachinski, Jonathan, (978) 750-3402 Evening Insured: MAGEN BLAKE Owner Policy #: PT5010A6021692792321 Claim #: IBU0340001 Type of Loss: Collision Date of Loss: 06/09/2019 07:30 PM Days'
    {'entities': [(0, 10, LABEL[0]), (190, 211, LABEL[2]), (212, 305, LABEL[1])]},
    ),
    (
    'SAFETY INSURANCE COMPANY Boston **DTP Fax Line 617-502-2860** For Supplements call 617-951-0600 x7877 20 Custom House Street Boston, MA 02110 Phone: (800) 951-2100 Claim #: 2840559-01 Fax: (617) 535-5860 Workfile ID: 533af736 Estimate of Record Written By: WILLIAM PENNIMAN, License Number: 9570, 7/2/2019 1:58:36 PM Adjuster: Gorman, Sean, (617) 951-0600 x5463 Business Insured: KARA SELEN Owner Policy #: 8832932 Claim #: 2840559-01 Type of Loss: Collision Date of Loss: 06/26/2019 09:00 AM Days to'
    {'entities': [(0, 25, LABEL[0]), (204, 225, LABEL[2]), (226, 264, LABEL[1])]},
    ),
    (
    'USAA CASUALTY INSURANCE COMPANY Unit 1010 Please Visit us at USAA.com PO Box 33490 San Antonio, TX 78265 Phone: (800) 531-8722 Claim #: 003880606000000033001 Fax: (800) 531-8669 Workfile ID: 421fd6ca Estimate of Record Written By: RAYMOND CHAPIN, License Number: 014258, 10/11/2019 7:31:13 AM Insured: KENT MARKS Owner Policy #: 003880606 Claim #: 003880606000000033001 Type of Loss: Collision Date of Loss: 09/20/2019 12:00 AM Days to Repair: 5 Point of Impact: 12 Front Deductible: Owner (Insured):'
    {'entities': [(0, 32, LABEL[0]), (178, 199, LABEL[2]), (200, 292, LABEL[1])]},
    ),
    (
    'GEICO REGION 08 For Supplement Requests copy the link below: //partners.geico.com/gvbps/Logon.aspx 300 CROSSPOINT PARKWAY GETZVILLE, NY 14068 Phone: (800) 841-3000 Claim #: 0313402190101071-01 Fax: (855) 221-2039 Workfile ID: ac6132eb Estimate of Record Written By: ROWLAND GEMMA, License Number: 016543, 5/29/2019 8:57:58 AM Adjuster: CGZE Insured: Melanie Gnazzo Owner Policy #: 4082522048 Claim #: 0313402190101071-01 Type of Loss: Collision Date of Loss: 05/28/2019 12:45 PM Days to Repair: 10 Po'
    {'entities': [(0, 6, LABEL[0]), (213, 234, LABEL[2]), (235, 325, LABEL[1])]},
    ),
    (
    'UNITED SERVICES AUTOMOBILE ASSOCIATION Unit 1010 Please Visit us at USAA.com PO Box 33490 San Antonio, TX 78265 Phone: (800) 531-8722 Claim #: 002762751000000056001 Fax: (800) 531-8669 Workfile ID: 25039e5b Estimate of Record Written By: RAYMOND CHAPIN, License Number: 014258, 6/20/2019 11:33:14 AM Insured: MAJ DANA Owner Policy #: 002762751 Claim #: 002762751000000056001 BARRETTE Type of Loss: Collision Date of Loss: 06/14/2019 12:00 AM Days to Repair: 5 Point of Impact: 06 Rear Deductible: 500'
    {'entities': [(0, 39, LABEL[0]), (185, 206, LABEL[2]), (207, 299, LABEL[1])]},
    ),
    (
    'GEICO GEICO MA/RI EMAIL SUPPLEMENTS TO R8ADSUPPMARI@GEICO.COM Visit us Online @ www.geico.com 300 Crosspoint Pkwy Getzville, NY 14068 Claim #: 0540767940101034-01 Phone: (781) 206-9368 Workfile ID: dade7d1f Estimate of Record Written By: MICHAEL MORRISON, License Number: 016398, 5/9/2017 9:39:32 AM Adjuster: ABTI Insured: Kevin Yeung Owner Policy #: 4405011737 Claim #: 0540767940101034-01 Type of Loss: Collision Date of Loss: 05/01/2017 10:50 AM Days to Repair: 4 Point of Impact: 09 Left T-Bone '
    {'entities': [(0, 6, LABEL[0]), (185, 206, LABEL[2]), (207, 299, LABEL[1])]},
    ),
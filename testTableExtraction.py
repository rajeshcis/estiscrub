#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 10:25:37 2020

@author: aman
"""

# import tabula
import numpy as np
import pandas as pd
import re

# file_1 = "/media/aman/698F6E1531797531/Data/rajeshcis-estiscrub-7fa744c289f6/PDF_data/8559 COTE s1.pdff"
# file = "PDF_data/7759 westerlind original.pdf"

# tables = tabula.read_pdf(file, pages='all')
# tables_1 = tabula.read_pdf(file_1, pages='all')


# tables = tabula.read_pdf(
#     "/media/aman/698F6E1531797531/Data/rajeshcis-estiscrub-7fa744c289f6/PDF_data/8095 JOLLY s1.pdf", 
#     pages='all')


import camelot

# file = "PDF_data/8063 VU oe.pdf"
file = "latest_files/10073 DALY s1_MAPFRE_CCC.pdf"
# file = "PDF_data/8559 COTE s1.pdf"

tables = camelot.read_pdf(file, flavor='stream', pages='1,2,3,4')

ab = tables[2].df

cols = [col for col in ab.iloc[0]]

ab = ab.drop([0, 1])
# empty_cols = [col for col in ab.columns if ab[col].isnull().all()]
# # Drop these columns from the dataframe
# df.drop(empty_cols,
#         axis=1,
#         inplace=True)

ab.columns = cols

def tab1(tables):
    for e in tables:
        for f in e.iloc[0]:
            try:
                if "Line" in f or "Line" in e:
                    return e
            except:
                pass
            try:
                if "Line" in e:
                    return e
            except:
                pass
                
ab = tab1(tables)

def tab2(tables):
    for e in tables:
        for f in e.iloc[0]:
            try:
                if "ESTIMATE TOTALS" in f or "ESTIMATE TOTALS" in e:
                    return e
            except:
                pass
            try:
                if "ESTIMATE TOTALS" in e:
                    return e
            except:
                pass
            
bd = tab2(tables)


        
# ab_1 = tables_1[0]

# ab.columns = [''] * len(ab.columns)
# ab_1.columns = [''] * len(ab_1.columns)

for i, e in enumerate(ab.iloc[0]):
    try:
        if np.isnan(e):
            ab.iloc[0, i] = "empty"
    except:
        ab.iloc[0, i] = e
# ab.iloc[0] = ab.iloc[0].replace(np.nan, "empty", regex=True)
# ab_1.iloc[0] = ab_1.iloc[0].replace(np.nan, "empty", regex=True)

# cols_1 = list(ab_1.iloc[0])
if "Line" in ab.columns:
    cols = list(ab.columns)
else:
    ab.columns =  ab.iloc[0]
    cols = list(ab.iloc[0])
    
    
col_new = []
for i in cols:
    if "Description" in i:
        print(len(i.split(" ")))
        print(i.split(" "))
        # j = i.split()
        for k in i.split():
            if k == "Number":
                pass
            else:
                col_new.append(k)
    else:
        j = i
        col_new.append(j)

k = []
for i, j in enumerate(ab.columns):
    if j == "empty":
        k.append(ab.columns[i+1])
    else:
        k.append(j)
        
ab.columns = [ i for i in k]

# for i in cols_1:

#     if "Description" in i:
#         print(len(i.split(" ")))
#         print(i.split(" "))



    
# cols = ab.iloc[0]

abc = pd.DataFrame(columns=col_new)

all_dic = {}
for i in range(8):
    all_dic[i] = {}
# for table in tables:
for m, e in enumerate(ab.columns):
    import pdb; pdb.set_trace()
    if "Line" in e:
        i = 1
        # tatal_len = len(ab.loc[:, e])
        for f in ab.iloc[:, m]:
            if (type(f) == str or type(f) == int) and re.search(r"(\d+\s|\d+)", f):
                line = re.search(r"(\d+\s|\d+)", f).group()
                all_dic[0][i] = line
            i += 1
    if "Oper" in e:
        i = 1
        for f in ab.iloc[:, m]:
            if type(f) == str and re.search(r"(R&I|Subl|Rpr|Repl|Algn|Blnd|Refn)", f):
                oper = re.search(r"(R&I|Subl|Rpr|Repl|Algn|Blnd|Refn)", f).group()
                all_dic[1][i] = oper
            i += 1
    if "Description" in e:
        i = 1
        for f in ab.iloc[:, m]:
            if type(f) == str and re.search(r"[-,\./\w\s]+(\s[A-Z0-9]{10,12}|\s\d)", f):
                desc = re.search(r"[-,\./\w\s]+(\s[A-Z0-9]{10,12}|\s\d)", f).group()
                if re.search(r"[A-Z0-9]{10,12}", desc):
                    desc = re.sub(r"[A-Z0-9]{10,12}[\.\d]*", "", desc)
                all_dic[2][i] = desc
            i += 1
    if "Part" in e:
        i = 1
        # import pdb; pdb.set_trace()
        for f in ab.iloc[:, m]:
            if type(f) == str and re.search(r"[A-Z0-9]{10,12}", f):
                part = re.search(r"[A-Z0-9]{10,12}", f).group()
                all_dic[3][i] = part
            i += 1
    if "Qty" in e:
        i = 1
        for f in ab.iloc[:, m]:
            if type(f) == str and re.search(r"\d", f):
                qty = re.search(r"\d", f).group()
                all_dic[4][i] = qty
            i += 1
    if "Extended" in e:
        i = 1
        for f in ab.iloc[:, m]:
            if type(f) == str and re.search(r"\d+\.\d+", f):
                extd = re.search(r"\d+\.\d+", f).group()
                all_dic[5][i] = extd
            i += 1
    if "Labor" in e:
        i = 1
        for f in ab.iloc[:, m]:
            if type(f) == str and re.search(r"(\d\.\d|Incl\.)", f):
                labr = re.search(r"(\d\.\d|Incl\.)", f).group()
                all_dic[6][i] = labr
            i += 1
    if "Paint" in e:
        i = 1
        for f in ab.iloc[:, m]:
            if type(f) == str and re.search(r"(\d\.\d|\-\d\.\d)", f):
                paint = re.search(r"(\d\.\d|\-\d\.\d)", f).group()
                all_dic[7][i] = paint
            i += 1

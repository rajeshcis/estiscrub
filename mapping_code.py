#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 22:28:29 2020

@author: aman
"""

import os
import pdftotext
import re
import copy

files_list = os.listdir("PDF_data")

training_data = []
new_data = []
file_name = []
for f in files_list:
    file = open("PDF_data/" + f, 'rb')
    try:
        pdf = pdftotext.PDF(file)
        file_name.append(f)
        for i in range(len(pdf)):
            if "CCC ONE Estimating" in pdf[i]:
                # import pdb; pdb.set_trace()
                df = re.sub(r"^\s+", "", pdf[0], flags=re.MULTILINE)
                # df = df.replace('\n', ' ')
                df = re.sub("\s{2,}", " ", df, flags=re.MULTILINE)
                training_data.append(df)
                df = df.replace('\n', ' ')
                wfid = re.search(r"\bWorkfile ID: \w{8}\b", df).span()
                wfid_t = df[wfid[0]:wfid[1]]
                df = re.sub(r"\b Workfile ID: \w{8}\b", "", df)
                if re.search(r"\bClaim #: [-\w]+\b ", df[:wfid[0]]):
                    df = re.sub(r"\b Claim #: [-\w]+\b", "", df)
                df = wfid_t + " " + df
                new_data.append(df)
                new_data_org = copy.copy(new_data)
                
            else:
                continue
            
    except Exception as e:
        continue


list_cord = {}
heading = {}
esti_type = {}
for_data = {}
diction = {}
spc = 0
spc1 = 0
j = 0
n = 0
for i in range(len(new_data)):
# timing = 0
# while timing==0:
#     i = 148
    try:
        if re.search(r"(Estimate|Supplement) [\w]+ (Record|Summary)", new_data[i]):
            # print(new_data[i])
            # import pdb; pdb.set_trace()
            # print("\n\n\n")
            a = re.search(r"\bWorkfile ID: \w{8}\b", new_data[i]).span()
            list_cord[i] = a
            dist = a[1] - a[0]
            new_data[i] = re.sub(r"\bWorkfile ID: \w{8}\b", " "*dist, new_data[i])
            # re.sub(r"\bWorkfile ID: \w{8}\b ", "", new_data[i])
            # if re.search(r"\bClaim #: [-\w]+\b ", new_data[i][:a[0]]):
            #     clai = re.search(r"\bClaim #: [-\w]+\b ", new_data[i][:a[0]]).span()
            #     clai_dist = clai[1] - clai[0]
            #     new_data[i] = re.sub(r"\bClaim #: [-\w]+\b ", " "*clai_dist, new_data[i])
            # if re.search(r"[-:&.|#,\s\w]+([-()\s\d]+|For:|Estimate|Supplement)", new_data[i]):
            # if re.search(r"[-:&.|#,()\s\w]+\s(Fax:|FAX:)[-()\d\s]+", new_data[i]):
            #     yx = re.search(r"[-:&.|#,()\s\w]+\s(Fax:|FAX:)[-()\d\s]+", new_data[i]).span()
            #     xy = (yx[0], yx[1]-1)
            #     if re.findall(r"\s{3,}", new_data[i][:300]):
            #         if len(re.findall(r"\s{3,}", new_data[i][:300])) == 2:
            #             sp = re.search(r"\s{3,}", new_data[i][:300]).span()
            #             spc = (sp[0]+1, sp[1]-1)
            #             sp1 = re.search(r"\s{3,}", new_data[i][sp[1]:300]).span()
            #             spc1 = (sp1[0]+sp[1]+1, sp1[1]+sp[1]-1)
            #         elif len(re.findall(r"\s{3,}", new_data[i][:300])) == 1:
            #             sp = re.search(r"\s{3,}", new_data[i]).span()
            #             spc = (sp[0]+1, sp[1]-1)
            
            #         if spc1:
            #             if xy[1] <= spc[0]:
            #                 b = [(xy[0], xy[1])]
            #             else:
            #                 if xy[1] >= spc[1]:
            #                     if spc1[0] <= spc[0]:
            #                         b = [(xy[0], spc1[0]), (spc1[1], spc[0]), (spc[1], xy[1])]
            #                     else:
            #                         b = [(xy[0], spc[0]), (spc[1], spc1[0]), (spc1[1], xy[1])]
            #                 else:
            #                     if spc1[0] <= spc[0]:
            #                         b = [(xy[0], spc1[0]), (spc1[1], spc[0])]
            #                     else:
            #                         b = [(xy[0], spc[0]), (spc[1], spc1[0])]
            
            #         else:
            #             if xy[1] <= spc[0]:
            #                 b = [(xy[0], xy[1])]
            
            #             else:
            #                 if xy[1] >= spc[1]:
            #                     b = [(xy[0], spc[0]), (spc[1], xy[1])]
            #                 else:
            #                     b = [(xy[0], spc[0])]
            
            # elif re.search(r"[-:&.|#,\s\w]+\s[-()\d\s]+", new_data[i]):
            #     xy = re.search(r"[-:&.|#,\s\w]+\s[-()\d\s]+", new_data[i]).span()
            #     if re.findall(r"\s{3,}", new_data[i][:300]):
            #         if len(re.findall(r"\s{3,}", new_data[i][:300])) == 2:
            #             sp = re.search(r"\s{3,}", new_data[i][:300]).span()
            #             spc = (sp[0]+1, sp[1]-1)
            #             sp1 = re.search(r"\s{3,}", new_data[i][sp[1]:300]).span()
            #             spc1 = (sp1[0]+sp[1]+1, sp1[1]+sp[1]-1)
            #         elif len(re.findall(r"\s{3,}", new_data[i][:300])) == 1:
            #             sp = re.search(r"\s{3,}", new_data[i]).span()
            #             spc = (sp[0]+1, sp[1]-1)
            
            #         if spc1:
            #             if xy[1] <= spc[0]:
            #                 b = [(xy[0], xy[1])]
            #             else:
            #                 if xy[1] >= spc[1]:
            #                     if spc1[0] <= spc[0]:
            #                         b = [(xy[0], spc1[0]), (spc1[1], spc[0]), (spc[1], xy[1])]
            #                     else:
            #                         b = [(xy[0], spc[0]), (spc[1], spc1[0]), (spc1[1], xy[1])]
            #                 else:
            #                     if spc1[0] <= spc[0]:
            #                         b = [(xy[0], spc1[0]), (spc1[1], spc[0])]
            #                     else:
            #                         b = [(xy[0], spc[0]), (spc[1], spc1[0])]
            
            #         else:
            #             if xy[1] <= spc[0]:
            #                 b = [(xy[0], xy[1])]
            
            #             else:
            #                 if xy[1] >= spc[1]:
            #                     b = [(xy[0], spc[0]), (spc[1], xy[1])]
            #                 else:
            #                     b = [(xy[0], spc[0])]
            
            if re.search(r"[-:@%!$&./|#,()\s\w]+(Estimate|Suppleme)", new_data[i]):
                yx = re.search(r"[-:@%!$&./|#,()\s\w]+(Estimate|Suppleme)", new_data[i]).span()
                if re.findall(r"\s{3,}", new_data[i][:300]):
                    xy = (yx[0]+21, yx[1]-9)

                    # if len(re.findall(r"\s{3,}", new_data[i][:300])) == 2:
                    #     sp = re.search(r"\s{3,}", new_data[i][:300]).span()
                    #     spc = (sp[0]+1, sp[1]-1)
                    #     sp1 = re.search(r"\s{3,}", new_data[i][sp[1]:300]).span()
                    #     spc1 = (sp1[0]+sp[1]+1, sp1[1]+sp[1]-1)
                    # if len(re.findall(r"\s{3,}", new_data[i][:300])) == 1:
                    #     sp = re.search(r"\s{3,}", new_data[i]).span()
                    #     spc = (sp[0]+1, sp[1]-1)
                    
                    # if spc1:
                    #     if xy[1] <= spc[0]:
                    #         b = [(xy[0], xy[1])]
                    #     else:
                    #         if xy[1] >> spc1[1]:
                                    
                    #                 # b = [(xy[0], spc1[0]), (spc1[1], spc[0]), (spc[1], xy[1])]
                    #             b = [(xy[0], spc[0]), (spc[1], spc1[0]), (spc1[1], xy[1])]
                    #         else:
                    #             b = [(xy[0], spc[0]), (spc[1], spc1[0])]
                    #         # else:
                    #         #     if spc1[0] <= spc[0]:
                    #         #         b = [(xy[0], spc1[0]), (spc1[1], spc[0])]
                    #         #     else:
                    #         #         b = [(xy[0], spc[0]), (spc[1], spc1[0])]
                    
                    # else:
                    #     if xy[1] <= spc[0]:
                    #         b = [(xy[0], xy[1])]
                        
                    #     else:
                    #         if xy[1] >> spc[1]:
                    #             b = [(xy[0], spc[0]), (spc[1], xy[1])]
                    #         else:
                    #             b = [(xy[0], spc[0])]
                    
                    sp = re.search(r"\s{3,}", new_data[i][:350]).span()
                    spc = (sp[0], sp[1])
                    b = [sp[1], xy[1]]
                    import pdb; pdb.set_trace()
            
            heading[i] = b
            c = re.search(r"(Estimate of Record|Supplement of Record 1 with Summary|Supplement of Record 2 with Summary)", new_data[i]).span()
            esti_type[i] = c
            # d = re.search(r"For: [-:&,\s\w]+(Estimate|Supplement)", new_data[i]).span()
            # for_data.append(d)
            j+=1
            # e = new_data[i]
            # fd = new_data[i][b[0]:b[1]]
            # if re.search(r"For:\s[-:&|,\s\w()]+(Estimate|Suppleme)", new_data[i]):
            #     d = re.search(r"For: [-:&|,\s\w()]+(Estimate|Suppleme)", new_data[i]).span()
            #     x , y = d
            #     d = (x, y-9)
            #     for_data.append(d)
            #     diction[i] = [a, b, c, d]
            #     # j += 1
            # else:
            if heading[i][-1][1] >> esti_type[i][0]:
                pass
            else:
                diction[i] = [a, b, c]
                # j += 1
        spc = 0
        spc1 = 0
        # timing += 1
    except:
        n += 1
        continue
# list_cord = []
# heading = []
# esti_type = []
# for_data = []
# diction = {}
# spc = 0
# spc1 = 0
# j = 0
# n = 0
# for i in range(len(new_data)):
# # timing = 0
# # while timing==0:
# #     i = 148
#     try:
#         if re.search(r"(Estimate|Supplement) [\w]+ (Record|Summary)", new_data[i]):
#             # print(new_data[i])
#             # import pdb; pdb.set_trace()
#             # print("\n\n\n")
#             a = re.search(r"\bWorkfile ID: \w{8}\b", new_data[i]).span()
#             list_cord.append(a)
#             dist = a[1] - a[0]
#             new_data[i] = re.sub(r"\bWorkfile ID: \w{8}\b", " "*dist, new_data[i])
#             # re.sub(r"\bWorkfile ID: \w{8}\b ", "", new_data[i])
#             if re.search(r"\bClaim #: [-\w]+\b ", new_data[i]):
#                 clai = re.search(r"\bClaim #: [-\w]+\b ", new_data[i]).span()
#                 clai_dist = clai[1] - clai[0]
#                 new_data[i] = re.sub(r"\bClaim #: [-\w]+\b ", " "*clai_dist, new_data[i])
#             # if re.search(r"[-:&.|#,\s\w]+([-()\s\d]+|For:|Estimate|Supplement)", new_data[i]):
#             if re.search(r"[-:&.|#,()\s\w]+\s(Fax:|FAX:)[-()\d\s]+", new_data[i]):
#                 yx = re.search(r"[-:&.|#,()\s\w]+\s(Fax:|FAX:)[-()\d\s]+", new_data[i]).span()
#                 xy = (yx[0], yx[1]-1)
#                 if re.findall(r"\s{3,}", new_data[i][:300]):
#                     if len(re.findall(r"\s{3,}", new_data[i][:300])) == 2:
#                         sp = re.search(r"\s{3,}", new_data[i][:300]).span()
#                         spc = (sp[0]+1, sp[1]-1)
#                         sp1 = re.search(r"\s{3,}", new_data[i][sp[1]:300]).span()
#                         spc1 = (sp1[0]+sp[1]+1, sp1[1]+sp[1]-1)
#                     elif len(re.findall(r"\s{3,}", new_data[i][:300])) == 1:
#                         sp = re.search(r"\s{3,}", new_data[i]).span()
#                         spc = (sp[0]+1, sp[1]-1)

#                     if spc1:
#                         if xy[1] <= spc[0]:
#                             b = [(xy[0], xy[1])]
#                         else:
#                             if xy[1] >= spc[1]:
#                                 if spc1[0] <= spc[0]:
#                                     b = [(xy[0], spc1[0]), (spc1[1], spc[0]), (spc[1], xy[1])]
#                                 else:
#                                     b = [(xy[0], spc[0]), (spc[1], spc1[0]), (spc1[1], xy[1])]
#                             else:
#                                 if spc1[0] <= spc[0]:
#                                     b = [(xy[0], spc1[0]), (spc1[1], spc[0])]
#                                 else:
#                                     b = [(xy[0], spc[0]), (spc[1], spc1[0])]
                                
#                     else:
#                         if xy[1] <= spc[0]:
#                             b = [(xy[0], xy[1])]
                            
#                         else:
#                             if xy[1] >= spc[1]:
#                                 b = [(xy[0], spc[0]), (spc[1], xy[1])]
#                             else:
#                                 b = [(xy[0], spc[0])]
                            
#             # elif re.search(r"[-:&.|#,\s\w]+\s[-()\d\s]+", new_data[i]):
#             #     xy = re.search(r"[-:&.|#,\s\w]+\s[-()\d\s]+", new_data[i]).span()
#             #     if re.findall(r"\s{3,}", new_data[i][:300]):
#             #         if len(re.findall(r"\s{3,}", new_data[i][:300])) == 2:
#             #             sp = re.search(r"\s{3,}", new_data[i][:300]).span()
#             #             spc = (sp[0]+1, sp[1]-1)
#             #             sp1 = re.search(r"\s{3,}", new_data[i][sp[1]:300]).span()
#             #             spc1 = (sp1[0]+sp[1]+1, sp1[1]+sp[1]-1)
#             #         elif len(re.findall(r"\s{3,}", new_data[i][:300])) == 1:
#             #             sp = re.search(r"\s{3,}", new_data[i]).span()
#             #             spc = (sp[0]+1, sp[1]-1)

#             #         if spc1:
#             #             if xy[1] <= spc[0]:
#             #                 b = [(xy[0], xy[1])]
#             #             else:
#             #                 if xy[1] >= spc[1]:
#             #                     if spc1[0] <= spc[0]:
#             #                         b = [(xy[0], spc1[0]), (spc1[1], spc[0]), (spc[1], xy[1])]
#             #                     else:
#             #                         b = [(xy[0], spc[0]), (spc[1], spc1[0]), (spc1[1], xy[1])]
#             #                 else:
#             #                     if spc1[0] <= spc[0]:
#             #                         b = [(xy[0], spc1[0]), (spc1[1], spc[0])]
#             #                     else:
#             #                         b = [(xy[0], spc[0]), (spc[1], spc1[0])]
                                
#             #         else:
#             #             if xy[1] <= spc[0]:
#             #                 b = [(xy[0], xy[1])]
                            
#             #             else:
#             #                 if xy[1] >= spc[1]:
#             #                     b = [(xy[0], spc[0]), (spc[1], xy[1])]
#             #                 else:
#             #                     b = [(xy[0], spc[0])]
                            
#             elif re.search(r"[-:&.|#,()\s\w]+\s(For:|Esti|Supp)", new_data[i]):
#                 yx = re.search(r"[-:&.|#,()\s\w]+\s(For:|Esti|Supp)", new_data[i]).span()
#                 xy = (yx[0], yx[1]-5)
#                 if re.findall(r"\s{3,}", new_data[i][:300]):
#                     if len(re.findall(r"\s{3,}", new_data[i][:300])) == 2:
#                         sp = re.search(r"\s{3,}", new_data[i][:300]).span()
#                         spc = (sp[0]+1, sp[1]-1)
#                         sp1 = re.search(r"\s{3,}", new_data[i][sp[1]:300]).span()
#                         spc1 = (sp1[0]+sp[1]+1, sp1[1]+sp[1]-1)
#                     elif len(re.findall(r"\s{3,}", new_data[i][:300])) == 1:
#                         sp = re.search(r"\s{3,}", new_data[i]).span()
#                         spc = (sp[0]+1, sp[1]-1)

#                     if spc1:
#                         if xy[1] <= spc[0]:
#                             b = [(xy[0], xy[1])]
#                         else:
#                             if xy[1] >= spc[1]:
#                                 if spc1[0] <= spc[0]:
#                                     b = [(xy[0], spc1[0]), (spc1[1], spc[0]), (spc[1], xy[1])]
#                                 else:
#                                     b = [(xy[0], spc[0]), (spc[1], spc1[0]), (spc1[1], xy[1])]
#                             else:
#                                 if spc1[0] <= spc[0]:
#                                     b = [(xy[0], spc1[0]), (spc1[1], spc[0])]
#                                 else:
#                                     b = [(xy[0], spc[0]), (spc[1], spc1[0])]
                                
#                     else:
#                         if xy[1] <= spc[0]:
#                             b = [(xy[0], xy[1])]
                            
#                         else:
#                             if xy[1] >= spc[1]:
#                                 b = [(xy[0], spc[0]), (spc[1], xy[1])]
#                             else:
#                                 b = [(xy[0], spc[0])]
                            
#                 # b = re.search(r"[-:&,\s\w]+([-()\s\d]+|For:|Estimate|supplement)", new_data[i]).span()
#             heading.append(b)
#             c = re.search(r"(Estimate of Record|Supplement of Record 1 with Summary|Supplement of Record 2 with Summary)", new_data[i]).span()
#             esti_type.append(c)
#             # d = re.search(r"For: [-:&,\s\w]+(Estimate|Supplement)", new_data[i]).span()
#             # for_data.append(d)
#             # j+=1
#             # e = new_data[i]
#             # fd = new_data[i][b[0]:b[1]]
#             if re.search(r"For:\s[-:&|,\s\w()]+(Estimate|Suppleme)", new_data[i]):
#                 d = re.search(r"For: [-:&|,\s\w()]+(Estimate|Suppleme)", new_data[i]).span()
#                 x , y = d
#                 d = (x, y-9)
#                 for_data.append(d)
#                 diction[i] = [a, b, c, d]
#                 # j += 1
#             else:
#                 diction[i] = [a, b, c]
#                 # j += 1
#         spc = 0
#         spc1 = 0
#         # timing += 1
#     except:
#         n += 1
#         continue
            


with open("train_data_2.py", "w") as fin:
    fin.write("""LABEL = ["InsCo", "Estimate Type", "Workfile_id", "IndApprCo"]
# training data
# Note: If you're using an existing model, make sure to mix in examples of
# other entity types that spaCy correctly recognized before. Otherwise, your
# model might learn the new type, but "forget" what it previously knew.
# https://explosion.ai/blog/pseudo-rehearsal-catastrophic-forgetting
TRAIN_DATA = [""")
    for e in diction.keys():
        if len(diction[e]) == 3:
            fin.write("(")
            fin.write("\n")
            # dt = [diction[e][1][0]:diction[e][1][1]]
            fin.write("'''")
            fin.write(str(e))
            fin.write(new_data_org[e][0:800])
            fin.write("'''")
            fin.write(",")
            fin.write("\n")
            fin.write("{")
            w, x, y, z = diction[e][0][0], diction[e][0][1], diction[e][2][0], diction[e][2][1]
            if len(diction[e][1]) == 3:
                u1, v1, u2, v2, u3, v3 = diction[e][1][0][0], diction[e][1][0][1], diction[e][1][1][0], diction[e][1][1][1], diction[e][1][2][0], diction[e][1][2][1]
                fin.write(f"'entities': [({u1}, {v1}, LABEL[0]), ({u2}, {v2}, LABEL[0]), ({u3}, {v3}, LABEL[0]), ({w}, {x}, LABEL[2]), ({y}, {z}, LABEL[1])]")
            elif len(diction[e][1]) == 2:
                u1, v1, u2, v2 = diction[e][1][0][0], diction[e][1][0][1], diction[e][1][1][0], diction[e][1][1][1]
                fin.write(f"'entities': [({u1}, {v1}, LABEL[0]), ({u2}, {v2}, LABEL[0]), ({w}, {x}, LABEL[2]), ({y}, {z}, LABEL[1])]")
                
            elif len(diction[e][1]) == 1:
                u1, v1 = diction[e][1][0][0], diction[e][1][0][1]
                fin.write(f"'entities': [({u1}, {v1}, LABEL[0]), ({w}, {x}, LABEL[2]), ({y}, {z}, LABEL[1])]")
            
            fin.write("},")
            fin.write("\n")
            fin.write("),")
            fin.write("\n")
        elif len(diction[e]) == 4:
            fin.write("(")
            fin.write("\n")
            # dt = [diction[e][1][0]:diction[e][1][1]]
            fin.write("'''")
            fin.write(str(e))
            fin.write(new_data_org[e][0:800])
            fin.write("'''")
            fin.write(",")
            fin.write("\n")
            fin.write("{")
            w, x, y, z, p, q = diction[e][0][0], diction[e][0][1], diction[e][2][0], diction[e][2][1], diction[e][3][0], diction[e][3][1]
            # fin.write(f"'entities': [({u}, {v}, LABEL[0]), ({w}, {x}, LABEL[2]), ({y}, {z}, LABEL[1]), ({p}, {q}, LABEL[3])]")
            if len(diction[e][1]) == 3:
                u1, v1, u2, v2, u3, v3 = diction[e][1][0][0], diction[e][1][0][1], diction[e][1][1][0], diction[e][1][1][1], diction[e][1][2][0], diction[e][1][2][1]
                fin.write(f"'entities': [({u1}, {v1}, LABEL[0]), ({u2}, {v2}, LABEL[0]), ({u3}, {v3}, LABEL[0]), ({w}, {x}, LABEL[2]), ({y}, {z}, LABEL[1]), ({p}, {q}, LABEL[3])]")
            elif len(diction[e][1]) == 2:
                u1, v1, u2, v2 = diction[e][1][0][0], diction[e][1][0][1], diction[e][1][1][0], diction[e][1][1][1]
                fin.write(f"'entities': [({u1}, {v1}, LABEL[0]), ({u2}, {v2}, LABEL[0]), ({w}, {x}, LABEL[2]), ({y}, {z}, LABEL[1]), ({p}, {q}, LABEL[3])]")
                
            elif len(diction[e][1]) == 1:
                u1, v1 = diction[e][1][0][0], diction[e][1][0][1]
                fin.write(f"'entities': [({u1}, {v1}, LABEL[0]), ({w}, {x}, LABEL[2]), ({y}, {z}, LABEL[1]), ({p}, {q}, LABEL[3])]")
                
            fin.write("},")
            fin.write("\n")
            fin.write("),")
            fin.write("\n")
    fin.write("]")
from django.contrib import admin
from .models import (Estimation, EstimationTotals, EstimationLine, InsuranceCompany, 
        InsuranceAppraisalCompany, Audatex_Owner, DamagesTable, EstimateTable,
        MitchelEstimation, MitchelEstimationTable)
from .models import CustomUser

# Register your models here.
admin.site.register(Estimation)
admin.site.register(EstimationTotals)
admin.site.register(EstimationLine)
admin.site.register(Audatex_Owner)
admin.site.register(DamagesTable)
admin.site.register(EstimateTable)
admin.site.register(InsuranceCompany)
admin.site.register(InsuranceAppraisalCompany)
admin.site.register(CustomUser)
admin.site.register(MitchelEstimation)
admin.site.register(MitchelEstimationTable)

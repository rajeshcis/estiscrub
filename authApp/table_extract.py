#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 20:22:07 2020

@author: aman
"""


import pdftotext
import re

dfd = []

file = open("/media/aman/698F6E1531797531/Data/rajeshcis-estiscrub-7fa744c289f6/PDF_data/7759 westerlind original.pdf", "rb")
pdf = pdftotext.PDF(file)
for pd in pdf:
    df = re.sub(r"^\s+", "", pd, flags=re.MULTILINE)
    df = re.sub("\s{2,}", ",", df, flags=re.MULTILINE)
    dfd.append(df)
    # df = df.replace('\n', '   ')

page= []
total = ""
for e in dfd:
    total = total + "\n" + e       
       
if re.search(r"Line,Oper,Description,Part Number,Qty,Extended,Labor,Paint\n[-,$|\.\'\*/&:\#\>\<\w\s\n\t]+SUBTOTALS,(\d+,\d+|\d+)\.\d+,\d+\.\d+,\d+\.\d+", total):
    val = re.search(r"Line,Oper,Description,Part Number,Qty,Extended,Labor,Paint\n[-,$|\.\'\*/&:\#\>\<\w\s\n\t]+SUBTOTALS,(\d+,\d+|\d+),\d+\.\d+,\d+\.\d+", total).group()    

val_lis = val.split("\n")


fav_lis = []
for e in val_lis:
    if e != "\n" and len(e) != 0 and e [1] != "/" and e [2] != "/" and not e[:4].isnumeric():
        if e[0].isnumeric() or e[:9] == "SUBTOTALS":
            fav_lis.append(e)
            
#Line,Oper,Description,Part Number,Qty,Extended,Labor,Paint
all_dict = {}
for e in fav_lis[:-1]:
    e = e+"\n"
    try:
        line = re.search(r"\d+,", e).group()
        line = line[:-1]
        e = re.sub(r"\d+,", "", e)
        all_dict[line] = {}
        import pdb; pdb.set_trace()
        if re.search(r"([A-Z\s]+|[\<\>\*\#S01]+),*\n", e):
            head = re.search(r"([A-Z\s]+|[\<\>\*\#S01]+),*\n", e).group()
            head = head[:-1]
            all_dict[line]["Head"] = head
        if re.search(r",[A-Za-z\&]+,", e):
            oper = re.search(r",[A-Za-z\&]+,", e).group()
            all_dict[line]["Oper"] = oper
        # if re.search(r"[-,\./\w\s]+,", e):
        #     descrip = re.search(r"[-,\./\w\s]+,", e).group()
        #     all_dict[line]["Description"] = descrip
        if re.search(r",[A-Z0-9]+,", e):
            pno = re.search(r",[A-Z0-9]+,", e).group()
            all_dict[line]["Part"] = pno
        if re.search(r",[\d]{,3},", e):
            quaty = re.search(r",[\d]{,3},", e).group()
            all_dict[line]["Qty"] = quaty
        if re.search(r",[-\d]+\.\d+,", e):
            extend = re.search(r",[-\d]+\.\d+,", e).group()
            all_dict[line]["Extend"] = extend
        if re.search(r",[-\d]+\.\d+,", e):
            labor = re.search(r",[-\d]+\.\d+,", e).group()
            all_dict[line]["Labor"] = labor
        if re.search(r",[-\d]+\.\d+,", e):
            paint = re.search(r",[-\d]+\.\d+,", e).group()
            all_dict[line]["Paint"] = paint
    except:
        print("Fail")
    
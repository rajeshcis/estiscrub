#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 16:12:54 2020

@author: aman
"""

import tabula
import pandas as pd
import numpy as np
import re

# def build_df(tables):
#     j = ""
#     abd = tables[0]
#     for e in tables[1:]:
#         for g, f in e.iterrows():
#             import pdb; pdb.set_trace()
#             for i in f:
#                 if type(i) == str and "SUBTOTALS" in i:
                    
#                     j = i
#                 elif "SUBTOTALS" in j:
#                     return ab
#             con_ser = pd.DataFrame(f).transpose()
#             ab = pd.concat([abd, con_ser], ignore_index=True)
                
# ab = build_df(tables)

tables = tabula.read_pdf(
    "/media/aman/698F6E1531797531/Data/rajeshcis-estiscrub-7fa744c289f6/PDF_data/7759 westerlind original.pdf",
    multiple_tables=True, 
    pages='2', 
    output_format="dataframe")


# cols = []
df1 = tables[0]
# for e in df1:
#     cols.append(e)
#     for f in df1[e]:
#         if f == "*":
#             line_col = e
#         if f == "R&I" or f == "Rpr" or f == "Repl":
#             oper_col = e
#         if line_col != oper_col:
#             print("NO")
            

k = []
count = 0
lin = df1.iloc[0]
for e in lin:
    try:
        if np.isnan(e):
            p = count
    except:
        p = e
    k.append(p)
    count += 1
df1.columns = k

dic_count = {}
for e in df1:
    if type(e) == int:
        dic_count[e] = df1.loc[:, e]
        # import pdb; pdb.set_trace()
         # "*" in [f for f in df1.loc[:, e]]:
        df1 = df1.drop([e], axis=1)

dic_c = []
for e in dic_count.keys():
    by = [i for i in dic_count[e]]
    # import pdb; pdb.set_trace()
    if "*" in by:
        for f, g in zip(dic_count[e], df1.loc[:, "Oper"]):
            try:
                if np.isnan(f):
                    f = ""
            except:
                pass
            try:
                if np.isnan(g):
                    g = ""
            except:
                pass
            dic_c.append(f + g)
            

if len(dic_c) != 0:    
    dic_count["Oper"] = pd.Series(dic_c)
    
    # ln = pd.Series(dic_count["edited"])
    # df1 = df1.update(dic_count["Oper"])
    print("yes")
    df1.Oper= pd.Series(dic_c)
    df1
    
df =df1
df = df.drop([0, 1])

new_df = pd.DataFrame(columns=["Line"])
val = 0
qty = 0
amt = 0
amt_lis = {}
qty_lis = {}
rem = {}
arem = {}
qt_rem = {}
part_no = {}
amt_dic = {}

for e, f in zip(df.iloc[:, 2], df.iloc[:, 0]):
    try:
         if not np.isnan(e):
             pass
    except:   
        # import pdb; pdb.set_trace()
        if re.search(r"\s[A-Z0-9]{10,12}", e) or re.search(r"\s\d", e) or re.search(r"\d+\.\d+", e):
            rem[f] = e
            if re.search(r"\s[A-Z0-9]{10,12}", e):
                val = re.search(r"\s[A-Z0-9]{10,12}", e).group()
                part_no[f] = val
                rem[f] = re.sub(r"\s[A-Z0-9]{10,12}", "", rem[f])
                val = 0
            if re.search(r"\s\d", e):
                qty = re.search(r"\s\d", e).group()
                rem[f] = re.sub(r"\s\d", " ", rem[f])
                qty_lis[f] = qty
                qty = 0
            if re.search(r"\d+\.\d+", e):
                amt = re.search(r"\d+\.\d+", e).group()
                rem[f] = re.sub(r"\d+\.\d+", "", rem[f])
                amt_lis[f] = amt
                amt = 0
        else:
            arem[f] = e

try:
    for e, f in zip(df.loc[:, "Extended"], df.iloc[:, 0]):
        try:
            if not np.isnan(e):
                pass
        except:
                amt_lis[f] = e
except:
    pass

try:
    for e, f in zip(df.loc[:, "Part Number"], df.iloc[:, 0]):
        try:
            if not np.isnan(e):
                pass
        except:        
                part_no[f] = e
except:
    pass

try:
    for e, f in zip(df.loc[:, "Qty"], df.iloc[:, 0]):
        try:
            if not np.isnan(e):
                pass
        except:        
                qty_lis[f] = e
except:
    pass


head = {}
oper = {}
for e, f, gg, hh, ii in zip(df.iloc[:, 1], df.iloc[:, 0], df.iloc[:, 2], df.iloc[:, 3], df.iloc[:, 4]):
    # import pdb; pdb.set_trace()
    try:
        if np.isnan(gg) and np.isnan(hh) and np.isnan(ii):
            head[f] = e
    except:
        try:
            if np.isnan(e):
                pass
        except:
            if re.search(r"[\*\#]+", e):
                e = re.sub(r"[\*\#]+", "", e)
        oper[f] = e


labor_dic = {}

for e, f in zip(df.iloc[:, 3], df.iloc[:, 0]):
    # import pdb; pdb.set_trace()
    try:
        if np.isnan(e):
            pass
            
    except:
        labor_dic[f] = e
        
        
paint_dic = {}

for e, f in zip(df.iloc[:, 4], df.iloc[:, 0]):
    # import pdb; pdb.set_trace()
    try:
        if np.isnan(e):
            pass
    except:
        paint_dic[f] = e
        
new_df["Line"] = range(df.shape[0])

all_val = {}
for i in range(1, df.shape[0]+1):
    all_val[i] = {}
    
for i in range(1, df.shape[0]+1):
    if str(i) in head.keys():
        all_val[i]['head'] = head[str(i)] 
    if str(i) in oper.keys():
        all_val[i]['oper'] = oper[str(i)] 
    if str(i) in rem.keys():
        all_val[i]['discription'] = rem[str(i)]
    if str(i) in arem.keys():
        all_val[i]['discription'] = arem[str(i)]
    if str(i) in part_no.keys():
        all_val[i]['part number'] = part_no[str(i)]
    if str(i) in qty_lis.keys():
        all_val[i]['qty'] = qty_lis[str(i)]
    if str(i) in amt_lis.keys():
        all_val[i]['extended price'] = amt_lis[str(i)]
    if str(i) in labor_dic.keys():
        all_val[i]['labor'] = labor_dic[str(i)]
    if str(i) in paint_dic.keys():
        all_val[i]['paint'] = paint_dic[str(i)]
        

# for e, f in oper.items():
#     for x in new_df["Line"]:
#         # import pdb; pdb.set_trace()
#         new_df["Oper2"] = np.where(x == e, f, np.NaN)


# #table = tables[1]
# #table.to_csv("/home/cis/table_1.csv")

# #with open ("/home/cis/table_1.csv", "w") as f:
# #    for 
# #    f.write(table)
    
# table1 = tables[1]
# header0 = table1.iloc[0]
# table1.columns = header0
# table1 = table1.drop(0, axis=0)
# table1.to_csv("/home/cis/table_3.csv")
# #j = 0
# #for f in table1:
# #    f.to_csv(f"file1_{i}.csv", index=False)
# ##    j += 1
# #with open ("/home/cis/table_2.csv", "w") as g:
# #    g.write(table1)


# #for e in tables:
# #    print(e)
# #    
# #    for f in e:
# #        print(f)
# #

# table3 = tables[5]
# table3 = table3.drop(8, axis=1)
# #table3.to_csv("/home/cis/table_1.csv")
# #for e in table3[3]:
# #    print(e)
# #    if e[-1].isalnum() and len(e[-1]) == 10:
# #        print(e[-1])
# header = table3.iloc[0]
# table3.columns = header
# table3 = table3.drop(0, axis=0)
# table3.to_csv("/home/cis/table_1.csv")


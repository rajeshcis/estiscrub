"""Forms to signup and login."""
import re
from django import forms
from .models import CustomUser, Estimation, InsuranceCompany, InsuranceAppraisalCompany
from django.contrib.auth.forms import UserCreationForm

ESTIMATION_TYPE = (
    ('', 'Choose'),
    ('CCC', 'CCC'),
    ('Audatex', 'Audatex'),
    ('Mitchell', 'Mitchell'),
    ('Web Est', 'Web Est')
)

INSU_CMP = (
    ('', 'Choose'),
    ('Mapfre', 'Mapfre'),
    ('Safety', 'Safety'),
    ('Liberty Mutual', 'Liberty Mutual')
)

INSU_APP = (
    ('', 'Choose'),
    ('Advanced Appraisal', 'Advanced Appraisal')
)


class SignUpForm(UserCreationForm):
    """Sign up form."""

    email = forms.EmailField(required=True)
    agree_terms = forms.BooleanField(required=True)

    def __init__(self, *args, **kwargs):
        """Initialize the form."""
        super(SignUpForm, self).__init__(*args, **kwargs)

        for field in iter(self.fields):
            if field != 'agree_terms':
                self.fields[field].widget.attrs.update({
                    'class': 'form-control',
                    'placeholder': self.fields[field].label,
                })
        self.fields['email'].widget.attrs = {'placeholder': 'User Email', 'class': 'form-control'}

    class Meta:
        """Meta info."""

        model = CustomUser
        fields = ('email', 'password1', 'password2', 'first_name', 'last_name') + ('agree_terms',)

    def clean_email(self):
        """Override email validation method."""
        email = self.cleaned_data['email']
        if CustomUser.objects.filter(email=email, is_active=False).exists():
            user = CustomUser.objects.get(email=email, is_active=False)
            user.delete()
        return email

    def clean_password2(self):
        """Override email validation method."""
        password2 = self.cleaned_data.get("password2")

        if re.search('[A-Z]', password2) is None or len(set(re.findall(r'[~!@#$%^&\*()_+=-`]', password2))) == 0 or \
           len(set(re.findall(r'[0-9]', password2))) == 0:
            raise forms.ValidationError(
                "Password must contains minimum 8 characters with one special character, one capital letter\
                 and one numeric character")

        return password2


class LoginForm(forms.ModelForm):
    """Established Business for Sale form."""

    password = forms.CharField(max_length=32, widget=forms.PasswordInput)

    class Meta:
        """Definition for this class."""

        model = CustomUser
        fields = ['email', 'password']

    def __init__(self, *args, **kwargs):
        """Override fields objects."""
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['password'].required = True
        self.fields['email'].required = True

        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
                'placeholder': self.fields[field].label
            })


class UploadPdfForm(forms.ModelForm):
    """Established Business for Sale form."""

    estimate_type = forms.ChoiceField(choices=ESTIMATION_TYPE, required=False)
    # insurance_company = forms.ChoiceField(choices=INSU_CMP, required=False)
    # insurance_appraisal = forms.ChoiceField(choices=INSU_APP, required=False)
    insurance_company = forms.ModelChoiceField(queryset=InsuranceCompany.objects.all(), empty_label="Choose", required=False)
    insurance_appraisal = forms.ModelChoiceField(queryset=InsuranceAppraisalCompany.objects.all(), empty_label="Choose", required=False)

    def __init__(self, *args, **kwargs):
        """Override fields objects."""
        super(UploadPdfForm, self).__init__(*args, **kwargs)
        self.fields['upload_pdf'].required = True

        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control select_option_section',
            })

    class Meta:
        """Definition for this class."""

        model = Estimation
        fields = ['estimate_type', 'insurance_company', 'insurance_appraisal', 'upload_pdf']

import pandas as pd
import camelot
import csv
from django.conf import settings
import numpy as np
import os
import random
import pdftotext
import string
import re

# file = "/media/aman/698F6E1531797531/Data/rajeshcis-estiscrub-7fa744c289f6/PDF_data/8899 CAWLEY oe.pdf"
# file = "PDF_data/7759 westerlind original.pdf"
# file = "PDF_data/8559 COTE s1.pdf"
# file = "/media/aman/698F6E1531797531/Data/rajeshcis-estiscrub-7fa744c289f6/PDF_data/8190 ERMANI oe.pdf"
# file = "/media/aman/698F6E1531797531/Data/rajeshcis-estiscrub-7fa744c289f6/PDF_data/8328 MONTANEZ s1.pdf"

def tab1(tables):
    for e in tables:

        for f in e.df.iterrows():
            try:
                if "Line" in f[1].any() or "Line" in e.df.any() or "REAR BUMPER" in f[1].any():
                    return e
            except:
                pass
            try:
                if "Line" in e.df:
                    return e
            except:
                pass

def tab2(tables):
    for e in tables:
        for f in e.df.iterrows():
            try:
                if "ESTIMATE TOTALS" in f[1].any() or "ESTIMATETOTALS" in f[1].any() or "ESTIMATE TOTALS" in e.df:
                    return e
            except:
                pass
            try:
                if "ESTIMATE TOTALS" in e.df:
                    return e
            except:
                pass


def table_extract(file, count, uuid):
    drop_one = False
    drop_two = False
    try:
        page_list = [x for x in range(1, count)]
        pages = ','.join(str(e) for e in page_list)
        tables = camelot.read_pdf(file, flavor='stream', pages=pages, edge_tol=50)
        fil = file.split("/")[-1].split(".")[0]
        with open(file, "rb") as f:
            pdf = pdftotext.PDF(f)
        fullpdf = "".join(pdf[x] for x in range(len(pdf)))
        df = re.sub(r"^\s+", "", fullpdf, flags=re.MULTILINE)

        tables_values = {}

        if re.search(r"\n\s*Line\s", df):
            line = re.search(r"\n\s*Line\s", df)
            tables_values['line'] = [line.start(), line.group()]

        if re.search(r"\s*SUBTOTALS.*\d\n", df):
            subtotal = re.search(r"\s*SUBTOTALS.*\d\n", df)
            tables_values['subtotal'] = [subtotal.end(), subtotal.group()]

        df = df[tables_values['line'][0]:tables_values['subtotal'][0]]

        if re.search(r'\d{1,2}\/\d{2}\/\d{4}[0-9\w\s\d\*\:\\\/\#\-]*(AM|PM)\s*\d*\s*Page\s\d\n', df):
            df = re.sub(r'\d{1,2}\/\d{2}\/\d{4}[0-9\w\s\d\*\:\\\/\#\-]*(AM|PM)\s*\d*\s*Page\s\d\n',"", df)
        else:
            df = re.sub(r"\d.*Page\s\d\n", "", df)
        if re.search(r'PDF created with pdfFactory Pro trial version www.pdffactory.com\n', df):
            df = re.sub(r'PDF created with pdfFactory Pro trial version www.pdffactory.com\n',"", df)
        if re.search(r'Supplement[a-zA-Z0-9\s]+\n', df):
            df = re.sub(r'Supplement[a-zA-Z0-9\s]+\n',"", df)
        if re.search(r'Owner.*Job Number.*\n\w*.+\n', df):
            df = re.sub(r'Owner.*Job Number.*\n\w*.+\n',"", df)
        # else:
        #     df = re.sub(r"Claim.*\nWorkfile ID.*", "", df)
        if re.search(r'Claim.*\nWorkfile.*\n\w*.+\n', df):
            df = re.sub(r'Claim.*\nWorkfile.*\n\w*.+\n',"", df)
        if re.search("\d{1,2}\/\d{1,2}\/\d{4}\s\d{1,2}.*(AM|PM)", df):
            df = re.sub("\d{1,2}\/\d{1,2}\/\d{4}\s\d{1,2}.*(AM|PM)", "", df)

        df_list = df.split("\n")
        df_list.remove("")
        for idx, data in enumerate(df_list[-5:]):
            if "SUBTOTALS" in data:
                # df_list = df_list[:idx]
                break
        idx = len(df_list) - idx+1
        subtotals = df_list[idx]
        df_list = df_list[:idx]
        col_names = df_list[:2]
        df_list = df_list[2:]
        lines = []
        headings = []
        oper = []
        description = []
        quantity = []
        part_number = []
        extended_price = []
        labour_col = []
        paint_col = []

        for idx, line in enumerate(df_list):
            spec_1, spec_2 = "", ""
            if re.search(r"^([0-9]{1,3}|[A-Z]\s*[0-9]{1,3})", line):
                data = re.search(r"^([0-9]{1,3}|[A-Z]\s*[0-9]{1,3})", line).group()
                line = re.sub(r"^([0-9]{1,3}|[A-Z]\s*[0-9]{1,3})", "", line)
                df_list[idx] = line
                data = re.sub("\n", "", data)
            else:
                data = ""

            lines.append(data.strip())

            if re.search(r"(##|\*\*|#|\*)", line):
                spec_1 = re.search(r"(##|\*\*|#|\*)", line).group()
                line = re.sub(r"(##|\*\*|#|\*)", "", line)
            if re.search(r"(S01|S02|S03|S04|S05|S06|S07)", line):
                spec_2 = re.search(r"(S01|S02|S03|S04|S05|S06|S07)", line).group()
                line = re.sub(r"(S01|S02|S03|S04|S05|S06|S07)", "", line)
            # spec = re.search(r"(S01|S02|S03|S\d*)", line).group()
                df_list[idx] = line
            spec = spec_1 + " " + spec_2
            headings.append(spec.strip())

            if re.search(r"(R&I|Subl|Rpr|Repl|Algn|Blnd|Refn)", line):
                operation = re.search(r"(R&I|Subl|Rpr|Repl|Algn|Blnd|Refn)", line).group()
                line = re.sub(r"(R&I|Subl|Rpr|Repl|Algn|Blnd|Refn)", "", line)
                df_list[idx] = line
            else:
                operation = ""
            oper.append(operation.strip())

            if re.search(r"([A-Z]|[a-z]).+", line):
                data = re.search(r"([A-Z]|[a-z]).+", line).group()
                desc = re.split(r"\s\s\s", data)[0]
                data = data.replace(desc, "") #re.sub(desc, "", data)
                line = line.replace(desc, "desc ")
                df_list[idx] = line
            else:
                desc = ""

            description.append(desc.strip())

            if re.search(r"\s\d{1,2}\s\s", line):
                qty = re.search(r"\s\d{1,2}\s\s", line).group()
                line = line.replace(qty, "qty ")
                df_list[idx] = line
            else:
                qty = ""

            quantity.append(qty.strip())

            if re.search(r"desc\s*\w{4,15}\s", line):
                p_num = re.search(r"desc\s*\w{4,15}\s", line).group()
                p_num = p_num.replace("desc", "")
                line = line.replace(p_num, " p_num ")
                df_list[idx] = line
            else:
                p_num = ""

            part_number.append(p_num.strip())

            if re.search(r"(qty|desc)\s{2,25}\d{1,4}.\d{2}(\s{2}|\s\w)", line):
                data = re.search(r"(qty|desc)\s{2,25}\d{1,4}.\d{2}(\s{2}|\s\w)", line).group()
                extended = data.strip().replace("qty", "")
                extended = re.sub(r"(qty|desc)", "", data)
                line = line.replace(extended, " Ex ")
                df_list[idx] = line
            elif re.search(r"(qty|desc)\s{2,25}\d{1,4}.\d{2}", line):
                data = re.search(r"(qty|desc)\s{2,25}\d{1,4}.\d{2}", line).group()
                extended = re.sub(r"(qty|desc)", "", data)
                line = line.replace(extended, " Ex ")
                df_list[idx] = line
            elif re.search(r"(qty|desc)\s*[A-Za-z]\s", line):
                data = re.search(r"(qty|desc)\s*[A-Za-z]\s", line).group()
                extended = re.sub(r"(qty|desc)", "", data)
                line = line.replace(extended, " Ex ")
                df_list[idx] = line
            elif re.search(r"p_num\s*[A-Za-z]\s", line):
                data = re.search(r"p_num\s*[A-Za-z]\s", line).group()
                extended = re.sub(r"p_num", "", data)
                line = line.replace(extended, " Ex ")
                df_list[idx] = line
            else:
                extended = ""

            extended_price.append(extended.strip())
            # labour_col.append(labour)

            if re.search(r"(qty Ex|p_num)\s{2,32}(Incl.|\d{1,2}.\d{1,2})", line):
                data = re.search(r"(qty Ex|p_num)\s{2,32}(Incl.|\d{1,2}.\d{1,2})", line).group()
                labor_data = re.sub(r"(qty Ex|p_num)", "", data)
                line = line.replace(labor_data, " lab ")
                df_list[idx] = line
            elif re.search(r"desc\s{50,71}(Incl.|\d{1,2}.\d{1,2})", line):
                data = re.search(r"desc\s{50,71}(Incl.|\d{1,2}.\d{1,2})", line).group()
                labor_data = re.sub(r"desc", "", data)
                line = line.replace(labor_data, " lab ")
                df_list[idx] = line
            elif re.search(r"desc\s*(Incl.|\d{1,2}.\d{1,2}\s)", line):
                data = re.search(r"desc\s*(Incl.|\d{1,2}.\d{1,2}\s)", line).group()
                labor_data = re.sub(r"desc", "", data)
                line = line.replace(labor_data, " lab ")
                df_list[idx] = line
            elif re.search(r"qty\s{15,23}(Incl.|\d{1,2}.\d{1,2})", line):
                data = re.search(r"qty\s{15,23}(Incl.|\d{1,2}.\d{1,2})", line).group()
                labor_data = re.sub(r"qty", "", data)
                line = line.replace(labor_data, " lab ")
                df_list[idx] = line
            elif re.search(r"desc Ex\s{2,8}(\d{1,2}.\d{1,2}|Incl.)", line):
                data = re.search(r"desc Ex\s{2,8}(\d{1,2}.\d{1,2}|Incl.)", line).group()
                labor_data = re.sub(r"desc Ex", "", data)
                line = line.replace(labor_data, " lab ")
                df_list[idx] = line
            elif re.search(r"p_num Ex\s*(Incl.|\d{1,2}.\d{1,2})", line):
                data = re.search(r"p_num Ex\s*(Incl.|\d{1,2}.\d{1,2})", line).group()
                labor_data = re.sub(r"p_num Ex", "", data)
                line = line.replace(labor_data, " lab ")
                df_list[idx] = line
            else:
                labor_data = ""

            labour_col.append(labor_data.strip())

            if re.search(r"(lab|desc)\s*(\d{1,2}.\d{1,2}|-\d{1,2}.\d{1,2})", line):
                data = re.search(r"(lab|desc)\s*(\d{1,2}.\d{1,2}|-\d{1,2}.\d{1,2})", line).group()
                paint = re.sub(r"lab|desc", "", data)
                line = line.replace(paint, " paint")
                df_list[idx] = line
            elif re.search(r"p_num\s*(\d{1,2}.\d{1,2}|-\d{1,2}.\d{1,2})", line):
                data = re.search(r"p_num\s*(\d{1,2}.\d{1,2}|-\d{1,2}.\d{1,2})", line).group()
                paint = re.sub(r"p_num", "", data)
                line = line.replace(paint, " paint ")
                df_list[idx] = line
            else:
                paint = ""

            paint_col.append(paint.strip())

        desc, ext_price, lab, paint = re.split(r"\s*", subtotals)

        lines.append("")
        headings.append("")
        oper.append("")
        description.append(desc.strip())
        part_number.append("")
        quantity.append("")
        extended_price.append(ext_price)
        labour_col.append(lab)
        paint_col.append(paint)

        line_items_dict = {}

        line_items_dict["Line"] = lines
        line_items_dict["Heading"] = headings
        line_items_dict["Oper"] = oper
        line_items_dict["Description"] = description
        line_items_dict["Qty"] = quantity
        line_items_dict["Part Number"] = part_number
        line_items_dict["Qty"] = quantity
        line_items_dict["Extended Price $"] = extended_price
        line_items_dict["Labor"] = labour_col
        line_items_dict["Paint"] = paint_col

        line_items_df = pd.DataFrame.from_dict(line_items_dict, orient='index')

        line_items_df = line_items_df.transpose()

        cols = list(line_items_df.columns)

        line_items_df["UUID"] = uuid

        est_old_tab = tab2(tables)

        if not est_old_tab:
            tables = camelot.read_pdf(file, flavor='stream', pages=pages, edge_tol=200)
            est_old_tab = tab2(tables)

        if est_old_tab:
            est_old_tab = est_old_tab.df

        try:
            if not est_old_tab.empty:
                for e in est_old_tab.iterrows():
                    try:
                        if flag:
                            est = est.transpose()
                            est = pd.concat([est, e[1].transpose()], axis=1, ignore_index=True)
                            est = est.transpose()
                    except:
                        if "ESTIMATE TOTALS" in e[1].any() or "ESTIMATETOTALS" in e[1].any():
                            flag = True
                            est = e[1]

                flag = False
                if "ESTIMATE TOTALS" in est.iloc[0].any() or "ESTIMATETOTALS" in est.iloc[0].any():
                    colom = est.iloc[1]
                    est = est.drop([0,1])
                    est.columns = colom
                    est["UUID"] = uuid
                elif "ESTIMATE TOTALS" in est.iloc[1].any() or "ESTIMATETOTALS" in est.iloc[1].any():
                    colom = est.iloc[2]
                    est = est.drop([0,1,2])
                    est.columns = colom
                    est["UUID"] = uuid
                elif "ESTIMATE TOTALS" in est.iloc[2].any() or "ESTIMATETOTALS" in est.iloc[2].any():
                    colom = est.iloc[3]
                    est = est.drop([0,1, 2, 3])
                    est.columns = colom
                    est["UUID"] = uuid
                elif "ESTIMATE TOTALS" in est.iloc[3].any() or "ESTIMATETOTALS" in est.iloc[3].any():
                    colom = est.iloc[4]
                    est = est.drop([0 ,1, 2, 3, 4])
                    est.columns = colom
                    est["UUID"] = uuid
        except Exception as e:
            # print(e)
            pass

        if est_old_tab is None:
            import tabula
            table = tabula.read_pdf(file, pages = pages, multiple_tables = True)
            est = table[-1]
            est_old_tab = True
            est = est.replace(np.nan, '', regex=True).astype(str)
            if "ESTIMATE TOTALS" in est.iloc[0, 0]:
                est = est.drop([0])
            if "Category Basis" in est.iloc[0, 0]:
                est.columns = est.iloc[0, :]
                est = est.drop([1])
            if "Category Basis" in est.columns:
                category_df = pd.DataFrame(est["Category Basis"].str.split(r"(\s\d)", 1).tolist(), columns=["Category", "Basis", "Extra"])
                category_df = category_df.replace(np.nan, '', regex=True).astype(str)
                category_df["Basis"] = category_df["Basis"].replace(np.nan, '', regex=True).astype(str) + category_df["Extra"].replace(np.nan, '', regex=True).astype(str)
                del category_df["Extra"]
                category_df = category_df.replace(np.nan, '', regex=True).astype(str)
                category_df.index += est.index[0]
                category_idx = est.columns.get_loc("Category Basis")
                est = est.drop(["Category Basis"], axis=1)
                est.insert(loc=category_idx, value=category_df["Category"], column="Category")
                est.insert(loc=category_idx+1, value=category_df["Basis"], column="Basis")
                est["UUID"] = uuid
                del category_df

        diction = {}
        for e in cols:
            diction[e] = {}

        for e in cols:
            for i, f in enumerate(line_items_df[e]):
                if "Note:" in f and "Part Number" in e and len(f) > 15:
                    diction[e][i] = ""
                    # continue
                diction[e][i] = f

        est_dic = {}

        if not est.empty:
            if "Rate\nCost $" in est.columns:
                cols = list(est.columns)
                rate_idx = cols.index("Rate\nCost $")
                rate, cost = cols[rate_idx].split("\n")
                cols.remove("Rate\nCost $")
                cols.insert(rate_idx, rate)
                cols.insert(rate_idx+1, cost)
                rate_idx = est.columns.get_loc("Rate\nCost $")
                rate_df = pd.DataFrame(est["Rate\nCost $"].str.split("\n", 1).tolist(), columns=["Rate", "Cost"])
                rate_df = rate_df.replace(np.nan, "", regex=True)
                rate_df.index += est.index[0]
                est = est.drop(["Rate\nCost $"], axis=1)
                est.insert(loc=rate_idx, value=rate_df["Rate"], column="Rate")
                est.insert(loc=rate_idx+1, value=rate_df["Cost"], column="Cost $")

            if "Basis\nRate\nCost $" in est.columns:
                cols = list(est.columns)
                rate_idx = cols.index("Basis\nRate\nCost $")
                basis, rate, cost = cols[rate_idx].split("\n")
                cols.remove("Basis\nRate\nCost $")
                cols.insert(rate_idx, basis)
                cols.insert(rate_idx+1, rate)
                cols.insert(rate_idx+2, cost)
                rate_idx = est.columns.get_loc("Basis\nRate\nCost $")
                basis_df = pd.DataFrame(est["Basis\nRate\nCost $"].str.split(r"\n", 1).tolist(), columns=["Basis", "Rate\nCost $"])
                basis_df = basis_df.replace(np.nan, "", regex=True)
                if "@\n" in basis_df["Rate\nCost $"].any():
                    basis_df = basis_df.replace(r"@\n", "", regex=True)
                rate_df = pd.DataFrame(basis_df["Rate\nCost $"].str.split("\n", 1).tolist(), columns=["Rate", "Cost"])
                rate_df = rate_df.replace(np.nan, "", regex=True)
                basis_df.index += est.index[0]

                rate_df.index += est.index[0]
                est = est.drop(["Basis\nRate\nCost $"], axis=1)
                est.insert(loc=rate_idx, value=basis_df["Basis"], column="Basis")
                est.insert(loc=rate_idx+1, value=rate_df["Rate"], column="Rate")
                est.insert(loc=rate_idx+2, value=rate_df["Cost"], column="Cost $")

            if "Basis\nRate" in est.columns:
                cols = list(est.columns)
                rate_idx = cols.index("Basis\nRate")
                basis, rate = cols[rate_idx].split("\n")
                cols.remove("Basis\nRate")
                cols.insert(rate_idx, basis)
                cols.insert(rate_idx+1, rate)
                rate_idx = est.columns.get_loc("Basis\nRate")
                basis_df = pd.DataFrame(est["Basis\nRate"].str.split(r"\n", 1).tolist(), columns=["Basis", "Rate"])
                basis_df = basis_df.replace(np.nan, "", regex=True)
                basis_df.index += est.index[0]
                est = est.drop(["Basis\nRate"], axis=1)
                est.insert(loc=rate_idx, value=basis_df["Basis"], column="Basis")
                est.insert(loc=rate_idx+1, value=basis_df["Rate"], column="Rate")

            basic_rate_idx = est.columns.get_loc("Basis")
            if est.iloc[-1].str.contains("Page").any():
                est = est[:-1]
            if "@" in est['Basis'].any() and est.iloc[:, basic_rate_idx-1].str.contains(r"\d").any() \
                and est.columns.get_loc("Category") != basic_rate_idx-1:
                est["Basis"] = est['Basis'].astype(str) + est.iloc[:, basic_rate_idx-1].astype(str)
                est.drop(est.columns[[basic_rate_idx-1]], axis=1, inplace=True)

            est['Basis'].replace('@', '', regex=True, inplace=True)
            try:
                for i, e in enumerate(est.iterrows()):
                    est_dic[i] = {}

                if est.shape != (0, 0):
                    for i, e in enumerate(est.iterrows()):

                        for f in e[1:]:
                            for r, s in zip(est.columns, f):
                                if "\n" in r:
                                    r = r.split("\n")
                                    s = s.split("\n")
                                    for t in s:
                                        if "/hr" in t:

                                            est_dic[i][r[0]] = t
                                        else:
                                            est_dic[i][r[1]] = t
                                    # est_dic[i]["Basis"] = est_dic[i].pop("")
                                    if "Page " in s[0]:
                                        _ = est_dic.pop(i)
                                else:
                                    est_dic[i][r] = s
            except Exception as e:
                print(e)

        est.to_csv(f'{settings.MEDIA_ROOT}/build_csv/{fil}_table_2.csv')
        line_items_df.to_csv(f'{settings.MEDIA_ROOT}/build_csv/{fil}_table_1.csv')
        return diction, est_dic
    except Exception as e:
        print("Error Reading PDF: ", e)
        return None, None


if __name__ == '__main__':
    files = os.listdir('/home/cis/estiscrub/CCC_files')
    working_file = []
    error_file = []
    error_count = 0
    success_count = 0
    for file in files:
        with open('/home/cis/estiscrub/CCC_files/'+file, "rb") as f:
            pdf = pdftotext.PDF(f)
        count = len(pdf)
        uuid = x = ''.join(random.choice(string.ascii_uppercase + \
                                         string.ascii_lowercase + string.digits) for _ in range(32))
        try:
            line_items, estimate_lines = table_extract('/home/cis/estiscrub/CCC_files/'+file, count, uuid)
            success_count += 1
            print("Success!!!!!!!!")
            print(success_count)
            working_file.append(file)
        except Exception as e:
            print(file)
            error_count += 1
            print(error_count)
            error_file.append(file)
            print(e)

    import subprocess as sp
    for file in working_file[-100:]:
        sp.Popen(['cp', '/home/cis/estiscrub/CCC_files/{}'.format(file), '/home/cis/estiscrub/files_to_share_13_aug'])
"""Urls for Financial app."""
from .views import (EstiDetailsView, UploadPdfView, SettingView, UpdateEstimationLineView, 
                UpdateEstimationTotalsView, AudatexDetailsView, UpdateDamagesTableView, UpdateEstimateTableView,
                MitchelDetailsView, UpdateMitchelEstimationTableView, DownloadCsvView)
from django.urls import path


urlpatterns = [
    path('details/<int:pk>/', EstiDetailsView.as_view(), name='estimation-detail'),
    path('audatex_details/<int:pk>/', AudatexDetailsView.as_view(), name='audatex-detail'),
    path('mitchel_details/<int:pk>/', MitchelDetailsView.as_view(), name='mitchel-detail'),
    path('upload_pdf/', UploadPdfView.as_view(), name='upload-pdf'),
    path('settings/', SettingView.as_view(), name='settings'),
    path('update_estimate_line/', UpdateEstimationLineView.as_view(), name='update-estimate-line'),
    path('update_estimate_total/', UpdateEstimationTotalsView.as_view(), name='update-estimate-total'),
    path('update_damagestable_line/', UpdateDamagesTableView.as_view(), name='update-damagestable-line'),
    path('update_estimate_table/', UpdateEstimateTableView.as_view(), name='update-estimate-table'),
    path('update_mitchell_estimation_table/', UpdateMitchelEstimationTableView.as_view(), name='update-mitchell-estimate-table'),
    path('download-csv/', DownloadCsvView, name='download-csv'),
]

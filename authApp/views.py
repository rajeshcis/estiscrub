"""Main application controllers."""
import re
from django.http import response

from django.http.response import JsonResponse
import plac
import random
import spacy
import pdftotext
import json
import string
import csv
import pandas as pd
import camelot
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views import View
from django.contrib import messages
from django.contrib.auth.forms import SetPasswordForm
from django.views.generic.detail import DetailView
from spacy.matcher import Matcher
from .models import Estimation, EstimationTotals, EstimationLine, InsuranceCompany, \
                    InsuranceAppraisalCompany, Audatex_Owner, DamagesTable, EstimateTable, \
                    MitchelEstimation, MitchelEstimationTable
from .forms import UploadPdfForm
from authApp.newfile import table_extract
from authApp.checking_file import extracting, match_func, information, table_first
from django.conf import settings
from .audatext_extraction import audatex_preprocessing
from .extract_mitchell import extract_mitchel_data
import logging
from estiscrub.settings import BASE_DIR, MEDIA_ROOT
import os
from django.http import HttpResponse
root_logger= logging.getLogger()
root_logger.setLevel(logging.DEBUG) # or whatever
handler = logging.FileHandler('error.log', 'w', 'utf-8') # or whatever
handler.setFormatter(logging.Formatter('%(name)s %(message)s')) # or whatever
root_logger.addHandler(handler)
# logging.basicConfig(filename='error.log', encoding='utf-8', level=logging.DEBUG)

nlp = spacy.load("en_core_web_sm")

matcher = Matcher(nlp.vocab)
# from train_data_1 import LABEL, TRAIN_DATA
# r = re.compile(r"^\s+", re.MULTILINE)
LABEL = ["InsCo", "Estimate Type", "Workfile_id", "IndApprCo"]




def DownloadCsvView(request):
    if request.method == "POST":
        # import pdb;pdb.set_trace()
        try:
            est_obj = Estimation.objects.get(id=request.POST['estiId'])
        except Estimation.DoesNotExist:
            return JsonResponse({"sussess": True, "message": "Estimation not found..."})
        estimation_file_name = (est_obj.upload_pdf.name).split("/")[1]
        estimation_file_csv = estimation_file_name.split(".")[0]
        csv_fpath = 'media/build_csv/'+(estimation_file_csv)+".csv"
        csv_fpath1 = 'media/build_csv/'+(estimation_file_csv)+"_table_1.csv"
        csv_fpath2 = 'media/build_csv/'+(estimation_file_csv)+"_table_2.csv"
        is_file = os.path.exists(csv_fpath)
        if is_file:
            return JsonResponse({"sussess":True,"message":"Success", "csv1": csv_fpath,"csv2": csv_fpath1,"csv3": csv_fpath2})
        else:
            return JsonResponse({"sussess": True, "message": "CSV Not Found..."})
    return JsonResponse({"sussess": True, "message": "Invalid Request..."})


class EstiDetailsView(LoginRequiredMixin, DetailView):
    """Details for EstiDetailsView."""

    model = Estimation
    template_name = 'details.html'
    context_object_name = 'estimation'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['estimation'].is_new = False
        context['estimation'].save()
        context['form'] = UploadPdfForm()
        if self.object.vehicle_parts:
            context['vehicle_parts'] = json.loads(self.object.vehicle_parts)
        if self.object.insured_locations:
            context['insured_locations'] = json.loads(self.object.insured_locations)
        return context

class AudatexDetailsView(LoginRequiredMixin, DetailView):
    """Details for AudatexDetailsView."""

    model = Audatex_Owner
    template_name = 'audatex_details.html'
    context_object_name = 'audatex'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['audatex'].is_new = False
        context['audatex'].save()
        context['form'] = UploadPdfForm()
        return context

class MitchelDetailsView(LoginRequiredMixin, DetailView):
    """Details for MitchelDetailsView."""

    model = MitchelEstimation
    # import pdb; pdb.set_trace()
    template_name = 'mitchel_details.html'
    context_object_name = 'mitchell'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['mitchell'].is_new = False
        context['mitchell'].save()
        context['form'] = UploadPdfForm()
        return context

class UploadPdfView(LoginRequiredMixin, View):
    """Details for EstiDetailsView."""
#***************************************
    def matcher_data(self, p_file):
        pdf = p_file
        if re.search(r"Workfile ID:\s+.*", pdf[0]):
            df = re.sub(r"^\s+", "", pdf[0], flags=re.MULTILINE)
        elif re.search(r"Workfile ID:\s+.*", pdf[1]):
            df = re.sub(r"^\s+", "", pdf[1], flags=re.MULTILINE)
        else:
            df = re.sub(r"^\s+", "", pdf[2], flags=re.MULTILINE)
        df = re.sub("\s{2,}", "   ", df, flags=re.MULTILINE)
        df = df.replace('\n', '   ')
        return df

    def matching(self, file_upload):
        dfd = self.matcher_data(file_upload)
        matc = match_func(dfd)
        return matc

    def info(self, p_file):
        dic_info = {}
        pos_dict = information(p_file)
        # dic_info["owner"] = pos_dict[0]
        # dic_info["Inspection Location"] = pos_dict[1]
        # if "Appraiser Information:" in p_file[0]:
        #     dic_info["Appraiser Information"] = pos_dict[2]
        #     dic_info["Repair Facility"] = pos_dict[3]
        # else:
        #     dic_info["Repair Facility"] = pos_dict[2]
        return pos_dict

    def CCC_handling(self, pdf, est_type, request, estimation, uuid):
        count = len(pdf)
        cla=''
        if re.search(r"Workfile ID:\s+.*", pdf[0]):
            df = re.sub(r"^\s+", "", pdf[0], flags=re.MULTILINE)
        elif re.search(r".*Workfile ID+.*", pdf[0], re.DOTALL):
            df = re.sub(r"^\s+", "", pdf[0], flags=re.MULTILINE)
        elif re.search(r"Workfile ID:\s+.*", pdf[1]):
            df = re.sub(r"^\s+", "", pdf[1], flags=re.MULTILINE)
        else:
            df = re.sub(r"^\s+", "", pdf[2], flags=re.MULTILINE)
        df = re.sub("\s{2,}", " ", df, flags=re.MULTILINE)
        df = df.replace('\n', ' ')
        if re.search(r"\bWorkfile ID: \w{8}\b", df):
            wfid = re.search(r"\bWorkfile ID: \w{8}\b", df).span()
            wfid_t = df[wfid[0]:wfid[1]]
            df = re.sub(r"\bWorkfile ID: \w{8}\b", "", df)
            if re.search(r"\bClaim #: [-\w]+\b ", df[:wfid[0]]):
                cla = re.search(r"\bClaim #: [-\w]+\b ", df[:wfid[0]]).group()
                df = re.sub(r"\b Claim #: [-\w]+\b", "", df)
        if re.search(r"\bFederal ID: [-\w]+\b ", df[:275]):
            df = re.sub(r" Federal ID: [-\w]+", "", df)
        if re.search(r"\bLicense Number: [-\w]+\b ", df[:275]):
            df = re.sub(r" License Number: \w\w\s\d\d\d\d", "", df)
        df = wfid_t + " " + df

        self.CCC_extract(df, pdf, cla, count, est_type, request, estimation, uuid)
        # messages.error(request, 'Estimation Uploaded!')
        return

    def storing_audatex_data(self, header, owner, control_information, inspection, repair, veh_detail, veh_table, damagesdata, estimate_table):
        try:
            audatex_obj = Audatex_Owner.objects.create(
                **header, **owner, **control_information, **inspection, **repair, **veh_detail, **veh_table)
        except Exception as e:
            root_logger.error('{}'.format(e))
            print(e)

        for e in estimate_table:
            EstimateTable.objects.create(**e, audatex_owner=audatex_obj)

        for e in damagesdata:
            DamagesTable.objects.create(**e, audatex_owner=audatex_obj)

    def storing_mitchel_data(self, header, corporate_data, claim_control_data, vehicle_info, line_items):
        try:
            mitchel_obj = MitchelEstimation.objects.create(
                **header, **corporate_data, **claim_control_data, **vehicle_info)
        except Exception as e:
            root_logger.error('{}'.format(e))
            print(e)

        for e in line_items:
            MitchelEstimationTable.objects.create(**e, mitchel_owner=mitchel_obj)

        # for e in damagesdata:
        #     DamagesTable.objects.create(**e, audatex_owner=audatex_obj)

    def handle_uploaded_file(self, p_file, request, estimation, uuid):
        df = ""
        cla = ""
        est_type = estimation.estimate_type
        wfid_t = ""
        pdf = pdftotext.PDF(p_file)
        for page in pdf:
            if "CCC ONE Estimating - A product of CCC Information Services Inc." in page:
                est_type = 'CCC'
                try:
                    self.CCC_handling(pdf, est_type, request, estimation, uuid)
                    messages.success(request, 'Pdf successfully uploaded')
                    break
                except  Exception as e:
                    root_logger.error('{}'.format(e))
                    messages.error(request, 'Unable to upload {} Estimation'.format(est_type))
                    return redirect('home')
            elif "Audatex North America, Inc." in page:
                est_type = "Audatex"
                try:
                    header, owner, control_information, inspection, repair, veh_detail, veh_table, damagesdata, estimate_table = audatex_preprocessing(pdf)
                    self.storing_audatex_data(header, owner, control_information, inspection, repair, veh_detail, veh_table, damagesdata, estimate_table)
                    messages.success(request, 'Pdf successfully uploaded')
                    break
                except  Exception as e:
                    root_logger.error('{}'.format(e))
                    messages.error(request, 'Unable to upload {} Estimation'.format(est_type))
                    return redirect('home')
            elif "Mitchell Data Version" in page or "Mitchell International" in page:
                est_type = "Mitchell"
                try:
                    estimation.save()
                    header, corporate_data, claim_control_data, vehicle_info, line_items = extract_mitchel_data(pdf, estimation.upload_pdf.path)
                    self.storing_mitchel_data(header, corporate_data, claim_control_data, vehicle_info, line_items)
                    messages.success(request, 'Pdf successfully uploaded')
                    break
                except Exception as e:
                    root_logger.error('{}'.format(e))
                    messages.error(request, 'Unable to upload {} Estimation'.format(est_type))
                    # root_logger.error('{}'.format(e))
                    return redirect('home')

    def extract_data(self, data, cla, model="final_model", new_model_name="animal", output_dir="final_model", n_iter=1000):
        LABEL = [ "Workfile_id", "Estimate Type", "InsCo", "IndApprCo"]
        aa, bb, cc, dd = 0, 0, 0, 0
        ext = {"InsCo" : 0, "Estimate Type" : 0, "Workfile_id" : 0, "IndApprCo" : 0}
        random.seed(0)
        if model is not None:
            nlp = spacy.load(model)
            print("Loaded model '%s'" % model)
        else:
            nlp = spacy.blank("en")
            print("Created blank 'en' model")

        if "ner" not in nlp.pipe_names:
            ner = nlp.create_pipe("ner")
            nlp.add_pipe(ner)
        else:
            ner = nlp.get_pipe("ner")

        for label in LABEL:
            ner.add_label(label)

        ner.add_label("POBOX")
        if model is None:
            optimizer = nlp.begin_training()
        else:
            optimizer = nlp.resume_training()

        move_names = list(ner.move_names)
        pipe_exceptions = ["ner", "trf_wordpiecer", "trf_tok2vec"]
        other_pipes = [pipe for pipe in nlp.pipe_names if pipe not in pipe_exceptions]

        ext = extracting(data, cla)
        test_text = data
        return ext

    def build_csv(self, ext_data, lis, info_dic, table, file_name):
        fil = file_name.split("/")[-1].split(".")[0]
        with open(f"{settings.MEDIA_ROOT}/build_csv/{fil}.csv", 'w', newline="") as csv_file:
            writer = csv.writer(csv_file)
            for key, value in ext_data.items():
                writer.writerow([key, value])
            for key, value in lis.items():
                if ": " in value:
                    frnt, bck = value.split(": ")
                    writer.writerow([frnt, bck])
                else:
                    writer.writerow([value])
            for key, value in info_dic.items():
                if ": " in value:
                    frnt, bck = value.split(": ")
                    writer.writerow([frnt, bck])
                else:
                    writer.writerow([value])
            for key, value in table.items():
                if ": " in value:
                    frnt, bck = value.split(": ")
                    writer.writerow([frnt, bck])
                else:
                    writer.writerow([value])

    def CCC_extract(self,data, pdf, cla, count, est_type, request, estimation, uuid):
        ext_data = self.extract_data(data, cla)
        print(ext_data)

        # if request.POST.get('estimate_type'):
        #     if est_type != request.POST.get('estimate_type'):
        #         messages.error(request, 'Please select correct Estimation type or correct estimation file which belongs to the estimation type.')
        #         return redirect('home')
        #     else:
        #         estimation.estimate_type = request.POST.get('estimate_type')

        # if request.POST.get('insurance_company') or request.POST.get('company'):
        #     com = ''
        #     if request.POST.get('insurance_company'):
        #         com = InsuranceCompany.objects.get(pk=request.POST.get('insurance_company')).name
        #     else:
        #         com = insurancecompany.name

        #     if com and com not in ext_data["InsCo"]:
        #         messages.error(request, 'requested insurance company not in the uploded PDF')
        #         return redirect('home')

        # if request.POST.get('insurance_appraisal') or request.POST.get('appraisal'):
        #     Acom = ''
        #     if request.POST.get('insurance_appraisal'):
        #         Acom = InsuranceAppraisalCompany.objects.get(pk=request.POST.get('insurance_appraisal')).name
        #     else:
        #         Acom = insurance_appraisal_company.name

        #     if Acom and Acom not in ext_data["IndApprCo"]:
        #         messages.error(request, 'requested insurance Appraisal company not in the uploded PDF')
        #         return redirect('home')

        lis = self.matching(pdf)
        table = table_first(pdf)
        info_dic = self.info(pdf)

        if request.POST.get('company'):
            if not request.POST.get('insurance_company') and request.POST.get('company'):
                estimation.insurance_company = request.POST.get('company')
        else:
            if not request.POST.get('insurance_company') and ext_data["InsCo"]:
                estimation.insurance_company = ext_data["InsCo"]
        if request.POST.get('appraisal'):
            if not request.POST.get('insurance_appraisal') and request.POST.get('appraisal'):
                estimation.insurance_appraisal = request.POST.get('appraisal')
        else:
            if not request.POST.get('insurance_appraisal') and ext_data["IndApprCo"]:
                estimation.insurance_appraisal = ext_data["IndApprCo"]

        if not request.POST.get('workfile_id') and ext_data["Workfile_id"] != "":
            estimation.workfile_id = ext_data["Workfile_id"]
            # estimation.workfile_id = ext_data["Workfile_id"][13:]
        print(lis)
        print(table)
        print(info_dic)

        estimation.independent_appraisal_company = ext_data["IndApprCo"]
        estimation.jobnumber = lis.get('Job Number')
        estimation.writtenby = lis.get('Written By')
        estimation.license_number = lis.get('License Number')
        estimation.adjuster = lis.get('Adjuster')
        estimation.typeofloss = lis.get('Type Of Loss')
        estimation.dateofloss = lis.get('Date Of Loss')
        estimation.dayofrepair = lis.get('Days To Repair')
        estimation.pointofimpact = lis.get('Point of Impact')
        estimation.vin = lis.get('VIN')
        estimation.state = lis.get('State')
        estimation.odometer = lis.get('Odometer')
        estimation.license = lis.get('License')
        estimation.production_date = lis.get('Production')
        estimation.vehicle_detail = lis.get('vehicle_detail')
        estimation.insured = lis.get('Insured')
        estimation.deductible = lis.get('Deductible')
        estimation.claim = lis.get('Claim')
        estimation.policy = lis.get('Policy')
        estimation.condition = lis.get('Condition')
        estimation.exteriorcolor = lis.get('Exterior Color')
        estimation.interiorcolor = lis.get('Interior Color')
        estimation.mileagein = lis.get('Mileage_In')
        estimation.mileageout = lis.get('Mileage_Out')
        estimation.owner = lis.get('Owner')
        estimation.vehicle_parts = json.dumps(table)
        estimation.insured_locations = json.dumps(info_dic)
        estimation.save()
        table1, table2 = table_extract(estimation.upload_pdf.path, count, uuid)
        print(table1, table2)
        if table2:
            self.save_estimation_totals(estimation, table2)
        self.save_estimation_line(estimation, table1)
        try:
            self.build_csv(ext_data, lis, info_dic, table, estimation.upload_pdf.path)
        except Exception as e:
            print(e)
        messages.success(request, 'Pdf successfully uploaded')

    def post(self, request, *args, **kwargs):
        """To get details."""
        lis = {"jobnumber:": "",
               "writtenBy:": "",
               "adjuster:": "",
               "insured:": "",
               "claim:": "",
               "pointofimpact:": "",
               "license:": "",
               "state:": "",
               "vin:": "",
               "odometer:": "",
               "policy:": "",
               "exteriorColor:": "",
               "owner:": ""}
        form = UploadPdfForm(request.POST, request.FILES)
        print(request.FILES)
        if form.is_valid():
            print(request.POST)
            if request.POST.get('company'):
                insurancecompany, created = InsuranceCompany.objects.get_or_create(name=request.POST.get('company'))

            if request.POST.get('appraisal'):
                insurance_appraisal_company, created = InsuranceAppraisalCompany.objects.get_or_create(name=request.POST.get('appraisal'))

            estimation = form.save(commit=False)
            uuid = x = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(32))
            estimation.user = request.user

            if request.POST.get('estimate_type'):
                # if est_type != request.POST.get('estimate_type'):
                #     messages.error(request, 'Please select correct Estimation type or correct estimation file which belongs to the estimation type.')
                #     return redirect('home')
                # else:
                estimation.estimate_type = request.POST.get('estimate_type')
            else:
                messages.error(request, 'You must select the estimation type')
                return redirect('home')

            self.handle_uploaded_file(request.FILES["upload_pdf"], request, estimation, uuid)
        else:
            print(form.errors)
            messages.error(request, 'Form got some error Please re-try to upload')
        return redirect('home')


    def save_estimation_totals(self, instance, data):
        if not data:
            return

        for key, value in data.items():
            newdict = {}
            newdict['estimation'] = instance
            newdict['category'] = value['Category']
            newdict['basis'] = value['Basis']
            newdict['rate'] = value['Rate']
            newdict['cost'] = value['Cost $']
            newdict['uuid'] = value['UUID']
            EstimationTotals.objects.create(**newdict)

    def save_estimation_line(self, instance, data):
        try:
            if data.get('Line'):
                line_len = len(data.get('Line').keys())
                for key, value in data['Line'].items():
                    newdict = {}
                    newdict['estimation'] = instance
                    newdict['line'] = value
                    newdict['oper'] = data['Heading'][key]
                    newdict['repl'] = data['Oper'][key]
                    newdict['description'] = data['Description'][key]
                    newdict['qty'] = data['Qty'][key]
                    if data.get('Extended'):
                        newdict['extanded_price'] = data['Extended'][key]
                    elif data.get('Extended Price $'):
                        newdict['extanded_price'] = data['Extended Price $'][key]
                    newdict['labor'] = data['Labor'][key]
                    if data.get('Paint'):
                        newdict['paint'] = data['Paint'][key]
                    else:
                        newdict['paint'] = ''
                    # newdict['uuid'] = value['UUID']
                    if data.get('Part Number'):
                        newdict['part_number'] = data['Part Number'][key]
                    else:
                        newdict['part_number'] = ''

                    # if ""
                    try:
                        if not value and key + 1 == line_len and "SUBTOTALS" not in data['Part Number'][key]:
                            newdict['description'] = 'SUBTOTALS'
                    except Exception as e:
                        print(e)
                        if "SUBTOTALS" not in data["Qty"][key]:
                            newdict['description'] = 'SUBTOTALS'
                        pass
                    EstimationLine.objects.create(**newdict)
                    print(newdict)
        except Exception as err:
            if hasattr(err, 'message'):
                print(err.message)
            else:
                print(err)




class SettingView(LoginRequiredMixin, View):
    """Details for EstiDetailsView."""

    def get(self, request, *args, **kwargs):
        """To get details."""
        return render(request, 'settings.html', {"form": SetPasswordForm(user=request.user)})

    def post(self, request, *args, **kwargs):
        """To get details."""
        form = SetPasswordForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Password is sucessfully changed.")
            return redirect('details:settings')
        else:
            messages.error(request, "Password change got some error.")
            return render(request, 'settings.html', {"form": form})

class UpdateEstimationLineView(LoginRequiredMixin, View):
    """Update Estimate View."""

    def post(self, request, *args, **kwargs):
        """To update details."""
        try:
            estimate_line = EstimationLine.objects.get(id=request.POST.get("estimate_id"))
            estimate_line.line = request.POST.get("line")
            estimate_line.oper = request.POST.get("oper")
            estimate_line.description = request.POST.get("description")
            estimate_line.part_number = request.POST.get("part_number")
            estimate_line.qty = request.POST.get("qty")
            estimate_line.extanded_price = request.POST.get("extanded_price")
            estimate_line.labor = request.POST.get("labor")
            estimate_line.paint = request.POST.get("paint")
            estimate_line.save()
            messages.success(request, 'Estimate line updated successfully')
        except:
            messages.error(request, 'Form got some error Please re-try to update')
        return redirect('/esti/details/{}/#estimate/'.format(estimate_line.estimation.id))

class UpdateEstimationTotalsView(LoginRequiredMixin, View):
    """Update Estimate totals View."""

    def post(self, request, *args, **kwargs):
        """To update details."""
        try:
            estimate_total = EstimationTotals.objects.get(id=request.POST.get("estimate_total_id"))
            estimate_total.category = request.POST.get("category")
            estimate_total.basis = request.POST.get("basis")
            estimate_total.rate = request.POST.get("rate")
            estimate_total.cost = request.POST.get("cost")
            estimate_total.save()
            messages.success(request, 'Estimate total updated successfully')
        except:
            messages.error(request, 'Form got some error Please re-try to update')
        return redirect('/esti/details/{}/#totals/'.format(estimate_total.estimation.id))


class UpdateDamagesTableView(LoginRequiredMixin, View):
    """Update Damages Table View."""

    def post(self, request, *args, **kwargs):
        """To update details."""
        try:
            damagestable_line = DamagesTable.objects.get(id=request.POST.get("damagestable_line_id"))
            damagestable_line.line = request.POST.get("line")
            damagestable_line.op = request.POST.get("op")
            damagestable_line.guide = request.POST.get("guide")
            damagestable_line.mc = request.POST.get("mc")
            damagestable_line.description = request.POST.get("description")
            damagestable_line.mfr_partno = request.POST.get("mfr_partno")
            damagestable_line.adj = request.POST.get("adj")
            damagestable_line.b = request.POST.get("b")
            damagestable_line.hours = request.POST.get("hours")
            damagestable_line.r = request.POST.get("r")
            damagestable_line.save()
            messages.success(request, 'Damages table line updated successfully')
        except:
            messages.error(request, 'Form got some error Please re-try to update')
        return redirect('/esti/audatex_details/{}/#damages/'.format(damagestable_line.audatex_owner.id))

class UpdateEstimateTableView(LoginRequiredMixin, View):
    """Update Estimate table View."""

    def post(self, request, *args, **kwargs):
        """To update details."""
        try:
            estimate_table = EstimateTable.objects.get(id=request.POST.get("estimate_table_id"))
            estimate_table.parts = request.POST.get("parts")
            estimate_table.rate_hour = request.POST.get("rate_hour")
            estimate_table.amount = request.POST.get("amount")
            estimate_table.save()
            messages.success(request, 'Estimate Table updated successfully')
        except:
            messages.error(request, 'Form got some error Please re-try to update')
        return redirect('/esti/audatex_details/{}/#estimate/'.format(estimate_table.audatex_owner.id))

class UpdateMitchelEstimationTableView(LoginRequiredMixin, View):
    """Update Estimate View."""

    def post(self, request, *args, **kwargs):
        """To update details."""
        try:
            mitchell_estimate_line = MitchelEstimationTable.objects.get(id=request.POST.get("estimate_line_id"))
            mitchell_estimate_line.line = request.POST.get("line")
            mitchell_estimate_line.entry_number = request.POST.get("entry_number")
            mitchell_estimate_line.labor_type = request.POST.get("labor_type")
            mitchell_estimate_line.operation = request.POST.get("operation")
            mitchell_estimate_line.description = request.POST.get("description")
            mitchell_estimate_line.part_number = request.POST.get("part_number")
            mitchell_estimate_line.amount = request.POST.get("amount")
            mitchell_estimate_line.labor_units = request.POST.get("labor_units")
            mitchell_estimate_line.save()
            messages.success(request, 'Estimate line updated successfully')
        except:
            messages.error(request, 'Form got some error Please re-try to update')
        return redirect('/esti/mitchel_details/{}/#estimateLine/'.format(mitchell_estimate_line.mitchel_owner.id))

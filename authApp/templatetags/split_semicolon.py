from django import template

register = template.Library()

@register.inclusion_tag('insured_locations_tag.html')
def insured_locations_tag(var):
    var_list = var.split(':')
    var_first = var_list[0]
    var_second = var_list[1]
    return {'var_first': var_first, 'var_second': var_second}

#!/u     sr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 11:25:30 2020

@author: cis
"""


import pdftotext
import re
import tabula
import numpy as np

r = re.compile(r"^\s+", re.MULTILINE)
# filename = "/home/cis/estiscrub/Mitchel_files/8286 PHAN oe.pdf"
# with open(filename, "rb") as file:
#     pdf = pdftotext.PDF(file)
#     pass

def extract_header(df, header):
    ################# HEADER DATA #################
    estimation_date = re.search(r"\bDate:\s+.*", df).group()
    header["estimate_date"] = estimation_date
    
    estimate_id = re.search(r"\bEstimate ID:\s+.*", df).group()
    header["estimate_id"] = estimate_id

    estimation_type = re.search(r"Mitchell Data.*", df).group()
    header["mitchel_type"] = "Mitchel"
    
    estimate_version = re.search(r"\bEstimate Version:\s+.*", df).group()
    header["estimate_version"] = estimate_version
    
    if re.search(r"\bCommitted+.*", df):
        committed = re.search(r"\bCommitted+.*", df).group()
        header["committed"] = committed
    else:
        header["committed"] = ""
    
    if re.search(r"\bSupplement+.*", df):
        supplement = re.search(r"\bSupplement+.*", df).group()
        header["supplement"] = supplement
    else:
        header["supplement"] = ""
    
    if re.search(r"\bProfile ID+.*", df):
        profile_id = re.search(r"\bProfile ID+.*", df).group()
        header["profile_id"] = profile_id
    else:
        header["profile_id"] = ""
    
    if re.search(r"\bQuote ID+.*", df):
        quote_id = re.search(r"\bQuote ID+.*", df).group()
        header["quote_id"] = quote_id
    else:
        header["quote_id"] = ""
    return header

def extract_corporate_details(df, corporate_data):
    ################# CORPORATE DATA ################# 
    if re.search(r"\*|\w([^\*]*\s\w*)(.*?)Damage", df):
        insco = re.search(r"\*|\w([^\*]*\s\w*)(.*?)Damage", df, re.DOTALL).group()
        if 'Profile ID' in insco:
            junk = re.search(r"Date:+.*Profile ID:\s*(\w*-\w*| \w*)", insco, re.DOTALL).group()
            insco = insco.replace(junk, '')
        else:
            insco = insco.replace('*', '')
            ignore_data = re.search(r"^\s.*", insco).group()
            insco = insco.replace(ignore_data, '')
            
        corporate_data['insco_info'] = insco[:-7]
    else:
        corporate_data['insco_info'] = ""
    
    if re.search(r"Damage Assessed By:(.*?)Condition", df, re.DOTALL):
        data = re.search(r"Damage Assessed By:(.*?)Condition", df, re.DOTALL).group()
        
        if re.search(r"Damage Assessed By:\s*.*(Appraised|Claim)", data):
            appraiser = re.search(r"Damage Assessed By:\s*.*(Appraised|Claim)", data).group()
        else:
            appraiser = re.search(r"Damage Assessed By:\s*\w*(\s\w*|\s*)", data).group()
        
        if "Appraised" in appraiser:
            corporate_data['appraiser_info'] = appraiser[:-12].strip()
        else:
            corporate_data['appraiser_info'] = appraiser
    else:
        corporate_data['appraiser_info'] = ""
        
    if re.search(r"(Appraised|Claim Rep|File).*(Supplemented|Classification)", df, re.DOTALL):
        data = re.search(r"(Appraised|Claim Rep|File).*(Supplemented|Classification)", df, re.DOTALL).group()
        corporate_data["adjuster_info"] = data[:-14]
    else:
        corporate_data["adjuster_info"] = ""
        
    if re.search(r"(Appraised|Claim Rep|File).*Condition", df, re.DOTALL):
        appraiser_info = re.search(r"(Appraised|Claim Rep|File).*Condition", df, re.DOTALL).group()
        appraiser_info = appraiser_info.replace(corporate_data["adjuster_info"], "")
        if "Classification" in appraiser_info:
            appraiser_info = re.sub(r"Classi.*\n", "", appraiser_info)
        appraiser_info = appraiser_info.replace("Condition", "")
        corporate_data["appraiser_address"] = appraiser_info
    elif re.search(r"Classification.*Condition", df, re.DOTALL):
        appraiser_info = re.search(r"Classification.*Condition", df, re.DOTALL).group()
        appraiser_info = re.sub(r"Classi.*\n", "", appraiser_info)
        appraiser_info = appraiser_info.replace("Condition", "")
        corporate_data["appraiser_address"] = appraiser_info
    else:
        corporate_data["appraiser_address"] = ""
    
    return corporate_data
 
def extract_claim_control(df, claim_control_data):
    if re.search(r"\bCondition Code:\s*\w*", df):
        condition = re.search(r"\bCondition Code:\s*\w*", df).group()
        claim_control_data["condition"] = condition
    else:
        claim_control_data["condition"] = ""
    
    if re.search(r"\bType of Loss:\s*\w*", df):
        loss_type = re.search(r"\bType of Loss:\s*\w*", df).group()
        claim_control_data["type_of_loss"] = loss_type
    else:
        claim_control_data["type_of_loss"] = ""
        
    if re.search(r"\b(Date of Loss:\s*\w*/\w*/\w*|\bDate of Loss.*)", df):
        loss_date = re.search(r"\b(Date of Loss:\s*\w*/\w*/\w*|\bDate of Loss.*)", df).group()
        claim_control_data["date_of_loss"] = loss_date
    else:
        claim_control_data["date_of_loss"] = ""
        
    if re.search(r"\bDate Assign:\s*\w*", df):
        assign_date = re.search(r"\bDate Assign:\s*\w*/\w*/\w*", df).group()
        claim_control_data["date_assign"] = assign_date
    else:
        claim_control_data["date_assign"] = ""
    
    if re.search(r"\bContact Date:\s*\w*", df):
        contact_date = re.search(r"\bContact Date:\s*\w*/(\w*|\s\w*)/(\w*|\s\w*)", df).group()
        claim_control_data["contact_date"] = contact_date
    else:
        claim_control_data["contact_date"] = ""
    
    if re.search(r"\bDeductible:\s*\w*", df):
        deductible = re.search(r"\bDeductible:\s*\w*(,|.)\w*", df).group()
        claim_control_data["deductible"] = deductible
    else:
        claim_control_data["deductible"] = ""

    if re.search(r"\bPolicy No:\s*\w*", df):
        policy_number = re.search(r"\bPolicy No:\s*\w*", df).group()
        claim_control_data["policy_no"] = policy_number
    else:
        claim_control_data["policy_no"] = ""
    
    if re.search(r"\bClaim Number:\s*.*", df):
        claim_number = re.search(r"\bClaim Number+.*", df).group()
        claim_control_data["claim_number"] = claim_number
    else:
        claim_control_data["claim_number"] = ""
        
    if re.search(r"\bInsured:\s*.*", df):
        insured = re.search(r"\bInsured:\s*.*", df).group()
        claim_control_data["insured"] = insured
    else:
        claim_control_data["insured"] = ""
                                      
    if re.search(r"\bAddress:\s*.*", df):
        address = re.search(r"\bAddress:\s*.*", df).group()
        claim_control_data["address"] = address
    else:
        claim_control_data["address"] = ""
    
    if re.search(r"\bTelephone:\s*.*", df):
        telephone = re.search(r"\bTelephone:\s*.*", df).group()
        claim_control_data["telephone"] = telephone
    else:
        claim_control_data["telephone"] = ""
    
    if re.search(r"\bOwner:\s*.*", df):
        owner = re.search(r"\bOwner:\s*.*", df).group()
        claim_control_data["owner"] = owner
    else:
        claim_control_data["owner"] = ""
        
def extract_vehicle_info(df, vehicle_info):
    if re.search(r"\bDescription:\s*.*", df):
        description = re.search(r"\bDescription:\s*.*", df).group()
        vehicle_info["description"] = description
    else:
        vehicle_info["description"] = ""
        
    if re.search(r"\bBody Style:\s*.*", df):
        body_style = re.search(r"Body Style:\s[-,():/\w\s]+Drive", df).group()
        vehicle_info["body_style"] = body_style[:-6].strip()
    else:
        vehicle_info["body_style"] = ""

    if re.search(r"\bDrive Train:\s*.*", df):
        drive_train = re.search(r"\bDrive Train:\s*.*", df).group()   
        vehicle_info['drive_train'] = drive_train
    else:
        vehicle_info['drive_train'] = ""
    
    if re.search(r"\bVIN:\s*.*", df):
        vin = re.search(r"\bVIN:\s*[-,/:;\w]*", df).group()
        vehicle_info['vin'] = vin
    else:
        vehicle_info['vin'] = ""
        
    if re.search(r"\bMileage:\s+\d+(?:,\d+)?", df):
        mileage = re.search(r"\bMileage:\s+\d+(?:,\d+)?", df).group()
        vehicle_info['mileage'] = mileage
    else:
        vehicle_info['mileage'] = ""
    
    if re.search(r"\bOEM/ALT:\s*\w*", df):
        oem_alt = re.search(r"\bOEM/ALT:\s*\w*", df).group()
        vehicle_info['oem_alt'] = oem_alt
    else:
        vehicle_info['oem_alt'] = ""
    
    if re.search(r"\bParts Profile:\s+((?:\w+-)+\w+)", df):
        parts_profile = re.search(r"\bParts Profile:\s+((?:\w+-)+\w+)", df).group()
        vehicle_info['parts_profile'] = parts_profile
    else:
        vehicle_info['parts_profile'] = ""
        
    if re.search(r"\bColor:\s*\w*", df):
        color = re.search(r"\bColor:\s*\w*", df).group()
        vehicle_info['color'] = color
    else:
        vehicle_info['color'] = ""
        
    if re.search(r"\bLicense:\s*\w*\s*\w*", df):
        license = re.search(r"\bLicense:\s*\w*\s*\w*", df).group()
        vehicle_info['license'] = license
    else:
        vehicle_info['license'] = ""
    
    if re.search(r"Options:\s*.*Line\s*Entry", df, re.DOTALL):
        options = re.search(r"Options:\s.*Line\s*Entry", df, re.DOTALL).group()
        vehicle_info['options'] = options[:-15]
    else:
        vehicle_info['options'] = ""
    
def extract_line_items(pdf_file):
    tables = tabula.read_pdf(pdf_file, pages="all", multiple_tables=True)
    line_items = tables[0].replace(np.nan, "", regex=True)
    if "Item" in line_items.iloc[0][0] or "Number" in line_items.iloc[0][1] \
        or "Description" in line_items.iloc[0][4]:
        line_items.columns = line_items.columns.str.replace(' ', '')
        column_name = line_items.iloc[0]
        line_items = line_items.drop(0)
        column_name = line_items.columns + " " + column_name
        column_name = column_name.replace("Unnamed:\d", "  ", regex=True)
        if len(column_name) >= 9 and "  " in column_name.iloc[7]:
            column_name.iloc[7] = "Dollar Amount Code"
        line_items.columns = column_name
        if column_name.str.contains("Dollar Amount Code").any():
            del line_items["Dollar Amount Code"]
        if "  " in line_items.columns[-1]:
            line_items = line_items.drop(line_items.columns[[-1]], axis=1)
        column_name = ["line", "entry_number", "labor_type", "operation", \
                        "description", "part_number", "amount", "labor_units"]
        line_items.columns = column_name
        line_items = line_items.to_dict('records')
        
        
    return line_items

def extract_mitchel_data(pdf, filename):
    # print(pdf[0])
    header = {}
    corporate_data = {}
    claim_control_data = {}
    vehicle_info = {}
    fullpdf = "".join(pdf[x] for x in range(len(pdf)))
    extract_header(fullpdf, header)
    df = re.sub(r"^\s+", "", fullpdf, flags=re.MULTILINE)
    extract_corporate_details(df, corporate_data)
    extract_claim_control(df, claim_control_data)
    extract_vehicle_info(df, vehicle_info)
    print(header)
    print(corporate_data)
    print(claim_control_data)
    print(vehicle_info)
    line_items = extract_line_items(filename)
    print(line_items)
    return header, corporate_data, claim_control_data, vehicle_info, line_items
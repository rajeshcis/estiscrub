import pdftotext
import re
import os


def extract_header(df, header):
    try:
        head = re.search(r"[A-Z]+[a-zA-Z0-9\s\n\-\,\(\)\.\:\;]*\d\d\d\-\d\d\d\d", df)
        header["head"] = head.group().strip()

        typee = re.search(r"\*{3}\s*[\w\d\s]*\s*\*{3}", df)
        header["audatex_type"] = typee.group().strip()

        datetime = re.search(r"\d{2}\/\d{2}\/\d{4}\s*\d{2}\:\d{2}\s*(AM|PM|am|pm)", df)
        header["datetime"] = datetime.group().strip()

    except Exception:
        print("Unable to fetch Header Data.")


def extract_ownerdetails(df, owner, own, c_i):
    if re.search(r"\n\s*Owner\s*\n", df):
        try:
            if re.search(r"Owner:\s*[\w\s\d\(\)\-]*(\s{2}|\n|\\)", df[own:c_i]):
                owner_name = re.search(r"Owner:\s*[\w\s\d\(\)\-]*(\s{2}|\n)", df[own:c_i])
                owner["owner_name"] = owner_name.group().strip()

            if re.search(r"Address:\s*[\w\s\d\-\@\_]*(\s{2}|\n)", df[own:c_i]):
                owner_address = re.search(r"Address:\s*[\w\s\d\-\@\_]*(\s{2}|\n)", df[own:c_i])
                owner["owner_address"] = owner_address.group().strip()

            if re.search(r"Cell:\s*[\w\s\d\(\)\-]*(\s{2}|\n)", df[own:c_i]):
                owner_cell = re.search(r"Cell:\s*[\w\s\d\(\)\-]*(\s{2}|\n)", df[own:c_i])
                owner["owner_cell"] = owner_cell.group().strip()

            if re.search(r"City State Zip: [\w\-\,\-\d\s]*\s{2}", df[own:c_i]):
                owner_citystatezip = re.search(r"City State Zip: [\w\-\,\-\d\s]*\s{2}", df[own:c_i])
                owner["owner_citystatezip"] = owner_citystatezip.group().strip()

            if re.search(r"Home\/Evening\s*[\w\s\d]*(\s{2}|\n)", df[own:c_i]):
                owner_home = re.search(r"Home\/Evening\s*[\w\s\d]*(\s{2}|\n)", df[own:c_i])
                owner["owner_home"] = owner_home.group().strip()

            if re.search(r"FAX:\s*[\w\s\d\(\)\-]*(\s{2}|\n)", df[own:c_i]):
                owner_fax = re.search(r"FAX:\s*[\w\s\d\(\)\-]*(\s{2}|\n)", df[own:c_i])
                owner["owner_fax"] = owner_fax.group().strip()

            if re.search(r"Email:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[own:c_i]):
                owner_email = re.search(r"Email:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[own:c_i])
                owner["owner_email"] = owner_email.group().strip()

        except Exception:
            print("Unable to fetch Owner Data.")


def extract_controlinformation(df, control_information, c_i, isp):
    if re.search(r"\n\s*Control Information\s*\n", df):
        try:
            if re.search(r"Claim # :\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp]):
                claim_number = re.search(r"Claim # :\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp])
                control_information["claim_number"] = claim_number.group().strip()

            if re.search(r"Insured Policy # :\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp]):
                insured_policy = re.search(r"Insured Policy # :\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp])
                control_information["insured_policy"] = insured_policy.group().strip()

            if re.search(r"Loss Date\/Time:\s*\d{2}\/\d{2}\/\d{4}\s*\d{2}\:\d{2}\s*(AM|PM|am|pm)", df[c_i:isp],):
                loss_date_time = re.search(
                    r"Loss Date\/Time:\s*\d{2}\/\d{2}\/\d{4}\s*\d{2}\:\d{2}\s*(AM|PM|am|pm)", df[c_i:isp],
                )
                control_information["loss_date_time"] = loss_date_time.group().strip()

            if re.search(r"Loss Type:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp]):
                loss_type = re.search(r"Loss Type:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp])
                control_information["loss_type"] = loss_type.group().strip()

            if re.search(r"Deductible:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp]):
                deductible = re.search(r"Deductible:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp])
                control_information["deductible"] = deductible.group().strip()

            if re.search(r"Ins. Company:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp]):
                claim = re.search(r"Ins. Company:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp])
                control_information["claim"] = claim.group().strip()

            if re.search(r"Address: \s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp]):
                address1 = re.search(r"Address: \s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp])
                control_information["address1"] = address1.group().strip()

            if re.search(r"City State Zip: [\w\-\,\-\d\s]*\s{2}", df[c_i:isp]):
                ci_citystatezip = re.search(r"City State Zip: [\w\-\,\-\d\s]*\s{2}", df[c_i:isp])
                control_information["ci_citystatezip"] = ci_citystatezip.group().strip()

            if re.search(r"Agent:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp]):
                agent = re.search(r"Agent:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp])
                control_information["agent"] = agent.group().strip()

            try:
                if re.search(r"Address:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[address1.end() : isp],):
                    address2 = re.search(r"Address:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[address1.end() : isp],)
                    control_information["address2"] = address2.group().strip()
            except:
                pass

            if re.search(r"Work/Day:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp]):
                work_day1 = re.search(r"Work/Day:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp])
                control_information["work_day1"] = work_day1.group().strip()

            if re.search(r"FAX:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp]):
                fax = re.search(r"FAX:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp])
                control_information["fax"] = fax.group().strip()

            if re.search(r"Email:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp]):
                email1 = re.search(r"Email:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp])
                control_information["email1"] = email1.group().strip()

            if re.search(r"Insured:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp]):
                insured = re.search(r"Insured:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp])
                control_information["insured"] = insured.group().strip()

            try:
                if re.search(r"Address:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[address2.end() : isp],):
                    address3 = re.search(r"Address:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[address2.end() : isp],)
                    control_information["address3"] = address3.group().strip()
            except:
                pass

            if re.search(r"Home/Day:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp]):
                home_day = re.search(r"Home/Day:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp])
                control_information["home_day"] = home_day.group().strip()

            if re.search(r"Cell:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp]):
                cell = re.search(r"Cell:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp])
                owner["cell"] = cell.group().strip()

            if re.search(r"Claim Rep:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp]):
                claim_replacement = re.search(r"Claim Rep:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[c_i:isp])
                control_information["claim_replacement"] = claim_replacement.group().strip()

            try:
                if re.search(r"Address:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[address3.end() : isp],):
                    address4 = re.search(r"Address:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[address3.end() : isp],)
                    control_information["address4"] = address4.group().strip()
            except:
                pass

            try:
                if re.search(r"Work/Day:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[work_day1.end() : isp],):
                    work_day2 = re.search(r"Work/Day:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[work_day1.end() : isp],)
                    control_information["work_day2"] = work_day2.group().strip()
            except:
                pass

            try:
                if re.search(r"Email:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[email1.end() : isp]):
                    email2 = re.search(r"Email:\s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[email1.end() : isp],)
                    control_information["email2"] = email2.group().strip()
            except:
                pass

            if re.search(r"Loss Payee:\s*[\w\s\d\(\)\-\@\.\_]*(\s{0}|\s{2}|\n)", df[c_i:isp]):
                loss_payee = re.search(r"Loss Payee:\s*[\w\s\d\(\)\-\@\.\_]*(\s{0}|\s{2}|\n)", df[c_i:isp])
                control_information["loss_payee"] = loss_payee.group().strip()

        except Exception:
            print("Unable to fetch Control Information Data.")


def extract_inspectionsdetails(df, inspection, isp, rep):
    if re.search(r"\n\s*Inspection\s*\n", df):
        try:
            if re.search(r"Inspection Date:\s*\d{2}\/\d{2}\/\d{4}\s*\d{2}\:\d{2}\s*(AM|PM|am|pm)", df[isp:rep]):
                inspection_date = re.search(
                    r"Inspection Date:\s*\d{2}\/\d{2}\/\d{4}\s*\d{2}\:\d{2}\s*(AM|PM|am|pm)", df[isp:rep]
                )
                inspection["inspection_date"] = inspection_date.group().strip()

            if re.search(r"Inspection Type:\s+.*", df[isp:rep]):
                inspection_type = re.search(r"Inspection Type:\s+.*", df[isp:rep])
                inspection["inspection_type"] = inspection_type.group().strip()
            if re.search(r"Inspection Location:\s*[\w\s]*\s{2}", df[isp:rep]):
                inspection_location = re.search(r"Inspection Location:\s*[\w\s]*\s{2}", df[isp:rep])
                inspection["inspection_location"] = inspection_location.group().strip()

            if re.search(r"Contact:\s+.*", df[isp:rep]):
                contact = re.search(r"Contact:\s+.*", df[isp:rep])
                inspection["contact"] = contact.group().strip()

            if re.search(r"City State Zip: [\w\-\,\-\d\s]*\s{2}", df[isp:rep]):
                citystatezip = re.search(r"City State Zip: [\w\-\,\-\d\s]*\s{2}", df[isp:rep])
                inspection["citystatezip"] = citystatezip.group().strip()

            if re.search(r"Primary Impact:\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep]):
                primary_impact = re.search(r"Primary Impact:\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep])
                inspection["primary_impact"] = primary_impact.group().strip()

            if re.search(r"Secondary Impact:\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep]):
                seconday_impact = re.search(r"Secondary Impact:\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep])
                inspection["seconday_impact"] = seconday_impact.group().strip()

            if re.search(r"Driveable:\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep]):
                driveable = re.search(r"Driveable:\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep])
                inspection["driveable"] = driveable.group().strip()

            if re.search(r"Rental Assisted:\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep]):
                rental_assisted = re.search(r"Rental Assisted:\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep])
                inspection["rental_assisted"] = rental_assisted.group().strip()

            if re.search(r"Assigned Date\/Time:\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep]):
                assigned_datetime = re.search(r"Assigned Date\/Time:\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep])
                inspection["assigned_datetime"] = assigned_datetime.group().strip()

            if re.search(r"Received Date\/Time:\s*\d{2}\/\d{2}\/\d{4}\s*\d{2}\:\d{2}\s*(AM|PM|am|pm)", df[isp:rep],):
                received_date = re.search(
                    r"Received Date\/Time:\s*\d{2}\/\d{2}\/\d{4}\s*\d{2}\:\d{2}\s*(AM|PM|am|pm)", df[isp:rep],
                )
                inspection["received_date"] = received_date.group().strip()

            if re.search(r"First Contact Date\/Time:\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep]):
                first_contact_datetime = re.search(r"First Contact Date\/Time:\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep])
                inspection["first_contact_datetime"] = first_contact_datetime.group().strip()

            if re.search(
                r"Appointment Date\/Time:\s*\d{2}\/\d{2}\/\d{4}\s*\d{2}\:\d{2}\s*(AM|PM|am|pm)", df[isp:rep],
            ):
                appointment_datetime = re.search(
                    r"Appointment Date\/Time:\s*\d{2}\/\d{2}\/\d{4}\s*\d{2}\:\d{2}\s*(AM|PM|am|pm)", df[isp:rep],
                )
                inspection["appointment_datetime"] = appointment_datetime.group().strip()

            if re.search(r"Appraiser Name:\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep]):
                appraiser_name = re.search(r"Appraiser Name:\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep])
                inspection["appraiser_name"] = appraiser_name.group().strip()

            if re.search(r"Appraiser License # :\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep]):
                appraiser_license = re.search(r"Appraiser License # :\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep])
                inspection["appraiser_license"] = appraiser_license.group().strip()

            if re.search(r"Address:\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep]):
                address = re.search(r"Address:\s*[\w\s\d]*(\s{2}|\n)", df[isp:rep])
                inspection["isp_address"] = address.group().strip()

            if re.search(r"Work/Day:\s+.*", df[isp:rep]):
                work_day = re.search(r"Work/Day:\s+.*", df[isp:rep])
                inspection["work_day"] = work_day.group().strip()

            try:
                if re.search(r"City State Zip:\s*[\w\-\,\-\d\s]*\s{2}", df[citystatezip.end() : rep],):
                    citystatezip2 = re.search(r"City State Zip:\s*[\w\-\,\-\d\s]*\s{2}", df[citystatezip.end() : rep],)
                    inspection["citystatezip2"] = citystatezip2.group().strip()
            except:
                pass

            if re.search(r"FAX:+.*", df[isp:rep]):
                fax = re.search(r"FAX:+.*", df[isp:rep])
                inspection["fax1"] = fax.group().strip()

            if re.search(r"Email:+.*", df[isp:rep]):
                email = re.search(r"Email:+.*", df[isp:rep])
                inspection["email"] = email.group().strip()
        except Exception:
            print("Unable to fetch Inspection Data.")


def extract_repairdetails(df, repair, rep, rem):
    if re.search(r"\n\s*Repairer\s*\n", df):
        try:
            if re.search(r"Repairer:\s*[\w\s\d]*(\s{2}|\n)", df[rep:rem]):
                repairer = re.search(r"Repairer:\s*[\w\s\d]*(\s{2}|\n)", df[rep:rem])
                repair["repairer"] = repairer.group().strip()

            if re.search(r"Contact:\s*[\w\s\d\-]*(\s{2}|\n)", df[rep:rem]):
                contact = re.search(r"Contact:\s*[\w\s\d]*(\s{2}|\n)", df[rep:rem])
                repair["contact1"] = contact.group().strip()

            if re.search(r"Address: \s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[rep:rem]):
                address1 = re.search(r"Address: \s*[\w\s\d\(\)\-\@\.\_]*(\s{2}|\n)", df[rep:rem])
                repair["rep_address1"] = address1.group().strip()

            if re.search(r"City State Zip:\s*[\w\s\d\,]*(\s{2}|\n)", df[rep:rem]):
                citystatezip = re.search(r"City State Zip:\s*[\w\s\d\,]*(\s{2}|\n)", df[rep:rem])
                repair["rep_citystatezip"] = citystatezip.group().strip()

            if re.search(r"FAX:\s*[\w\s\d\,\(\)\-]*(\s{2}|\n)", df[rep:rem]):
                fax = re.search(r"FAX:\s*[\w\s\d\,\(\)\-]*(\s{2}|\n)", df[rep:rem])
                repair["fax2"] = fax.group().strip()

            if re.search(r"Target Complete Date/Time:\s*\d{2}\/\d{2}\/\d{4}\s*\d{2}\:\d{2}\s*(AM|PM|am|pm)", df[rep:rem]):
                taget_complete_datetime = re.search(r"Target Complete Date/Time:\s*\d{2}\/\d{2}\/\d{4}\s*\d{2}\:\d{2}\s*(AM|PM|am|pm)", df[rep:rem])
                repair["taget_complete_datetime"] = taget_complete_datetime.group().strip()

            if re.search(r"Days To Repair:+.*", df[rep:rem]):
                days_to_repair = re.search(r"Days To Repair:+.*", df[rep:rem])
                repair["days_to_repair"] = days_to_repair.group().strip()
        except Exception:
            print("Unable to fetch Repairer Data.")


def vehicle_preprocess(df):
    if re.search(r"Vehicle[a-zA-Z0-9\s\-\.\_\,\(\)\/\n\&\%\$\*\#\:]+AudaVIN options", df):
        veh = re.search(r"Vehicle[a-zA-Z0-9\s\-\.\_\,\(\)\/\n\&\%\$\*\#\:]+AudaVIN options", df).group()
    elif re.search(r"Vehicle[a-zA-Z0-9\s\-\.\_\,\(\)\/\n\&\%\$\*\#\:]+Damages", df):
        veh = re.search(r"Vehicle[a-zA-Z0-9\s\-\.\_\,\(\)\/\n\&\%\$\*\#\:]+Damages", df).group()
    try:
        veh_data = veh.strip()
        return veh_data
    except:
        return False

def extract_vehicledetails(veh_data_1):
    matches = {
        "Lic.Plate:": None,
        "Lic State:": None,
        "Lic Expire:": None,
        "VIN:": None,
        "Prod Date:": None,
        "Mileage:": None,
        "Veh Insp#:": None,
        "Mileage Type:": None,
        "Condition:": None,
        "Ext. Color:": None,
        "Code:": None,
        "Int. Color:": None,
        "Ext. Refinish:": None,
        "Int. Refinish:": None,
        "Ext. Paint Code:": None,
        "Int. Trim Code:": None,
    }
    not_found = []
    lis_dict = {'tab1': [], 'tab2': [], 'tab3': []}

    for e in matches.keys():
        if re.search(f"{e}\s*[a-zA-Z0-9\s\-\.\_\,\/]+\n", veh_data_1):
            value = re.search(f"{e}\s*[a-zA-Z0-9\s\-\.\_\,\/]+\n", veh_data_1).group()[:-1]
            matches[e] = value
        else:
            not_found.append(e)
    for f in not_found:
        del matches[f]

    if re.search(r"[a-zA-Z0-9\s\.\-\_\n]+Lic.Plate", veh_data_1):
        vehicle_detail = re.search(r"[a-zA-Z0-9\s\.\-\_\n]+Lic.Plate", veh_data_1).group()[:-9]
        matches["Vehicle"] = vehicle_detail

    if "Lic.Plate:" in matches.keys():
        matches['license_plate'] = matches.pop("Lic.Plate:")
    if "Lic State:" in matches.keys():
        matches['license_state'] = matches.pop("Lic State:")
    if "Lic Expire:" in matches.keys():
        matches['license_expire'] = matches.pop("Lic Expire:")
    if "VIN:" in matches.keys():
        matches['vin'] = matches.pop("VIN:")
    if "Prod Date:" in matches.keys():
        matches['prod_date'] = matches.pop("Prod Date:")
    if "Mileage:" in matches.keys():
        matches['mileage'] = matches.pop("Mileage:")
    if "Veh Insp#:" in matches.keys():
        matches['veh_insp'] = matches.pop("Veh Insp#:")
    if "Mileage Type:" in matches.keys():
        matches['mileage_type'] = matches.pop("Mileage Type:")
    if "Condition:" in matches.keys():
        matches['condition'] = matches.pop("Condition:")
    if "Ext. Color:" in matches.keys():
        matches['ext_color'] = matches.pop("Ext. Color:")
    if "Code:" in matches.keys():
        matches['code'] = matches.pop("Code:")
    if "Int. Color:" in matches.keys():
        matches['int_color'] = matches.pop("Int. Color:")
    if "Ext. Refinish:" in matches.keys():
        matches['ext_refinish'] = matches.pop("Ext. Refinish:")
    if "Int. Refinish:" in matches.keys():
        matches['int_refinish'] = matches.pop("Int. Refinish:")
    if "Ext. Paint Code:" in matches.keys():
        matches['ext_paintcode'] = matches.pop("Ext. Paint Code:")
    if "Int. Trim Code:" in matches.keys():
        matches['int_trimcode'] = matches.pop("Int. Trim Code:")
    if "Vehicle" in matches.keys():
        matches['vehicle'] = matches.pop("Vehicle")
    
    if re.search(r"Options[a-zA-Z0-9\s\-\.\_\,\(\)\/\n\&\%\$\*\\\:\#\+\?\[\]\^\{\}\|\']+AudaVIN options", veh_data_1):
        value = re.search(r"Options[a-zA-Z0-9\s\-\.\_\,\(\)\/\n\&\%\$\*\\\:\#\+\?\[\]\^\{\}\|\']+AudaVIN options", veh_data_1).group()
        value_li = value.split("\n")

    elif re.search(r"Options[a-zA-Z0-9\s\-\.\_\,\(\)\/\n\&\%\$\*\\\:\#\+\?\[\]\^\{\}\|\']+Damages", veh_data_1):
        value = re.search(r"Options[a-zA-Z0-9\s\-\.\_\,\(\)\/\n\&\%\$\*\\\:\#\+\?\[\]\^\{\}\|\']+Damages", veh_data_1).group()
        value_li = value.split("\n")

    for i in range(1, len(value_li) - 1):
        if i % 3 == 1:
            lis_dict['tab1'].append(value_li[i])
        elif i % 3 == 2:
            lis_dict['tab2'].append(value_li[i])
        else:
            lis_dict['tab3'].append(value_li[i])

    return matches, lis_dict


def extract_damagetable(df):
    tables_values = {}
    data_extr = []
    try:
        df = re.sub(r'\d{2}\/\d{2}\/\d{4}[\w\s\d\*\:\\\/\#\-]*(AM|PM)\n',"", df)

        if re.search(r"\n\s*Damages\s*\n", df):
            damages = re.search(r"\n\s*Damages\s*\n", df)
            tables_values['damages'] = [damages.end(), damages.group()]
        
        if re.search(r"\s*Items\s*", df):
            items = re.search(r"\s*Items\s*", df)
            tables_values['items'] = [items.end(), items.group()]
        
        if re.search(r"\n\s*Unibody\/Frame\s*\n", df):
            unibody_frame = re.search(r"\n\s*Unibody\/Frame\s*\n", df)
            tables_values['unibody_frame'] = [unibody_frame.end(), unibody_frame.group()]
        
        if re.search(r"\n\s*Stripes\s*And\s*Mouldings\s*\n", df):
            stripes_mouldings = re.search(r"\n\s*Stripes\s*And\s*Mouldings\s*\n", df)
            tables_values['stripes_mouldings'] = [stripes_mouldings.end(), stripes_mouldings.group()]
        
        if re.search(r"\n\s*Front\s*Bumper\s*\n", df):
            frontbumper = re.search(r"\n\s*Front\s*Bumper\s*\n", df)
            tables_values['frontbumper'] = [frontbumper.end(), frontbumper.group()]

        if re.search(r"\n\s*Front\s*End\s*Panel\s*And\s*Lamps\s*\n", df):
            front_end_panel_lamps = re.search(r"\n\s*Front\s*End\s*Panel\s*And\s*Lamps\s*\n", df)
            tables_values['front_end_panel_lamps'] = [front_end_panel_lamps.end(), front_end_panel_lamps.group()]
        
        if re.search(r"\n\s*Radiator\s*Support\s*\n", df):
            radiator_support = re.search(r"\n\s*Radiator\s*Support\s*\n", df)
            tables_values['radiator_support'] = [radiator_support.end(), radiator_support.group()]
        
        if re.search(r"\n\s*Cooling\s*And\s*Air\s*Conditioning\s*\n", df):
            cooling = re.search(r"\n\s*Cooling\s*And\s*Air\s*Conditioning\s*\n", df)
            tables_values['cooling'] = [cooling.end(), cooling.group()]
        
        if re.search(r"\n\s*Front\s*Body\s*And\s*Windshield\s*\n", df):
            front_body_windshield = re.search(r"\n\s*Front\s*Body\s*And\s*Windshield\s*\n", df)
            tables_values['front_body_windshield'] = [front_body_windshield.end(), front_body_windshield.group()]
        
        if re.search(r"\n\s*Front\s*Body\s*Interior\s*Sheetmetal\s*\n", df):
            interior = re.search(r"\n\s*Front\s*Body\s*Interior\s*Sheetmetal\s*\n", df)
            tables_values['interior'] = [interior.end(), interior.group()]
        
        if re.search(r"\n\s*Engine\s*Mounts\s*\n", df):
            engine_mounts = re.search(r"\n\s*Engine\s*Mounts\s*\n", df)
            tables_values['engine_mounts'] = [engine_mounts.end(), engine_mounts.group()]
        
        if re.search(r"\n\s*Engine\s*And\s*Components\s*\n", df):
            engine_components = re.search(r"\n\s*Engine\s*And\s*Components\s*\n", df)
            tables_values['engine_components'] = [engine_components.end(), engine_components.group()]
        
        if re.search(r"\n\s*Wheels\s*\n", df):
            wheels = re.search(r"\n\s*Wheels\s*\n", df)
            tables_values['wheels'] = [wheels.end(), wheels.group()]
        
        if re.search(r"\n\s*Roof\s*\n", df):
            roof = re.search(r"\n\s*Roof\s*\n", df)
            tables_values['roof'] = [roof.end(), roof.group()]
        
        if re.search(r"\n\s*Center\s*Console\s*And\s*Seat\s*Tracks\s*\n", df):
            center_console_seat_tracks = re.search(r"\n\s*Center\s*Console\s*And\s*Seat\s*Tracks\s*\n", df)
            tables_values['center_console_seat_tracks'] = [center_console_seat_tracks.end(), center_console_seat_tracks.group()]
        
        if re.search(r"\n\s*Fuel Tank & Inr Bed Parts\s*\n", df):
            fueltank = re.search(r"\n\s*Fuel Tank & Inr Bed Parts\s*\n", df)
            tables_values['fueltank'] = [fueltank.end(), fueltank.group()]
        
        if re.search(r"\n\s*Cab And Components\s*\n", df):
            cab_components = re.search(r"\n\s*Cab And Components\s*\n", df)
            tables_values['cab_components'] = [cab_components.end(), cab_components.group()]
        
        if re.search(r"\n\s*Front Doors\s*\n", df):
            front_doorts = re.search(r"\n\s*Front Doors\s*\n", df)
            tables_values['front_doorts'] = [front_doorts.end(), front_doorts.group()]
        
        if re.search(r"\n\s*Rear Doors\s*\n", df):
            rear_doors = re.search(r"\n\s*Rear Doors\s*\n", df)
            tables_values['rear_doors'] = [rear_doors.end(), rear_doors.group()]
        
        if re.search(r"\n\s*Quarter And Rocker Panel\s*\n", df):
            quatar_rocker_panels = re.search(r"\n\s*Quarter And Rocker Panel\s*\n", df)
            tables_values['quatar_rocker_panels'] = [quatar_rocker_panels.end(), quatar_rocker_panels.group()]
        
        if re.search(r"\n\s*Inner Quarter & Panels\s*\n", df):
            inner_quatar_panels = re.search(r"\n\s*Inner Quarter & Panels\s*\n", df)
            tables_values['inner_quatar_panels'] = [inner_quatar_panels.end(), inner_quatar_panels.group()]
        
        if re.search(r"\n\s*Deck Lid And Back Glass\s*\n", df):
            decklid_backglass = re.search(r"\n\s*Deck Lid And Back Glass\s*\n", df)
            tables_values['decklid_backglass'] = [decklid_backglass.end(), decklid_backglass.group()]
        
        if re.search(r"\n\s*Tailgate\s*\n", df):
            tailgate = re.search(r"\n\s*Tailgate\s*\n", df)
            tables_values['tailgate'] = [tailgate.end(), tailgate.group()]
        
        if re.search(r"\n\s*Rear Bumper\s*\n", df):
            rear_bumper = re.search(r"\n\s*Rear Bumper\s*\n", df)
            tables_values['rear_bumper'] = [rear_bumper.end(), rear_bumper.group()]
        
        if re.search(r"\n\s*Rear Body, Lamps And Floor Pan\s*\n", df):
            rearbody_lamps_floorpan = re.search(r"\n\s*Rear Body, Lamps And Floor Pan\s*\n", df)
            tables_values['rearbody_lamps_floorpan'] = [rearbody_lamps_floorpan.end(), rearbody_lamps_floorpan.group()]
        
        if re.search(r"\n\s*Section Replacement & Refinish\s*\n", df):
            selection_replacement_refinish = re.search(r"\n\s*Section Replacement & Refinish\s*\n", df)
            tables_values['selection_replacement_refinish'] = [selection_replacement_refinish.end(), selection_replacement_refinish.group()]

        if re.search(r"\n\s*Manual Entries\s*\n", df):
            manual_entries = re.search(r"\n\s*Manual Entries\s*\n", df)
            tables_values['manual_entries'] = [manual_entries.end(), manual_entries.group()]
    except:
        pass
    
    
    tables_values = sorted(tables_values.items(), key=lambda x: x[1])
    # page_list = [x for x in range(1, len(pdf))]
    # pages = ','.join(str(e) for e in page_list)
    count = 1
    for x in range(1, len(tables_values)):
        if tables_values[x][0] == 'items':
            break
        else:
            count += 1
    for x in range(0, count):
        try:
            if df[tables_values[x][1][0]:tables_values[x+1][1][0]]:
                val = df[tables_values[x][1][0]:tables_values[x+1][1][0]]
                val_list = val.split("\n")
                for e in val_list[:-2]:
                    try:
                        list_val = []
                        new_list_val = []
                        if re.search(r"\d*\.\d\s[\w\-]+",e):
                            des = re.sub(re.search(r"\d*\.\d\s[\w\-]+",e).group(), "", e)
                            dic_ext = {"line": "", "op": "", "guide": "","mc": "", "description":"", "mfr_partno":re.search(r"\d*\.\d\s[\w\-]+",e).group(), "price":"", "adj":"", "b":"", "hours":"", "r":""}
                            data_extr.append(dic_ext)
                        elif re.search(r"\>\>\s*[\w\s\&\-\:\/]*",e):
                            des = re.sub(re.search(r"\>\>\s*[\w\s\&\-\:\/]*",e).group(), "", e)
                            dic_ext = {"line": "", "op": "", "guide": "","mc": "", "description":re.search(r"\>\>\s*[\w\s\&\-\:\/]*",e).group()[:-1], "mfr_partno":"", "price":"", "adj":"", "b":"", "hours":"", "r":""}
                            data_extr.append(dic_ext)
                        else:
                            yyz = re.search(r"\s{2}", e).group()
                            list_val.extend(e.split(yyz))
                            for g in list_val:
                                if g != "":
                                    new_list_val.append(g)    
                            try:
                                lin, ops = new_list_val[0].split(" ")
                                if re.search(r"\d{2}\s\w", new_list_val[2]):
                                    mc = re.search(r"\d{2}\s\w", new_list_val[2]).group()[:-2]
                                    des =  re.sub(mc, "", new_list_val[2])
                                else:
                                    mc = ""
                                    des = new_list_val[2]
                                if re.search(r"\$\d+\.\d\d\*", new_list_val[4]):
                                    pri = re.search(r"\$\d+\.\d\d\*", new_list_val[4]).group()
                                else:
                                    pri = ""
                                if re.search(r"\+\d+\.\d{2}", new_list_val[5]):
                                    adj = re.search(r"\+\d+\.\d{2}", new_list_val[5]).group()
                                else:
                                    adj = ""
                                b_val = ""
                                for j in range(4,len(new_list_val)-1):
                                    if re.search(r"S\d", new_list_val[j]):
                                        b_val = str(re.search(r"S\d", new_list_val[j]).group())                                
                                hrs = ""
                                if re.search(r"(\d+\.\d+|INC)", new_list_val[-2]):
                                    hrs = re.search(r"(\d+\.\d+|INC)", new_list_val[-2]).group()
                                    
                                r_val = new_list_val[-1]
                                dic_ext = {"line": lin, "op":ops, "guide":new_list_val[1],"mc":mc, "description":des, "mfr_partno":new_list_val[3], "price":pri, "adj":adj, "b":b_val, "hours":hrs, "r":r_val}
                                data_extr.append(dic_ext)
                            except:
                                lin = new_list_val[0]
                                ops = new_list_val[1]
                                if re.search(r"\d{2}\s\w", new_list_val[3]):
                                    mc = re.search(r"\d{2}\s\w", new_list_val[3]).group()[:-2]
                                    des =  re.sub(mc, "", new_list_val[3])
                                else:
                                    mc = ""
                                    des = new_list_val[3]
                                if re.search(r"\$\d+\.\d\d\*", new_list_val[5]):
                                    pri = re.search(r"\$\d+\.\d\d\*", new_list_val[5]).group()
                                else:
                                    pri = ""
                                if re.search(r"\+\d+\.\d{2}", new_list_val[6]):
                                    adj = re.search(r"\+\d+\.\d{2}", new_list_val[6]).group()
                                else:
                                    adj = ""
                                b_val = ""
                                for j in range(5, len(new_list_val)-1):
                                    if re.search(r"S\d", new_list_val[j]):
                                        b_val =str(re.search(r"S\d", new_list_val[j]).group())
                                hrs = ""
                                if re.search(r"(\d+\.\d+|INC)", new_list_val[-2]):
                                    hrs = re.search(r"(\d+\.\d+|INC)", new_list_val[-2]).group()
                                    
                                r_val = new_list_val[-1]
                                dic_ext = {"line": lin, "op":ops, "guide":new_list_val[2],"mc":mc, "description":des, "mfr_partno":new_list_val[4], "price":pri, "adj":adj, "b":b_val, "hours":hrs, "r":r_val}
                                data_extr.append(dic_ext)
                    except:
                        pass
        except:
            print("Error while extracting Damages Table.")
    return data_extr

def extract_estimatetable(df):
    heading = {}
    try:
        if re.search(r"Estimate Total[a-zA-Z0-9\s\-\.\_\,\(\)\/\n\&\%\$\*\#\:\$\@]+Net Total[\s\$\d\,\.]+", df):
            veh_ind = re.search(r"Estimate Total[a-zA-Z0-9\s\-\.\_\,\(\)\/\n\&\%\$\*\#\:\$\@]+Net Total[\s\s$\d\,\.]+", df).span()
            veh = re.search(r"Estimate Total[a-zA-Z0-9\s\-\.\_\,\(\)\/\n\&\%\$\*\#\:\$\@]+Net Total[\s\s$\d\,\.]+", df).group()
            if re.search(r"Labor[a-zA-Z0-9\s\-\.\_\,\(\)\/\n\&\%\$\*\#\:\$\@]+Labor Total", veh):
                lab = re.search(r"Labor[a-zA-Z0-9\s\-\.\_\,\(\)\/\n\&\%\$\*\#\:\$\@]+Labor Total", veh).span()
            else:
                lab = re.search(r"Labor[a-zA-Z0-9\s\-\.\_\,\(\)\/\n\&\%\$\*\#\:\$\@]+Gross Total", veh).span()
            tab2 = veh[:lab[0]]+veh[lab[1]-11:]
            veh_data =  tab2.split("\n")
            for e in veh_data[1:]:
                if re.search(r"\s\s\s+", e):
                    xyz = re.findall(r"\s\s\s+", e)
                    if len(xyz) == 1:
                        tab_data = e.split(re.search(r"\s\s\s+", e).group())
                        heading[tab_data[0]] = tab_data[-1]
                    elif len(xyz) != 1:
                        tab_data = []
                        for f in xyz:
                            tab_data.extend(e.split(re.search(f, e).group()))
                            heading[tab_data[0]] = [tab_data[-2], tab_data[-1]]
                            
        dtatatable = []
        for e in veh_data[1:]:
            lak = e.split(re.search(r"\s\s\s+", e).group())
            lak = lak[0]
            head_data = heading[lak]
            if type(head_data) != list:
                zyx = {'parts': lak, 'amount': head_data}
                dtatatable.append(zyx)
            elif type(head_data) == list:
                zyx = {'parts': lak, 'amount': head_data[-1], 'rate_hour':head_data[-2]}
                dtatatable.append(zyx)
    except Exception as e:
        print(e)
        pass
    return dtatatable

def audatex_preprocessing(pdf):

    header, owner, control_information, inspection, repair, vehicle = {}, {}, {}, {}, {}, {}
    fullpdf = "".join(pdf[x] for x in range(len(pdf)))
    df = re.sub(r"^\s+", "", fullpdf, flags=re.MULTILINE)
    try:
        if re.search(r"\n\s*Owner\s*\n", df):
            own = re.search(r"\n\s*Owner\s*\n", df).end()
        else:
            own = len(df)
        if re.search(r"\n\s*Control Information\s*\n", df):
            c_i = re.search(r"\n\s*Control Information\s*\n", df).end()
        else:
            c_i = len(df)
        if re.search(r"\n\s*Inspection\s*\n", df):
            isp = re.search(r"\n\s*Inspection\s*\n", df).end()
        else:
            isp = len(df)
        if re.search(r"\n\s*Repairer\s*\n", df):
            rep = re.search(r"\n\s*Repairer\s*\n", df).end()
        else:
            rep = len(df)
        if re.search(r"\n\s*Remarks\s*\n", df):
            rem = re.search(r"\n\s*Remarks\s*\n", df).end()
        else:
            rem = len(df)
        if re.search(r"\n\s*Vehicle\s*\n", df):
            veh = re.search(r"\n\s*Vehicle\s*\n", df).end()
        else:
            veh = len(df)

        extract_header(df, header)
        extract_ownerdetails(df, owner, own, c_i)
        extract_controlinformation(df, control_information, c_i, isp)
        extract_inspectionsdetails(df, inspection, isp, rep)
        extract_repairdetails(df, repair, rep, rem)
        veh_data = vehicle_preprocess(df)
        damagesdata = extract_damagetable(df)
        estimate_table = extract_estimatetable(df)
        if veh_data:
            veh_data_1 = re.sub(r"\s\s\s\s[\s]+", "\n", veh_data)
            veh_detail, tab1 = extract_vehicledetails(veh_data_1)
        print(header, '\n\n', owner, '\n\n', control_information, '\n\n',  inspection, '\n\n', \
              repair, '\n\n', veh_detail, '\n\n', tab1, '\n\n', damagesdata, '\n\n', estimate_table)

        return header, owner, control_information, inspection, \
              repair, veh_detail, tab1, damagesdata, estimate_table
    except:
        print(" Error while extracting data ")
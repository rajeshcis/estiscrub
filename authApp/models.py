"""Authentication models."""
from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from jsonfield import JSONField

import collections
import os


class CustomUserManager(BaseUserManager):
    """Well.. using BaseUserManager."""

    def create_user(self, email, password):
        """Create user."""
        if not email:
            raise ValueError("Users must register an email")

        user = self.model(email=CustomUserManager.normalize_email(email))
        user.is_active = True
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """Create superuser."""
        user = self.create_user(email, password)
        user.is_active = True
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):
    """Using email instead of username."""

    first_name = models.CharField(max_length=60, null=True, blank=True)
    last_name = models.CharField(max_length=60, null=True, blank=True)
    email = models.CharField(max_length=255, null=True, blank=True, unique=True)
    password = models.CharField(max_length=100)
    is_active = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True)
    is_staff = models.BooleanField(default=False)
    slug = models.SlugField(max_length=255, blank=True, null=True, unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = CustomUserManager()

    def __str__(self):
        """Str method to return ContactInfo name."""
        return self.email

    def save(self, *args, **kwargs):
        """To save the slug."""
        if not self.id:
            self.slug = slugify("user-" + timezone.now().isoformat())
        super(CustomUser, self).save(*args, **kwargs)


class BaseModel(models.Model):
    """Base model for created and modified date."""

    created_date = models.DateTimeField(auto_now_add=True, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        """Meta class."""

        abstract = True


class InsuranceCompany(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        """Str method to return InsuranceCompany name."""
        return self.name


class InsuranceAppraisalCompany(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        """Str method to return InsuranceAppraisalCompany name."""
        return self.name


class Estimation(BaseModel):
    """Base model for created and modified date."""

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='upload_esti',
                             blank=True, null=True)
    estimate_type = models.TextField(null=True, blank=True)
    workfile_id = models.TextField(null=True, blank=True)
    insurancecompany = models.ForeignKey(InsuranceCompany, on_delete=models.CASCADE, related_name='estimation', blank=True, null=True)
    insurance_company = models.TextField(null=True, blank=True)
    insurance_appraisal_company = models.ForeignKey(InsuranceAppraisalCompany, on_delete=models.CASCADE, related_name='estimation', blank=True, null=True)
    appraiser = models.TextField(null=True, blank=True)
    upload_pdf = models.FileField(upload_to='uploaded_pdf', blank=True, null=True)
    adjuster = models.TextField(null=True, blank=True)
    independent_appraisal_company = models.TextField(null=True, blank=True)
    jobnumber = models.TextField(null=True, blank=True)
    insured = models.TextField(null=True, blank=True)
    owner = models.TextField(null=True, blank=True)
    writtenby = models.TextField(null=True, blank=True)
    policy = models.TextField(null=True, blank=True)
    claim = models.TextField(null=True, blank=True)
    vin = models.TextField(null=True, blank=True)
    license = models.TextField(null=True, blank=True)
    license_number = models.TextField(null=True, blank=True)
    odometer = models.TextField(null=True, blank=True)
    exteriorcolor = models.TextField(null=True, blank=True)
    interiorcolor = models.TextField(null=True, blank=True)
    mileagein = models.TextField(null=True, blank=True)
    mileageout = models.TextField(null=True, blank=True)
    state = models.TextField(null=True, blank=True)
    pointofimpact = models.TextField(null=True, blank=True)
    typeofloss = models.TextField(null=True, blank=True)
    dateofloss = models.TextField(null=True, blank=True)
    dayofrepair = models.TextField(null=True, blank=True)
    production_date = models.TextField(null=True, blank=True)
    condition = models.TextField(null=True, blank=True)
    vehicle_detail = models.TextField(null=True, blank=True)
    deductible = models.TextField(null=True, blank=True)
    uuid = models.TextField(null=True, blank=True)
    vehicle_parts = JSONField(load_kwargs={'object_pairs_hook': collections.OrderedDict}, null=True, blank=True)
    insured_locations = JSONField(load_kwargs={'object_pairs_hook': collections.OrderedDict}, null=True, blank=True)
    is_new = models.BooleanField(default=True)


    def filename(self):
        return os.path.basename(self.upload_pdf.name)
    
    class Meta:
        ordering = ['-created_date']


class EstimationTotals(BaseModel):
    estimation = models.ForeignKey(Estimation, on_delete=models.CASCADE, related_name='estimationtotal', blank=True, null=True)
    category = models.TextField(null=True, blank=True)
    basis = models.TextField(null=True, blank=True)
    rate = models.TextField(null=True, blank=True)
    cost = models.TextField(null=True, blank=True)
    uuid = models.TextField(null=True, blank=True)

    class Meta:
        ordering = ['id']


class EstimationLine(BaseModel):
    estimation = models.ForeignKey(Estimation, on_delete=models.CASCADE, related_name='estimationline', blank=True, null=True)
    line = models.TextField(null=True, blank=True)
    oper = models.TextField(null=True, blank=True)
    repl = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    part_number = models.TextField(null=True, blank=True)
    qty = models.TextField(null=True, blank=True)
    extanded_price = models.TextField(null=True, blank=True)
    labor = models.TextField(null=True, blank=True)
    paint = models.TextField(null=True, blank=True)
    uuid = models.TextField(null=True, blank=True)

    class Meta:
        ordering = ['id']

class Audatex_Owner(models.Model):
    head = models.TextField(null=True, blank=True)
    audatex_type = models.TextField(null=True, blank=True)
    datetime = models.TextField(null=True, blank=True)

    owner_name = models.TextField(null=True, blank=True)
    owner_address = models.TextField(null=True, blank=True)
    owner_cell = models.TextField(null=True, blank=True)
    owner_citystatezip = models.TextField(null=True, blank=True)
    owner_home = models.TextField(null=True, blank=True)
    owner_fax = models.TextField(null=True, blank=True)
    owner_email = models.TextField(null=True, blank=True)

    claim_number = models.TextField(null=True, blank=True)
    insured_policy = models.TextField(null=True, blank=True)
    loss_date_time = models.TextField(null=True, blank=True)
    loss_type = models.TextField(null=True, blank=True)
    deductible = models.TextField(null=True, blank=True)
    claim = models.TextField(null=True, blank=True)
    address1 = models.TextField(null=True, blank=True)
    ci_citystatezip = models.TextField(null=True, blank=True)
    agent = models.TextField(null=True, blank=True)
    address2 = models.TextField(null=True, blank=True)
    work_day1 = models.TextField(null=True, blank=True)
    fax = models.TextField(null=True, blank=True)
    email1 = models.TextField(null=True, blank=True)
    insured = models.TextField(null=True, blank=True)
    address3 = models.TextField(null=True, blank=True)
    home_day = models.TextField(null=True, blank=True)
    cell = models.TextField(null=True, blank=True)
    claim_replacement = models.TextField(null=True, blank=True)
    address4 = models.TextField(null=True, blank=True)
    work_day2 = models.TextField(null=True, blank=True)
    email2 = models.TextField(null=True, blank=True)
    loss_payee = models.TextField(null=True, blank=True)

    inspection_date = models.TextField(null=True, blank=True)
    inspection_type = models.TextField(null=True, blank=True)
    inspection_location = models.TextField(null=True, blank=True)
    contact = models.TextField(null=True, blank=True)
    citystatezip = models.TextField(null=True, blank=True)
    primary_impact = models.TextField(null=True, blank=True)
    seconday_impact = models.TextField(null=True, blank=True)
    driveable = models.TextField(null=True, blank=True)
    rental_assisted = models.TextField(null=True, blank=True)
    assigned_datetime = models.TextField(null=True, blank=True)
    received_date = models.TextField(null=True, blank=True)
    first_contact_datetime = models.TextField(null=True, blank=True)
    appointment_datetime = models.TextField(null=True, blank=True)
    appraiser_name = models.TextField(null=True, blank=True)
    appraiser_license = models.TextField(null=True, blank=True)
    isp_address = models.TextField(null=True, blank=True)
    work_day = models.TextField(null=True, blank=True)
    citystatezip2 = models.TextField(null=True, blank=True)
    fax1 = models.TextField(null=True, blank=True)
    email = models.TextField(null=True, blank=True)

    repairer = models.TextField(null=True, blank=True)
    contact1 = models.TextField(null=True, blank=True)
    rep_address1 = models.TextField(null=True, blank=True)
    rep_citystatezip = models.TextField(null=True, blank=True)
    fax2 = models.TextField(null=True, blank=True)
    taget_complete_datetime = models.TextField(null=True, blank=True)
    days_to_repair = models.TextField(null=True, blank=True)

    license_plate = models.TextField(null=True, blank=True)
    license_state = models.TextField(null=True, blank=True)
    license_expire = models.TextField(null=True, blank=True)
    vin = models.TextField(null=True, blank=True)
    prod_date = models.TextField(null=True, blank=True)
    mileage = models.TextField(null=True, blank=True)
    veh_insp = models.TextField(null=True, blank=True)
    mileage_type = models.TextField(null=True, blank=True)
    condition = models.TextField(null=True, blank=True)
    ext_color= models.TextField(null=True, blank=True)
    code = models.TextField(null=True, blank=True)
    int_color = models.TextField(null=True, blank=True)
    ext_refinish = models.TextField(null=True, blank=True)
    int_refinish = models.TextField(null=True, blank=True)
    ext_paintcode = models.TextField(null=True, blank=True)
    int_trimcode = models.TextField(null=True, blank=True)
    vehicle = models.TextField(null=True, blank=True)

    tab1 = models.TextField(null=True, blank=True)
    tab2 = models.TextField(null=True, blank=True)
    tab3 = models.TextField(null=True, blank=True)
    is_new = models.BooleanField(default=True)


class DamagesTable(models.Model):
    audatex_owner = models.ForeignKey(Audatex_Owner, related_name='damagestable', on_delete=models.CASCADE)
    line = models.TextField(null=True, blank=True)
    op = models.TextField(null=True, blank=True)
    guide = models.TextField(null=True, blank=True)
    mc = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    mfr_partno = models.TextField(null=True, blank=True)
    price= models.TextField(null=True, blank=True)
    adj = models.TextField(null=True, blank=True)
    b = models.TextField(null=True, blank=True)
    hours = models.TextField(null=True, blank=True)
    r = models.TextField(null=True, blank=True)

class EstimateTable(models.Model):
    audatex_owner = models.ForeignKey(Audatex_Owner, related_name='estimatetable', on_delete=models.CASCADE)
    parts = models.TextField(null=True, blank=True)
    rate_hour = models.TextField(null=True, blank=True)
    amount = models.TextField(null=True, blank=True)

class MitchelEstimation(models.Model):
    # head = models.TextField(null=True, blank=True)
    mitchel_type = models.TextField(null=True, blank=True)
    # datetime = models.TextField(null=True, blank=True)

    estimate_date = models.TextField(null=True, blank=True)
    estimate_id = models.TextField(null=True, blank=True)
    estimate_version = models.TextField(null=True, blank=True)
    committed = models.TextField(null=True, blank=True)
    supplement = models.TextField(null=True, blank=True)
    profile_id = models.TextField(null=True, blank=True)
    quote_id = models.TextField(null=True, blank=True)

    insco_info = models.TextField(null=True, blank=True)
    appraiser_info = models.TextField(null=True, blank=True)
    adjuster_info = models.TextField(null=True, blank=True)
    appraiser_address = models.TextField(null=True, blank=True)

    condition = models.TextField(null=True, blank=True)
    type_of_loss = models.TextField(null=True, blank=True)
    date_of_loss = models.TextField(null=True, blank=True)
    date_assign = models.TextField(null=True, blank=True)
    contact_date = models.TextField(null=True, blank=True)
    deductible = models.TextField(null=True, blank=True)
    policy_no = models.TextField(null=True, blank=True)
    claim_number = models.TextField(null=True, blank=True)
    insured = models.TextField(null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    telephone = models.TextField(null=True, blank=True)
    owner = models.TextField(null=True, blank=True)

    description = models.TextField(null=True, blank=True)
    body_style = models.TextField(null=True, blank=True)
    drive_train = models.TextField(null=True, blank=True)
    vin = models.TextField(null=True, blank=True)
    mileage = models.TextField(null=True, blank=True)
    oem_alt = models.TextField(null=True, blank=True)
    parts_profile = models.TextField(null=True, blank=True)
    color = models.TextField(null=True, blank=True)
    license = models.TextField(null=True, blank=True)
    options = models.TextField(null=True, blank=True)
    is_new = models.BooleanField(default=True)

class MitchelEstimationTable(models.Model):
    mitchel_owner = models.ForeignKey(MitchelEstimation, related_name='estimatetable', on_delete=models.CASCADE)
    line = models.TextField(null=True, blank=True)
    entry_number = models.TextField(null=True, blank=True)
    labor_type = models.TextField(null=True, blank=True)
    operation = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    part_number = models.TextField(null=True, blank=True)
    amount = models.TextField(null=True, blank=True)
    labor_units = models.TextField(null=True, blank=True)

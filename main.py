#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 18:47:39 2020

@author: cis
"""


import spacy
from spacy.matcher import Matcher
# import PyPDF2

# # matcher = Matcher(nlp.vocab)

# pdfObj = open('PDF_data/9317 PAGAN Supp 2.pdf', 'rb')

# pdfReader = PyPDF2.PdfFileReader(pdfObj)

# pageObj = pdfReader.getPage(0)

# print(pageObj.extractText())

# pdfObj.close()

import pdftotext

with open("/home/cis/10056 Mapfre-Advanced Appraisal oe.pdf", "rb") as file:
    pdf = pdftotext.PDF(file)

single_space = ""

for i in range(len(pdf[0])):
    e = pdf[0][i]
    if i >= 0:
        f = pdf[0][i-1]
        if (ord(e) == 32 and ord(f) == 32) or e == "\n":
            pass
        else:
            single_space += e


def parse_data():
    pass

def load_model():
    return spacy.load("custom_model")

def parse_NER(ner, data):
    for ent in data.ents:
        print(ent.label_, ent.text)


nlp = load_model()

ner = nlp.get_pipe("ner")

# with open("PDF_data/pdf_log.txt", 'r') as file:
#     data = file.read()
data = single_space

doc = nlp(data)

parse_ner = parse_NER(ner, doc)
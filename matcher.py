#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 18:28:14 2020

@author: aman
"""


import re
import pdftotext

file = open("/media/aman/698F6E1531797531/Data/rajeshcis-estiscrub-7fa744c289f6/PDF_data/8063 VU oe.pdf", "rb")
pdf = pdftotext.PDF(file)

df = re.sub(r"^\s+", "", pdf[0], flags=re.MULTILINE)
df = re.sub("\s{2,}", "   ", df, flags=re.MULTILINE)
df = df.replace('\n', '   ')


if re.search(r"Job Number:\s[-\w]+", df):
    jobnumber = re.search(r"Job Number:\s[-\w]+", df).group()

if re.search(r"Written By:\s[-,\w\s]+(Adjuste|License)", df):
    check = re.search(r"Written By:\s[-,\w\s]+(Adjuste|License)", df).group()
    writtenby = check[:-8]
    
if re.search(r"License Number:\s[-,():/\w\s]+Adjuster", df):
    check = re.search(r"License Number:\s[-,():/\w\s]+Adjuster", df).group()
    licensenumber = check[:-9]
    
if re.search(r"Adjuster:\s[-,()\w\s]+Insured", df):
    check = re.search(r"Adjuster:\s[-,()\w\s]+Insured", df).group()
    adjuster = check[:-8]
    
if re.search(r"Insured:\s[-\w]+(Owner Po|Policy #:)", df):
    check = re.search(r"Insured:\s[-\w]+(Owner Po|Policy #:)", df).group()
    insured = check[:-9]
    
if re.search(r"Claim #:\s[-\w]+", df):
    claim = re.search(r"Claim #:\s[-\w]+", df).group()
    
if re.search(r"Type of Loss:\s\s\s[-,/:;\s\w]+Date", df):
    check = re.search(r"Type of Loss:\s[-,/:;\s\w]+Date", df).group()
    typeofloss = check[:-7]
    
if re.search(r"Date[\w\s]+:\s[-,/:;\s\w]+Days", df):
    check = re.search(r"Date[\w\s]+:\s[-,/:;\s\w]+Days", df).group()
    date = check[:-5]
    
if re.search(r"Days[\w\s]+:\s[-,/:;\s\w]+Point", df):
    check = re.search(r"Days[\w\s]+:\s[-,/:;\s\w]+Point", df).group()
    days = check[:-6]

if re.search(r"Point[\w\s]+:\s[-,/:;\s\w]+Deductible", df):
    check = re.search(r"Point[\w\s]+:\s[-,/:;\s\w]+Deductible", df).group()
    point = check[:-11]
    
if re.search(r"Owner:\s[-,/:;\s\w]+Job Number", df):
    check = re.search(r"Owner:\s[-,/:;\s\w]+Job Number", df).group()
    owner = check[:-11]
    
if re.search(r"VIN:\s\s\s[-,/:;\w]+\s\s\s", df):
    check = re.search(r"VIN:\s\s\s[-,/:;\w]+\s\s\s", df).group()
    vin = check[:-3]
    
if re.search(r"State:\s\s\s[-,/:;\s\w]+\s\s\s", df):
    check = re.search(r"State:\s\s\s[-,/:;\w]+\s\s\s", df).group()
    state = check[:-3]
    
if re.search(r"Odometer:\s\s\s[-,/:;\s\w]+\s\s\s", df):
    check = re.search(r"Odometer:\s\s\s[-,/:;\w]+\s\s\s", df).group()
    odometer = check[:-3]
    
if re.search(r"Condition:\s\s\s[-,/:;\s\w]+\s\s\s", df) and not (r"Condition:\s\s\sTransmission", df):
    check = re.search(r"Condition:\s\s\s[-,/:;\w]+\s\s\s", df).group()
    condition = check[:-3]
    
if re.search(r"Exterior Color:\s\s\s[-,/:;\s\w]+\s\s\s", df):
    check = re.search(r"Exterior Color:\s\s\s[-,/:;\w]+\s\s\s", df).group()
    exterior = check[:-3]
    
if re.search(r"Interior Color:\s\s\s[-,/:;\s\w]+\s\s\s", df) and not (r"Interior Color:\s\s\sLicense", df):
    check = re.search(r"Interior Color:\s\s\s[-,/:;\w]+\s\s\s", df).group()
    interior = check[:-3]
    
if re.search(r"License:\s\s\s[-,/:;\s\w]+\s\s\s", df):
    check = re.search(r"License:\s\s\s[-,/:;\w]+\s\s\s", df).group()
    licence = check[:-3]
    
if re.search(r"Production Date:\s\s\s[-,/:;\s\w]+\s\s\s", df):
    check = re.search(r"Production Date:\s\s\s[-,/:;\w]+\s\s\s", df).group()
    production = check[:-3]
    
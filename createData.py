#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 19:36:30 2020

@author: cis
"""


import os
import pdftotext
import re

files_list = os.listdir("PDF_data")

training_data = []
new_data = []

for f in files_list:
    file = open("PDF_data/" + f, 'rb')
    try:
        pdf = pdftotext.PDF(file)
        for i in range(len(pdf)):
            if "CCC ONE Estimating" in pdf[i]:
                df = re.sub(r"^\s+", "", pdf[0], flags=re.MULTILINE)
                # df = df.replace('\n', ' ')
                df = re.sub("\s{2,}", " ", df, flags=re.MULTILINE)
                training_data.append(df)
                df = df.replace('\n', ' ')
                new_data.append(df)
            else:
                continue
            
    except Exception as e:
        print("Not Valid PDF!!!")


with open("new_training_data.txt", "w") as file:
    for e in training_data:
        file.write("{")
        file.write(e)
        file.write("}")
        file.write("\n\n\n\n")
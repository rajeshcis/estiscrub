#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 19:42:56 2020

@author: aman
"""


from __future__ import unicode_literals, print_function

import plac
import random
from pathlib import Path
import spacy
from spacy.util import minibatch, compounding
# from train_data_1 import LABEL, TRAIN_DATA
# new entity label

@plac.annotations(
    model=("Model name. Defaults to blank 'en' model.", "option", "m", str),
    new_model_name=("New model name for model meta.", "option", "nm", str),
    output_dir=("Optional output directory", "option", "o", Path),
    n_iter=("Number of training iterations", "option", "n", int),
)



def main(model=None, new_model_name="animal", output_dir="custom_model", n_iter=1000):
    LABEL = ["InsCo", "Estimate Type", "Workfile_id", "IndApprCo"]
    random.seed(0)
    if model is not None:
        nlp = spacy.load(model)
        print("Loaded model '%s'" % model)
    else:
        nlp = spacy.blank("en")
        print("Created blank 'en' model")

    if "ner" not in nlp.pipe_names:
        ner = nlp.create_pipe("ner")
        nlp.add_pipe(ner)
    else:
        ner = nlp.get_pipe("ner")
    
    for label in LABEL:
        ner.add_label(label)
        
    ner.add_label("POBOX")
    if model is None:
        optimizer = nlp.begin_training()
    else:
        optimizer = nlp.resume_training()
        
    move_names = list(ner.move_names)
  
    test_text = input("PDF data")
    doc = nlp(test_text)

    if output_dir is not None:
        print("Loading from", output_dir)
        nlp2 = spacy.load(output_dir)
        # Check the classes have loaded back consistently
        assert nlp2.get_pipe("ner").move_names == move_names
        doc2 = nlp2(test_text)
        for ent in doc2.ents:
            print(ent.label_, ent.text)
    import pdb; pdb.set_trace()


if __name__ == "__main__":
    plac.call(main)

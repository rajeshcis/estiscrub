#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 21:19:50 2020

@author: aman
"""


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 22:28:29 2020

@author: aman
"""

import os
import pdftotext
import re
import copy

files_list = os.listdir("PDF_data")

training_data = []
new_data = []
file_name = []
for f in files_list:
    file = open("PDF_data/" + f, 'rb')
    try:
        pdf = pdftotext.PDF(file)
        file_name.append(f)
        for i in range(len(pdf)):
            if "CCC ONE Estimating" in pdf[i]:
                # import pdb; pdb.set_trace()
                df = re.sub(r"^\s+", "", pdf[0], flags=re.MULTILINE)
                # df = df.replace('\n', ' ')
                df = re.sub("\s{2,}", " ", df, flags=re.MULTILINE)
                training_data.append(df)
                df = df.replace('\n', ' ')
                wfid = re.search(r"\bWorkfile ID: \w{8}\b", df).span()
                wfid_t = df[wfid[0]:wfid[1]]
                df = re.sub(r"\bWorkfile ID: \w{8}\b", "", df)
                if re.search(r"\bClaim #: [-\w]+\b ", df[:wfid[0]]):
                    df = re.sub(r"\b Claim #: [-\w]+\b", "", df)
                df = wfid_t + " " + df
                new_data.append(df)
                new_data_org = copy.copy(new_data)
                
            else:
                continue
            
    except Exception as e:
        continue

list_cord = {}
heading = {}
esti_type = {}
for_data = {}
diction = {}
ddd = 0
for i in range(len(new_data)):
    try:
        if re.search(r"(Estimate|Supplement) [\w]+ (Record|Summary)", new_data[i]):
            aaa = re.search(r"\bWorkfile ID: \w{8}\b", new_data[i]).span()
            list_cord[i] = aaa
            dist = aaa[1] - aaa[0]
            new_data[i] = re.sub(r"\bWorkfile ID: \w{8}\b", " "*dist, new_data[i])
                       
            if re.search(r"[-:@%!$&./|#,()\s\w]+(Estimate|Suppleme)", new_data[i]):
                yx = re.search(r"[-:@%!$&./|#,()\s\w]+(Estimate|Suppleme)", new_data[i]).span()
                if re.findall(r"\s{3,}", new_data[i][:300]):
                    xy = (yx[0]+22, yx[1]-9)
                    sp = re.search(r"\s{3,}", new_data[i][:350]).span()
                    # if i == 214:
                    #     import pdb; pdb.set_trace()
                    if re.search(r"For: [-:@%!$&./|#,()\s\w]+(Estimate|Suppleme)", new_data[i][xy[0]:yx[1]]):
                        dt = re.search(r"For: [-:@%!$&./|#,()\s\w]+(Estimate|Suppleme)", new_data[i]).span()
                        ddd = (dt[0]+5, dt[1]-9)
                        bbb = (sp[1], dt[0]-1)
                    else:
                        bbb = (sp[1], xy[1])
                    # import pdb; pdb.set_trace()
                    heading[i] = bbb
            ccc = re.search(r"(Estimate of Record|Supplement of Record 1 with Summary|Supplement of Record 2 with Summary)", new_data[i]).span()
            esti_type[i] = ccc
           
            if ddd != 0:
                diction[i] = [aaa,ccc,bbb,ddd]
                ddd = 0
            else:
                diction[i] = [aaa, ccc, bbb]

    except:
        ddd = 0
        continue


with open("train_data_2.py", "w") as fin:
    fin.write("""LABEL = [ "Workfile_id", "Estimate Type", "InsCo", "IndApprCo"]
# training data
# Note: If you're using an existing model, make sure to mix in examples of
# other entity types that spaCy correctly recognized before. Otherwise, your
# model might learn the new type, but "forget" what it previously knew.
# https://explosion.ai/blog/pseudo-rehearsal-catastrophic-forgetting
TRAIN_DATA = [""")
    for e in diction.keys():
        if len(diction[e]) == 3:
            fin.write("(")
            fin.write("\n")
            # dt = [diction[e][1][0]:diction[e][1][1]]
            fin.write("'''")
            # fin.write(str(e))
            fin.write(new_data_org[e][0:800])
            fin.write("'''")
            fin.write(",")
            fin.write("\n")
            fin.write("{")
            
            u, v, w, x, y, z = diction[e][0][0], diction[e][0][1], diction[e][1][0], diction[e][1][1], diction[e][2][0], diction[e][2][1]
            # if len(diction[e][1]) == 3:
            #     u1, v1, u2, v2, u3, v3 = diction[e][1][0][0], diction[e][1][0][1], diction[e][1][1][0], diction[e][1][1][1], diction[e][1][2][0], diction[e][1][2][1]
            #     fin.write(f"'entities': [({u1}, {v1}, LABEL[0]), ({u2}, {v2}, LABEL[0]), ({u3}, {v3}, LABEL[0]), ({w}, {x}, LABEL[2]), ({y}, {z}, LABEL[1])]")
            # elif len(diction[e][1]) == 2:
            #     u1, v1, u2, v2 = diction[e][1][0][0], diction[e][1][0][1], diction[e][1][1][0], diction[e][1][1][1]
            #     fin.write(f"'entities': [({u1}, {v1}, LABEL[0]), ({u2}, {v2}, LABEL[0]), ({w}, {x}, LABEL[2]), ({y}, {z}, LABEL[1])]")
                
            # elif len(diction[e][1]) == 1:
            #     u1, v1 = diction[e][1][0][0], diction[e][1][0][1]
            fin.write(f"'entities': [({u}, {v}, LABEL[0]), ({w}, {x}, LABEL[1]), ({y}, {z}, LABEL[2])]")
            
            fin.write("},")
            fin.write("\n")
            fin.write("),")
            fin.write("\n")
        elif len(diction[e]) == 4:
            fin.write("(")
            fin.write("\n")
            # dt = [diction[e][1][0]:diction[e][1][1]]
            fin.write("'''")
            # fin.write(str(e))
            fin.write(new_data_org[e][0:800])
            fin.write("'''")
            fin.write(",")
            fin.write("\n")
            fin.write("{")
            # import pdb; pdb.set_trace()
            u, v, w, x, y, z, p, q = diction[e][0][0], diction[e][0][1], diction[e][1][0], diction[e][1][1], diction[e][2][0], diction[e][2][1],  diction[e][3][0], diction[e][3][1]
            # # fin.write(f"'entities': [({u}, {v}, LABEL[0]), ({w}, {x}, LABEL[2]), ({y}, {z}, LABEL[1]), ({p}, {q}, LABEL[3])]")
            # if len(diction[e][1]) == 3:
            #     u1, v1, u2, v2, u3, v3 = diction[e][1][0][0], diction[e][1][0][1], diction[e][1][1][0], diction[e][1][1][1], diction[e][1][2][0], diction[e][1][2][1]
            #     fin.write(f"'entities': [({u1}, {v1}, LABEL[0]), ({u2}, {v2}, LABEL[0]), ({u3}, {v3}, LABEL[0]), ({w}, {x}, LABEL[2]), ({y}, {z}, LABEL[1]), ({p}, {q}, LABEL[3])]")
            # elif len(diction[e][1]) == 2:
            #     u1, v1, u2, v2 = diction[e][1][0][0], diction[e][1][0][1], diction[e][1][1][0], diction[e][1][1][1]
            #     fin.write(f"'entities': [({u1}, {v1}, LABEL[0]), ({u2}, {v2}, LABEL[0]), ({w}, {x}, LABEL[2]), ({y}, {z}, LABEL[1]), ({p}, {q}, LABEL[3])]")
                
            # elif len(diction[e][1]) == 1:
            #     u1, v1 = diction[e][1][0][0], diction[e][1][0][1]
            fin.write(f"'entities': [({u}, {v}, LABEL[0]), ({w}, {x}, LABEL[1]), ({y}, {z}, LABEL[2]), ({p}, {q}, LABEL[3])]")
                
            fin.write("},")
            fin.write("\n")
            fin.write("),")
            fin.write("\n")
    fin.write("]")
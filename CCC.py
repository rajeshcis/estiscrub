#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 18 15:43:00 2020

@author: cis
"""
import pdftotext
import re

file = "/home/cis/estiscrub/CCC_files/8612 DROHAN s1.pdf"

with open(file, "rb") as f:
    pdf = pdftotext.PDF(f)
header, owner, control_information, inspection, repair, vehicle = {}, {}, {}, {}, {}, {}
fullpdf = "".join(pdf[x] for x in range(len(pdf)))
df = re.sub(r"^\s+", "", fullpdf, flags=re.MULTILINE)

tables_values = {}
data_extr = []
try:
    if re.search(r"\n\s*Line\s", df):
        line = re.search(r"\n\s*Line\s", df)
        tables_values['line'] = [line.start(), line.group()]
        
    
    if re.search(r"\s*SUBTOTALS\s", df):
        subtotal = re.search(r"\s*SUBTOTALS\s", df)
        tables_values['subtotal'] = [subtotal.end(), subtotal.group()]
        
    df = df[tables_values['line'][0]:tables_values['subtotal'][0]+55]
    
    if re.search(r'\d{1,2}\/\d{2}\/\d{4}[0-9\w\s\d\*\:\\\/\#\-]*(AM|PM)\s*\d*\s*Page\s\d\n', df):
        df = re.sub(r'\d{1,2}\/\d{2}\/\d{4}[0-9\w\s\d\*\:\\\/\#\-]*(AM|PM)\s*\d*\s*Page\s\d\n',"", df)
    if re.search(r'PDF created with pdfFactory Pro trial version www.pdffactory.com\n', df):
        df = re.sub(r'PDF created with pdfFactory Pro trial version www.pdffactory.com\n',"", df)
    if re.search(r'Supplement[a-zA-Z0-9\s]+\n', df):
        df = re.sub(r'Supplement[a-zA-Z0-9\s]+\n',"", df)
    if re.search(r'Owner[a-zA-Z0-9\:\,\.\s]+\s*Job Number[a-zA-Z0-9\:\,\.\s\-]+\s*\n', df):
        df = re.sub(r'Owner[a-zA-Z0-9\:\,\.\s]+\s*Job Number[a-zA-Z0-9\:\,\.\s\-]+\s*\n',"", df)
    if re.search("\d{1,2}\/\d{1,2}\/\d{4}\s\d{1,2}.*(AM|PM)", df):
        df = re.sub("\d{1,2}\/\d{1,2}\/\d{4}\s\d{1,2}.*(AM|PM)", "", df)

    # import pdb; pdb.set_trace()

    df_list = df.split("\n")
    df_list.remove("")
    col_names = df_list[:2]
    df_list = df_list[2:]
    lines = []
    headings = []
    oper = []
    description = []
    quantity = []
    part_number = []

    for idx, line in enumerate(df_list):
        spec_1, spec_2 = "", ""
        if re.search(r"^([0-9]{1,3})", line):
            data = re.search(r"^([0-9]{1,3})", line).group()
            line = re.sub(r"^([0-9]{1,3})", "", line)
            df_list[idx] = line
            data = re.sub("\n", "", data)
        else:
            data = ""
        
        lines.append(data)
            
        if re.search(r"(##|\*\*|#|\*)", line):
            spec_1 = re.search(r"(##|\*\*|#|\*)", line).group()
            line = re.sub(r"(##|\*\*|#|\*)", "", line)
        if re.search(r"(S01|S02|S03|S04|S05|S06|S07)", line):
            spec_2 = re.search(r"(S01|S02|S03|S04|S05|S06|S07)", line).group()
            line = re.sub(r"(S01|S02|S03|S04|S05|S06|S07)", "", line)
        # spec = re.search(r"(S01|S02|S03|S\d*)", line).group()
            df_list[idx] = line
        spec = spec_1 + " " + spec_2
        headings.append(spec)
        
        if re.search(r"(R&I|Subl|Rpr|Repl|Algn|Blnd|Refn)", line):
            operation = re.search(r"(R&I|Subl|Rpr|Repl|Algn|Blnd|Refn)", line).group()
            line = re.sub(r"(R&I|Subl|Rpr|Repl|Algn|Blnd|Refn)", "", line)
            df_list[idx] = line
        else:
            operation = ""
        oper.append(operation)
        
        if re.search(r"([A-Z]|[a-z]).+", line):
            data = re.search(r"([A-Z]|[a-z]).+", line).group()
            desc = re.split(r"\s\s\s", data)[0]
            data = data.replace(desc, "") #re.sub(desc, "", data)
            line = line.replace(desc, "    ")
            df_list[idx] = line
        else:
            desc = ""
        
        description.append(desc.strip())
        
        if re.search(r"\s\d{1,2}\s", line):
            qty = re.search(r"\s\d{1,2}\s", line).group()
            line = line.replace(qty, "    ")
            df_list[idx] = line
        else:
            qty = ""
        
        quantity.append(qty.strip())
        
        if re.search(r"(\s*\d*\w*\d*\s)|(\s*\d\d\d*\s)", line):
            p_number = re.search(r"(\s*\d*\w*\d*\s)|(\s*\d\d\d*\s)", line).group()
            line = line.replace(p_number, " ")
            p_number = p_number.strip()
            df_list[idx] = line
        else:
            p_number = ""
            
        part_number.append(p_number)
            
from tabula import read_pdf
import pandas as pd
import camelot

tables = camelot.read_pdf(file, flavor='stream', pages='all',
                          split_text=True, layout_kwargs={'detect_vertical': True})

line_items_dict = {}

def tab1(tables):
    for e in tables:
        for f in e.df.iterrows():
            try:
                if "Line" in f[1].any() or "Line" in e.df.any() or "REAR BUMPER" in f[1].any():
                    return e
            except:
                pass
            try:
                if "Line" in e.df:
                    return e
            except:
                pass
             
ab_tab = tab1(tables)

ab = ab_tab.df

if ab.iloc[1].str.contains("Line").any():
    cols = list(ab.iloc[1])
    ab = ab.drop(ab[[0,1]])
elif ab.iloc[2].str.contains("Line").any():
    cols = list(ab.iloc[2])
    ab = ab.drop(ab[[0,1]])
elif ab.iloc[3].str.contains("Line").any():
    cols = list(ab.iloc[3])
    ab = ab.drop(ab[[0,1,2,3]])
elif ab.iloc[4].str.contains("Line").any():
    cols = list(ab.iloc[4])
    ab = ab.drop(ab[[0,1,2,3,4]])


# ab = ab.drop(ab[[2,3]])

# ab.columns = cols

# ab_dict = ab.to_dict('list')
# line_items_dict["Extended Price $"] = ab_dict["Extended"]

labor= []
# re.sub(r"\w*", tt.iloc[0])
i = 0
tbl = None

for table in tables:
    if ab.shape[1] >= table.shape[1]:
        tbl = table.df
        if tbl.iloc[0].str.contains("Owner").any() or \
            tbl.iloc[0].str.contains("Job Number").any() \
            or tbl.iloc[0].str.contains("Own").any():
            tbl = tbl.drop(tbl[[0, 1]])
            tbl_first_element = tbl.iloc[0, tbl.columns.get_loc(0)]
            tbl_first_element = re.sub(r"\s[A-Z]*", "", tbl_first_element)
            if re.search(r"\d*", tbl_first_element):
                tbl_first_element = int(tbl_first_element)
            ab_first_element = int(ab.iloc[-1, 0])
            if ab_first_element + 1 == tbl_first_element:
                import pdb; pdb.set_trace()
                ab = pd.concat([ab, tbl], ignore_index=True)

            


for table in tables:
    if i < tables.n -1:
        if tbl is None and table.shape[1] == tables[i+1].shape[1] \
           and table.shape[1] > 5:
            if table.df.iloc[0].str.contains("Owner").any() or \
                table.df.iloc[0].str.contains("Job Number").any() \
                or table.df.iloc[0].str.contains("Own").any():
                table.df = table.df.drop(table.df[[0, 1]])
            if table.df.iloc[:, -2].str.contains(r"(\.\d*|Incl.*)").any():
                labor.extend(table.df.iloc[:, -2])
            tbl  = pd.concat([tbl, table.df], ignore_index=True)
        elif table.shape[1] == tables[i+1].shape[1] and table.shape[1] > 5:
            # tbl = table.df
            if table.df.iloc[0].str.contains("Owner").any() or \
                table.df.iloc[0].str.contains("Job Number").any() \
                or table.df.iloc[0].str.contains("Own").any():
                table.df = table.df.drop(table.df[[0, 1]])
            if table.df.iloc[:, -2].str.contains(r"(\.\d*|Incl.*)").any():
                labor.extend(table.df.iloc[:, -2])
            tbl = pd.concat([tbl, table.df], ignore_index=True)
        elif tbl is not None and tbl.shape[1] == table.df.shape[1]:
            if table.df.iloc[0].str.contains("Owner").any() or \
                table.df.iloc[0].str.contains("Job Number").any() \
                or table.df.iloc[0].str.contains("Own").any():
                table.df = table.df.drop(table.df[[0, 1]])
            if table.df.iloc[:, -2].str.contains(r"(\.\d*|Incl.*)").any():
                labor.extend(table.df.iloc[:, -2])                    
            tbl = pd.concat([tbl, table.df], ignore_index=True)
    i+=1
    


line_items_dict["Line"] = lines
line_items_dict["Heading"] = headings
line_items_dict["Oper"] = oper
line_items_dict["Desciption"] = description
line_items_dict["Qty"] = quantity
line_items_dict["Part Number"] = part_number

line_items_df = pd.DataFrame(line_items_dict, columns=line_items_dict.keys())   

    
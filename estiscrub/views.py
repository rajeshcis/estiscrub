"""Main application controllers."""
# from django.http import HttpRequest
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import login
from django.contrib.auth.views import LogoutView
from django.contrib.auth.forms import PasswordResetForm
from django.contrib import messages
from django.views.generic.list import ListView
from django.views.generic.edit import DeleteView 
from authApp.forms import SignUpForm, LoginForm, UploadPdfForm
from authApp.models import Estimation, Audatex_Owner, MitchelEstimation


class HomeView(LoginRequiredMixin, ListView):
    """Docstring for HomeView."""

    model = Estimation
    paginate_by = 100  # if pagination is desired
    context_object_name = 'my_estimations'
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = ''
        context['form'] = UploadPdfForm()
        context['audatex_objs'] = Audatex_Owner.objects.all()
        context['mitchel_objs'] = MitchelEstimation.objects.all()
        context['active_type'] = self.request.GET.get('type', 'ccc')
        return context

class SignupView(View):
    """Docstring for HomeView."""

    def get(self, request, *args, **kwargs):
        """To get Dashboard."""
        return render(request, 'signup.html', {'signup': SignUpForm()})

    def post(self, request, *args, **kwargs):
        """Create new user."""
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = True
            user.save()
            return redirect('home')
        return render(request, 'signup.html', {'signup': form})


class LoginView(View):
    """Docstring for HomeView."""

    def get(self, request, *args, **kwargs):
        """To get Dashboard."""
        if not request.user.is_authenticated:
            return render(request, 'login.html', {'login': LoginForm()})
        return redirect('home')

    def post(self, request, *args, **kwargs):
        """To authenticate the user."""
        form = LoginForm(request.POST)
        password = request.POST.get('password')
        username = request.POST.get("email")
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            messages.success(request, 'login successfully')
            return redirect('home')
        messages.error(request, 'not valid crediantial')
        return render(request, 'login.html', {'login': form})


class LogoutView(LogoutView):
    """Logout view to user."""

    template_name = 'login.html'


class ForgotView(View):
    """Docstring for HomeView."""

    def get(self, request, *args, **kwargs):
        """To get Dashboard."""
        return render(request, 'forgot.html', {'form': PasswordResetForm()})

    def post(self, request, *args, **kwargs):
        """To authenticate the user."""
        form = PasswordResetForm(request.POST)
        if form.is_valid():
            form.save(from_email='rajavi.g@cisinlabs.com', request=request)
            messages.success(request, 'Email has been send to your email address')
            return redirect('login')
        else:
            messages.error(request, 'Email not found.')
            return render(request, 'forgot.html', {'form': form})

class DeleteEstimationView(DeleteView):
    """Delete Estimation View."""

    model = Estimation
    success_url ="/?type=ccc"

class DeleteAudatexView(DeleteView):
    """Delete Estimation View."""

    model = Audatex_Owner
    success_url ="/?type=audatex"

class DeleteMitchelView(DeleteView):
    """Delete Estimation View."""

    model = MitchelEstimation
    success_url ="/?type=mitchel"
"""Estiscrub URL Configuration.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from .views import HomeView, SignupView, LoginView, \
    LogoutView, ForgotView, DeleteEstimationView, \
    DeleteAudatexView, DeleteMitchelView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^signup/$', SignupView.as_view(), name='signup'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^forgot/$', ForgotView.as_view(), name='forgot'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(
            template_name='password_reset_confirm.html'), name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.PasswordResetCompleteView.as_view(
        template_name='password_reset_complete.html'), name='password_reset_complete'),
    url(r'^esti/', include(('authApp.urls', 'details'), namespace='details')),
    url(r'^(?P<pk>[0-9]+)/delete-estimation/$', DeleteEstimationView.as_view(), name='delete_estimation'),
    url(r'^(?P<pk>[0-9]+)/delete-audatex/$', DeleteAudatexView.as_view(), name='delete_audatex'),
    url(r'^(?P<pk>[0-9]+)/delete-mitchel/$', DeleteMitchelView.as_view(), name='delete_mitchel'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

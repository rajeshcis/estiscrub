#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 19:49:26 2020

@author: cis
"""
from __future__ import unicode_literals, print_function

import plac
import random
from pathlib import Path
import spacy
from spacy.util import minibatch, compounding
from train_data_2 import LABEL, TRAIN_DATA
# new entity label

@plac.annotations(
    model=("Model name. Defaults to blank 'en' model.", "option", "m", str),
    new_model_name=("New model name for model meta.", "option", "nm", str),
    output_dir=("Optional output directory", "option", "o", Path),
    n_iter=("Number of training iterations", "option", "n", int),
)

def main(model="test_model", new_model_name="animal", output_dir="final_model", n_iter=200):
    """Set up the pipeline and entity recognizer, and train the new entity."""
    random.seed(0)
    if model is not None:
        nlp = spacy.load(model)  # load existing spaCy model
        print("Loaded model '%s'" % model)
    else:
        nlp = spacy.blank("en")  # create blank Language class
        print("Created blank 'en' model")
    # Add entity recognizer to model if it's not in the pipeline
    # nlp.create_pipe works for built-ins that are registered with spaCy
    if "ner" not in nlp.pipe_names:
        ner = nlp.create_pipe("ner")
        nlp.add_pipe(ner)
    # otherwise, get it, so we can add labels to it
    else:
        ner = nlp.get_pipe("ner")
    
    for label in LABEL:
        ner.add_label(label)  # add new entity label to entity recognizer
    # Adding extraneous labels shouldn't mess anything up
    ner.add_label("POBOX")
    if model is None:
        optimizer = nlp.begin_training()
    else:
        optimizer = nlp.resume_training()
    move_names = list(ner.move_names)
    # get names of other pipes to disable them during training
    pipe_exceptions = ["ner", "trf_wordpiecer", "trf_tok2vec"]
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe not in pipe_exceptions]
    with nlp.disable_pipes(*other_pipes):  # only train NER
        sizes = compounding(1.0, 4.0, 1.001)
        # batch up the examples using spaCy's minibatch
        for itn in range(n_iter):
            random.shuffle(TRAIN_DATA)
            batches = minibatch(TRAIN_DATA, size=sizes)
            losses = {}
            for batch in batches:
                texts, annotations = zip(*batch)
                nlp.update(texts, annotations, sgd=optimizer, drop=0.35, losses=losses)
            print("Losses", losses)

    # test the trained model
    test_text = '''Workfile ID: 3f5f4c0f LIBERTY MUTUAL INSURANCE COMPANY Albany Claim Office RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW 3 Lear Jet Lane Suite 100 Latham, NY 12110 Phone: (800) 252-5730 Fax: (800) 632-3704 Supplement of Record 1 with Summary Written By: TODD HEYBECK, 9/6/2019 3:14:06 PM Adjuster: DICKINSON, KARAH, (800) 252-5730 Business Insured: BRENDA FRITZ Owner Policy #: MARI Type of Loss: COLL - Collision Date of Loss: 12/27/2018 12:00 AM Days to Repair: 6 Coverage Point of Impact: 12 Front Deductible: 500.00 Owner (Insured): Inspection Location: Appraiser Information: Repair Facility: BRENDA FRITZ 290 AUTO BODY Todd.Heybeck@Libertymutual.com 290 AUTO BODY 10 LUDINGTON RD 1 STOWELL AVE (508) 320-0429 1 STOWELL AVE WORCESTER, MA 01602-3223 WORCESTER, MA 01606 WORCESTER, MA 01606 (5'''
    doc = nlp(test_text)
    print("Entities in '%s'" % test_text)
    for ent in doc.ents:
        print(ent.label_, ent.text)

    # save model to output directory
    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
        nlp.meta["name"] = new_model_name  # rename model
        nlp.to_disk(output_dir)
        print("Saved model to", output_dir)

        # test the saved model
        print("Loading from", output_dir)
        nlp2 = spacy.load(output_dir)
        # Check the classes have loaded back consistently
        assert nlp2.get_pipe("ner").move_names == move_names
        doc2 = nlp2(test_text)
        for ent in doc2.ents:
            print(ent.label_, ent.text)


if __name__ == "__main__":
    plac.call(main)

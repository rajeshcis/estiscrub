import tabula
    #pandas_options={"names":["Line", "Heading", "Oper", "Description", "Part Number", "OTY", "Extended", "Labor", "Paint"]})
#tabula.convert_into("/home/cis/Downloads/abcd.pdf", "/home/cis/tables1.tsv", output_format="tsv", multiple_tables=True, pages="all")

import pandas as pd
import numpy as np
import re

tables = tabula.read_pdf(
    "/media/aman/698F6E1531797531/Data/rajeshcis-estiscrub-7fa744c289f6/PDF_data/7759 westerlind original.pdf",
    multiple_tables=True, 
    pages='all')

for e in tables[0]:
    if e.shape[1] == 6:
        df = e.drop([0, 1])
        df.columns= [0,1,2,3,4,5]

# new_df = pd.DataFrame(columns=["Line", "Heading", "Oper", "Description", "Part Number", "OTY", "Extended", "Labor", "Paint"])
new_df = pd.DataFrame(columns=["Line"])

val = 0
qty = 0
amt = 0
amt_lis = {}
qty_lis = {}
rem = {}
arem = {}
qt_rem = {}
part_no = {}
amt_dic = {}

for e, f in zip(df[2], df[0]):
    try:
         if not np.isnan(e):
             pass
    except:   
        # import pdb; pdb.set_trace()
        if re.search(r"\s[A-Z0-9]{10,12}", e) or re.search(r"\s\d\s", e) or re.search(r"\d+\.\d+", e):
            if re.search(r"\s[A-Z0-9]{10,12}", e):
                val = re.search(r"\s[A-Z0-9]{10,12}", e).group()
                part_no[f] = val
                rem[f] = re.sub(r"\s[A-Z0-9]{10,12}", "", e)
                val = 0
            if re.search(r"\s\d\s", e):
                qty = re.search(r"\s\d\s", e).group()
                rem[f] = re.sub(r"\s\d\s", " ", rem[f])
                qty_lis[f] = qty
                qty = 0
            if re.search(r"\d+\.\d+", e):
                amt = re.search(r"\d+\.\d+", e).group()
                rem[f] = re.sub(r"\d+\.\d+", "", rem[f])
                amt_lis[f] = amt
                amt = 0
        else:
            arem[f] = e

            
head = {}
oper = {}
for e, f, gg, hh, ii, jj in zip(df[1], df[0], df[2], df[3], df[4], df[5]):
    # import pdb; pdb.set_trace()
    try:
        if np.isnan(gg) and np.isnan(hh) and np.isnan(ii) and np.isnan(jj):
            head[f] = e
    except:
        try:
            if np.isnan(e):
                pass
        except:
            if re.search(r"[\*\#]+", e):
                e = re.sub(r"[\*\#]+", "", e)
        oper[f] = e


labor_dic = {}

for e, f in zip(df[4], df[0]):
    # import pdb; pdb.set_trace()
    try:
        if np.isnan(e):
            pass
            
    except:
        labor_dic[f] = e
        
        
paint_dic = {}

for e, f in zip(df[5], df[0]):
    # import pdb; pdb.set_trace()
    try:
        if np.isnan(e):
            pass
    except:
        paint_dic[f] = e
        
new_df["Line"] = range(df.shape[0])

all_val = {}
for i in range(1, df.shape[0]+1):
    all_val[i] = {}
    
for i in range(1, df.shape[0]+1):
    if str(i) in head.keys():
        all_val[i]['head'] = head[str(i)] 
    if str(i) in oper.keys():
        all_val[i]['oper'] = oper[str(i)] 
    if str(i) in rem.keys():
        all_val[i]['discription'] = rem[str(i)]
    if str(i) in arem.keys():
        all_val[i]['discription'] = arem[str(i)]
    if str(i) in part_no.keys():
        all_val[i]['part number'] = part_no[str(i)]
    if str(i) in qty_lis.keys():
        all_val[i]['qty'] = qty_lis[str(i)]
    if str(i) in amt_lis.keys():
        all_val[i]['extended price'] = amt_lis[str(i)]
    if str(i) in labor_dic.keys():
        all_val[i]['labor'] = labor_dic[str(i)]
    if str(i) in paint_dic.keys():
        all_val[i]['paint'] = paint_dic[str(i)]
        

# for e, f in oper.items():
#     for x in new_df["Line"]:
#         # import pdb; pdb.set_trace()
#         new_df["Oper2"] = np.where(x == e, f, np.NaN)


# #table = tables[1]
# #table.to_csv("/home/cis/table_1.csv")

# #with open ("/home/cis/table_1.csv", "w") as f:
# #    for 
# #    f.write(table)
    
# table1 = tables[1]
# header0 = table1.iloc[0]
# table1.columns = header0
# table1 = table1.drop(0, axis=0)
# table1.to_csv("/home/cis/table_3.csv")
# #j = 0
# #for f in table1:
# #    f.to_csv(f"file1_{i}.csv", index=False)
# ##    j += 1
# #with open ("/home/cis/table_2.csv", "w") as g:
# #    g.write(table1)


# #for e in tables:
# #    print(e)
# #    
# #    for f in e:
# #        print(f)
# #

# table3 = tables[5]
# table3 = table3.drop(8, axis=1)
# #table3.to_csv("/home/cis/table_1.csv")
# #for e in table3[3]:
# #    print(e)
# #    if e[-1].isalnum() and len(e[-1]) == 10:
# #        print(e[-1])
# header = table3.iloc[0]
# table3.columns = header
# table3 = table3.drop(0, axis=0)
# table3.to_csv("/home/cis/table_1.csv")


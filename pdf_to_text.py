"""
Extract PDF text using PDFMiner. Adapted from
http://stackoverflow.com/questions/5725278/python-help-using-pdfminer-as-a-library
"""
# import os

from os import listdir
from os.path import isfile, join
import PyPDF2
# import re
pdfpath = "/home/cis/InvoiceExtraction/PDF_data"
txtpath = "/home/cis/InvoiceExtraction/txt_data"
onlyfiles = [f for f in listdir("/home/cis/InvoiceExtraction/PDF_data") if isfile(join(pdfpath, f))]

def write_data(data, name):
    name = name[:-3]+"txt"
    txt_file = open(txtpath+"/"+name, "w")
    txt_file.write(data)
    print("Done")


def pdf_to_text(path, pdfname):   
    pdf_file = open(path+"/"+pdfname, 'rb')
    read_pdf = PyPDF2.PdfFileReader(pdf_file)
    number_of_pages = read_pdf.getNumPages()
    
    page = read_pdf.getPage(0)
    page_content = page.extractText()
    write_data(page_content, pdfname)
    
    
for e in onlyfiles:
    try:
        pdf_to_text(pdfpath, e)
    except:
        continue
import re

def extracting(data, cla):
    aa, bb, cc, dd = 0, 0, 0, 0
    ext = {"InsCo": 0, "Estimate Type": 0, "Workfile_id": 0, "IndApprCo": 0, "claim_id": 0}
    ext["claim_id"] = cla
    aa = data[:21]
    ext["Workfile_id"] = aa[-8:]
    dd = re.search(r"(Estimate of Record|Supplement of Record 1 with Summary|Supplement of Record 2 with Summary|Preliminary Estimate|Supplement of Record 1 Summary|Supplement of Record 2 Summary|Preliminary Supplement 1 with Summary)", data).group()
    ext['Estimate Type'] = dd
    if re.search(r"For:\s[-:@%!$&./|#,()\s\w]+(Prelimin|Estimate|Suppleme)", data[22:300]):
        bb = re.search(r"[-:@%!$&./|#,()\s\w]+For:\s", data[22:300]).group()
        ext["InsCo"] = bb[:-5]
        cc = re.search(r"For: [-:@%!$&./|#,()\s\w]+(Prelimin|Estimate|Suppleme)", data[22:300]).group()
        ext["IndApprCo"] = cc[5:-9]
    else:
        bb = re.search(r"[-:@%!$&./|#,()\s\w]+(Prelimin|Estimate|Suppleme)", data[22:]).group()
        ext["InsCo"] = bb[:-9]
    return ext


def match_func(dfd):
    matches = {}
    # import pdb; pdb.set_trace()
    if re.search(r"Job Number:\s[-\w]+", dfd):
        jobnumber = re.search(r"Job Number:\s[-\w]+", dfd).group()
        matches["Job Number"] = jobnumber

    if re.search(r"Written By:\s[-,\w\s]+(Adjuste|License|Insured)", dfd):
        check = re.search(r"Written By:\s[-,\w\s]+(Adjuste|License|Insured)", dfd).group()
        writtenby = check[:-8]
        matches["Written By"] = writtenby

    if re.search(r"License Number:\s[-,():/\w\s]+Adjuster", dfd):
        check = re.search(r"License Number:\s[-,():/\w\s]+Adjuster", dfd).group()
        licensenumber = check[:-9]
        matches["License Number"] = licensenumber

    if re.search(r"Adjuster:\s[-,()\w\s]+Insured", dfd):
        check = re.search(r"Adjuster:\s[-,()\w\s]+Insured", dfd).group()
        adjuster = check[:-8]
        matches["Adjuster"] = adjuster

    if re.search(r"Insured:\s[-,\w\s]+(Owner Po|Policy)\s#:", dfd):
        check = re.search(r"Insured:\s[-,\w\s]+(Owner Policy|Policy)\s#:", dfd).group()
        if check[-15:] =="Owner Policy #:":
            insured = check[:-15]
        else:
            insured = check[:-9]
        matches["Insured"] = insured

    if re.search(r"Customer:\s[-,\w\s]+(Owner Po|Policy)\s#:", dfd):
        check = re.search(r"Customer:\s[-,\w\s]+(Owner Policy|Policy)\s#", dfd).group()
        if check[-15:] =="Owner Policy #:":
            custo = check[:-15]
        else:
            custo = check[:-9]
        matches["Customer"] = custo

    if re.search(r"(Owner Policy|Policy)\s#:\s[-\w\s]+Claim", dfd):
        check = re.search(r"(Owner Policy|Policy)\s#:\s[-\w\s]+Claim", dfd).group()
        policy = check[:-6]
        matches["Policy"] = policy

    if re.search(r"Claim #:\s[-\w\s]+Type of", dfd):
        check = re.search(r"Claim #:\s[-\w\s]+Type of", dfd).group()
        claim = check[:-8]
        matches["Claim"] = claim

    if re.search(r"Type of Loss:\s\s\s[-,/:;\s\w]+Date", dfd):
        check = re.search(r"Type of Loss:\s[-,/:;\s\w]+Date", dfd).group()
        typeofloss = check[:-7]
        matches["Type Of Loss"] = typeofloss

    if re.search(r"Date[\w\s]+:\s[-,/:;\s\w]+Days", dfd):
        check = re.search(r"Date[\w\s]+:\s[-,/:;\s\w]+Days", dfd).group()
        date = check[:-5]
        matches["Date Of Loss"] = date

    if re.search(r"Days[\w\s]+:\s[-,/:;\s\w]+Point", dfd):
        check = re.search(r"Days[\w\s]+:\s[-,/:;\s\w]+Point", dfd).group()
        days = check[:-6]
        matches["Days To Repair"] = days

    if re.search(r"Point[\w\s]+:\s[-,/:();\s\w]+(Deduc|Owner)", dfd):
        check = re.search(r"Point[\w\s]+:\s[-,/:();\s\w]+(Deduc|Owner)", dfd).group()
        point = check[:-6]
        matches["Point of Impact"] = point

    if re.search(r"Deductible:[\.\d\s]+Owner", dfd):
        check = re.search(r"Deductible:[\.\d\s]+Owner", dfd).group()
        deduc = check[:-6]
        matches["Deductible"] = deduc

    if re.search(r"Owner:\s[-,/:;\s\w]+Job Number", dfd):
        check = re.search(r"Owner:\s[-,/:;\s\w]+Job Number", dfd).group()
        owner = check[:-11]
        matches["Owner"] = owner

    if re.search(r"VIN:\s\s\s[-,/:;\w]+\s\s\s", dfd):
        check = re.search(r"VIN:\s\s\s[-,/:;\w]+\s\s\s", dfd).group()
        vin = check[:-3]
        matches["VIN"] = vin

    if re.search(r"State:\s\s\s[-,/:;\s\w]+\s\s\s", dfd):
        check = re.search(r"State:\s\s\s[-,/:;\w]+\s\s\s", dfd).group()
        state = check[:-3]
        matches["State"] = state

    if re.search(r"Odometer:\s\s\s[-,/:;\s\w]+\s\s\s", dfd):
        check = re.search(r"Odometer:\s\s\s[-,/:;\w]+\s\s\s", dfd).group()
        odometer = check[:-3]
        matches["Odometer"] =odometer

    if re.search(r"Condition:\s\s\s[-,/:;\s\w]+\s\s\s", dfd) and not re.search(r"Condition:\s\s\sTransmission", dfd):
        check = re.search(r"Condition:\s\s\s[-,/:;\w]+\s\s\s", dfd).group()
        condition = check[:-3]
        matches["Condition"] = condition

    if re.search(r"Mileage In:\s\s\s[,\d]+\s\s\s", dfd):
        check = re.search(r"Mileage In:\s\s\s[,\d]+\s\s\s", dfd).group()
        mileagein = check[:-3]
        matches["Mileage_In"] = mileagein

    if re.search(r"Mileage Out:\s\s\s[,\d]+\s\s\s", dfd):
        check = re.search(r"Mileage Out:\s\s\s[,\d]+\s\s\s", dfd).group()
        mileageout = check[:-3]
        matches["Mileage_Out"] = mileageout

    if re.search(r"Exterior Color:\s\s\s([-,/:;\w]+|[-,/:;\w]+\s[-,/:;\w]+)\s\s\s", dfd):
        check = re.search(r"Exterior Color:\s\s\s([-,/:;\w]+|[-,/:;\w]+\s[-,/:;\w]+)\s\s\s", dfd).group()
        exterior = check[:-3]
        matches["Exterior Color"] = exterior

    if re.search(r"Interior Color:\s\s\s[-,/:;\s\w]+\s\s\s", dfd) and not re.search(r"Interior Color:\s\s\sLicense", dfd):
        check = re.search(r"Interior Color:\s\s\s[-,/:;\w]+\s\s\s", dfd).group()
        interior = check[:-3]
        matches["Interior Color"] = interior

    if re.search(r"License:\s\s\s([-,/:;\w]+|[-,/:;\w]+\s[-,/:;\w]+)\s\s\s", dfd):
        check = re.search(r"License:\s\s\s([-,/:;\w]+|[-,/:;\w]+\s[-,/:;\w]+)\s\s\s", dfd).group()
        licence = check[:-3]
        matches["License"] = licence

    if re.search(r"Production Date:\s\s\s[-,/:;\d]+\s\s\s", dfd):
        check = re.search(r"Production Date:\s\s\s[-,/:;\d]+\s\s\s", dfd).group()
        production = check[:-3]
        matches["Production"] = production

    if re.search(r"VEHICLE[-,/.;\w\s]+(VIN|Yea)", dfd):
        check = re.search(r"VEHICLE[-,/.;\w\s]+(VIN|Yea)", dfd).group()
        vehicle_detail = check[8:-4]
        matches["vehicle_detail"] = vehicle_detail

    if re.search(r"Make:\s\s\s[-,/:;\w]+\s\s\s", dfd):
        check = re.search(r"Make:\s\s\s[-,/:;\w]+\s\s\s", dfd).group()
        make = check[0:-3]
        matches["make"] = make

    if re.search(r"Year:\s\s\s[-,/:;\d]+\s\s\s", dfd):
        check = re.search(r"Year:\s\s\s[-,/:;\d]+\s\s\s", dfd).group()
        year = check[0:-3]
        matches["year"] = year

    if re.search(r"Model:\s\s\s[-,/;\w\s]+\s\s\s", dfd):
        check = re.search(r"Model:\s\s\s[-,/;\w\s]+\s\s\s", dfd).group()
        model = check[0:-3]
        matches["model"] = model

    if re.search(r"Engine:\s\s\s[-,/.;\w\s]+\s\s\s", dfd):
        check = re.search(r"Engine:\s\s\s[-,/.;\w\s]+\s\s\s", dfd).group()
        engine = check[0:-3]
        matches["engine"] = engine

    if re.search(r"Body Style:\s\s\s[-,/.;\w\s]+\s\s\s", dfd):
        check = re.search(r"Body Style:\s\s\s[-,/.;\w\s]+\s\s\s", dfd).group()
        body_style = check[0:-3]
        matches["body_style"] = body_style

    return matches

def table_first(dfd):
    table_list = []
    table_dict = {1:[],2:[],3:[],4:[]}
    table_data = re.search(r"TRANSMISSION[-();/\"\'\w\s]+[/:\d]+\s(PM|AM)", dfd[0]).group()
    table_li = table_data.split("\n")
    table_li.pop(-1)
    for g in table_li:
        if g[0] == " ":
            g = g.strip()
        table_sub = re.sub(r"\s{2,}", "\\\s\\\s\\\s", g)
        table_list.append(table_sub)
    for e in table_list:
        tb = e.split("\s\s\s")
        if len(tb) == 4:
            table_dict[1].append(tb[0])
            table_dict[2].append(tb[1])
            table_dict[3].append(tb[2])
            table_dict[4].append(tb[3])
        elif len(tb) == 3:
            table_dict[1].append(tb[0])
            table_dict[2].append(tb[1])
            table_dict[3].append(tb[2])
    return table_dict

def information(dfd):
    leng_lis = []
    pos_dict = {0:"", 1:"", 2:"", 3:""}
    pos = re.search(r"Owner[-:,/()|@.\s\w\t]+(Vehicle|VEHICLE)", dfd[0]).group()
    pos = pos[:-9]
    # pos = re.sub(r"[' ']{4,}", "\t", pos)
    pos_lis = pos.split("\n")
    pos_lis = pos_lis[:-1]
    pos1 = re.sub(r"[' ']{3,}", "\t", pos_lis[0])
    pos2 = pos1.split("\t")
    for e in pos_lis:
        leng_lis.append(len(e))
    length = max(leng_lis)
    for i in range(len(pos_lis)):
        if len(pos_lis[i]) < length:
            diff = length-len(pos_lis[i])
            pos_lis[i] = pos_lis[i] + " "*diff
    if len(pos2) == 4:
        for e in pos_lis:
            # import pdb; pdb.set_trace()
            if re.search(r"(\(|\w)[-().:,#@\s\w]+\s{3,}", e[:26]):
                ow = re.search(r"(\(|\w)[-().:,#@\s\w]+(:|\w)", e[:26]).group()
                ow = ow.strip()
                ow = ow + " "
                pos_dict[0] = pos_dict[0] + ow
            if re.search(r"(\(|\w)[-().:,#@\s\w]+\s{3,}", e[28:63]):
                il = re.search(r"(\(|\w)[-().:,#@\s\w]+(:|\w)", e[28:63]).group()
                il = il.strip()
                il = il + " "
                pos_dict[1] = pos_dict[1] + il
            if re.search(r"(\(|\w)[-().:,#@\s\w]+\s{3,}", e[65:108]):
                ai = re.search(r"(\(|\w)[-().:,#@\s\w]+(:|\w)", e[65:108]).group()
                ai = ai.strip()
                ai = ai + " "
                pos_dict[2] = pos_dict[2] + ai
            if re.search(r"(\(|\w)[-().:,#@\s\w]+", e[110:]):
                rf = re.search(r"(\(|\w)[-().:,#@\s\w]+(:|\w)", e[110:]).group()
                rf = rf.strip()
                rf = rf + " "
                pos_dict[3] = pos_dict[3] + rf
    elif len(pos2) == 3:
        for e in pos_lis:
            # import pdb; pdb.set_trace()
            if re.search(r"(\(|\w)[-().:,#@\s\w]+\s{3,}", e[:40]):
                ow = re.search(r"(\(|\w)[-().:,#@\s\w]+(:|\w)", e[:40]).group()
                ow = ow.strip()
                ow = ow + " "
                pos_dict[0] = pos_dict[0] + ow
            if re.search(r"(\(|\w)[-().:,#@\s\w]+\s{3,}", e[42:90]):
                il = re.search(r"(\(|\w)[-().:,#@\s\w]+(:|\w)", e[42:90]).group()
                il = il.strip()
                il = il + " "
                pos_dict[1] = pos_dict[1] + il
            if re.search(r"(\(|\w)[-().:,#@\s\w]+", e[100:]):
                rf = re.search(r"(\(|\w)[-().:,#@\s\w]+(:|\w)", e[100:]).group()
                rf = rf.strip()
                rf = rf + " "
                pos_dict[3] = pos_dict[3] + rf

    return pos_dict


    # pos_lis.pop(-1)
    # fir_line = pos_lis[0].split("\t")
    # if len(fir_line) == 3:
    #     pos_dict = {0: "", 1: "", 2: ""}
    # elif len(fir_line) == 4:
    #     pos_dict = {0: "", 1: "", 2: "", 3: ""}
    # for e in pos_lis:
    #     pos_split = e.split("\t")
    #     for f in range(len(pos_split)):
    #         pos_dict[f] = pos_dict[f]+pos_split[f]
    #         pos_dict[f] = pos_dict[f] + " "
    # # import pdb; pdb.set_trace()

    # return pos_dict

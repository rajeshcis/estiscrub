import pandas as pd
import camelot
import csv
# file = "/media/aman/698F6E1531797531/Data/rajeshcis-estiscrub-7fa744c289f6/PDF_data/8899 CAWLEY oe.pdf"
# file = "PDF_data/7759 westerlind original.pdf"
# file = "PDF_data/8559 COTE s1.pdf"
# file = "/media/aman/698F6E1531797531/Data/rajeshcis-estiscrub-7fa744c289f6/PDF_data/8190 ERMANI oe.pdf"
# file = "/media/aman/698F6E1531797531/Data/rajeshcis-estiscrub-7fa744c289f6/PDF_data/8328 MONTANEZ s1.pdf"

def tab1(tables):
        for e in tables:
            # import pdb; pdb.set_trace()
            for f in e.df.iterrows():
                try:
                    if "Line" in f[1].any() or "Line" in e.df.any() or "REAR BUMPER" in f[1].any():
                        return e
                except:
                    pass
                try:
                    if "Line" in e.df:
                        return e
                except:
                    pass

def tab2(tables):
    for e in tables:
        for f in e.df.iterrows():
            try:
                if "ESTIMATE TOTALS" in f[1].any() or "ESTIMATETOTALS" in f[1].any() or "ESTIMATE TOTALS" in e.df:
                    return e
            except:
                pass
            try:
                if "ESTIMATE TOTALS" in e.df:
                    return e
            except:
                pass
                

def table_extract(file):
    tables = camelot.read_pdf(file, flavor='stream', pages='1,2,3,4')
    fil = file.split("/")[-1].split(".")[0]
    ab_tab = tab1(tables)
    # ab_tab.to_csv(f'static/build_csv/{fil}_table_1.csv')
    ab = ab_tab.df
    import pdb; pdb.set_trace()
    est_old_tab = tab2(tables)
    # est_old_tab.to_csv(f'static/build_csv/{fil}_table_2.csv')
    est_old = est_old_tab.df
    for e in est_old.iterrows():
        try:
            if flag:
                est = est.transpose()
                est = pd.concat([est, e[1].transpose()], axis=1, ignore_index=True)
                est = est.transpose()
        except:
            if "ESTIMATE TOTALS" in e[1].any() or "ESTIMATETOTALS" in e[1].any():
                flag = True
                est = e[1]


    flag = False
    if "ESTIMATE TOTALS" in est.iloc[0].any() or "ESTIMATETOTALS" in est.iloc[0].any():
        colom = est.iloc[1]
        est = est.drop([0,1])
        est.columns = colom
    elif "ESTIMATE TOTALS" in est.iloc[1].any() or "ESTIMATETOTALS" in est.iloc[1].any():
        colom = est.iloc[2]
        est = est.drop([0,1,2])
        est.columns = colom
    elif "ESTIMATE TOTALS" in est.iloc[2].any() or "ESTIMATETOTALS" in est.iloc[2].any():
        colom = est.iloc[3]
        est = est.drop([0,1, 2, 3])
        est.columns = colom


    cols = list(ab.columns)

    # ab = tables[2].df

    if "Line" in ab.iloc[0, 0]:
        cols = [col for col in ab.iloc[0]]
        ab = ab.drop([0, 1])
        a = cols.index("Line")
        b = cols.index("Oper")
        if b-a == 2:
            cd = cols.pop(a+1)
            cols.insert(a+1, "Heading")
        elif b-a == 3:
            cd = cols.pop(a+1)
            cols.insert(a+1, "Heading")
            _ = cols.pop(a+2)
            ab.iloc[:, a+1] = ab.iloc[:, a+1] + ab.iloc[:, a+2]
            ab = ab.drop([a+2], axis=1)

        qt = cols.index("Qty")
        cols.remove("Qty")
        cols.insert(qt+1, "Qty")

        ab.columns = cols

    elif "Line" in ab.iloc[1, 0]:
        cols = [col for col in ab.iloc[1]]
        ab = ab.drop([0, 1, 2])
        a = cols.index("Line")
        b = cols.index("Oper")
        if b-a == 2:
            cd = cols.pop(a+1)
            cols.insert(a+1, "Heading")
        elif b-a == 3:
            cd = cols.pop(a+1)
            cols.insert(a+1, "Heading")
            _ = cols.pop(a+2)
            ab.iloc[:, a+1] = ab.iloc[:, a+1] + ab.iloc[:, a+2]
            ab = ab.drop([a+2], axis=1)

        qt = cols.index("Qty")
        cols.remove("Qty")
        cols.insert(qt+1, "Qty")

        ab.columns = cols

    elif "" in ab.iloc[0]:
        ab = ab.drop([0])

    diction = {}
    for e in cols:
        diction[e] = {}

    for e in cols:
        for i, f in enumerate(ab[e]):
            if "Note:" in f:
                continue
            diction[e][i] = f

    est_dic = {}
    for i, e in enumerate(est.iterrows()):
        est_dic[i] = {}

    if est.shape != (0, 0):
        for i, e in enumerate(est.iterrows()):
            # import pdb; pdb.set_trace()
            for f in e[1:]:
                for r, s in zip(est.columns, f):
                    if "\n" in r:
                        r = r.split("\n")
                        s = s.split("\n")
                        for t in s:
                            if "/hr" in t:
                                # import pdb; pdb.set_trace()
                                est_dic[i][r[0]] = t
                            else:
                                est_dic[i][r[1]] = t
                        est_dic[i]["Basis"] = est_dic[i].pop("")
                        if "Page " in s[0]:
                            _ = est_dic.pop(i)
                    else:
                        est_dic[i][r] = s
    return diction, est_dic




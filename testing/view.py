#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 19:22:20 2020

@author: aman
"""
import re
import numpy as np
import random
import spacy
import pdftotext
import json
import csv
from newfile import  table_extract
from checking_file import extracting, match_func, information, table_first

class UploadPdfView():
    def __init__(self, upl_file_obj, upl_file):
        self.upl_fl = upl_file
        self.upl_fl_obj = upl_file_obj
    
    def matcher_data(self, p_file):
        # import pdb; pdb.set_trace()
        # pdf = pdftotext.PDF(p_file)
        pdf = p_file
        df = re.sub(r"^\s+", "", pdf[0], flags=re.MULTILINE)
        df = re.sub("\s{2,}", "   ", df, flags=re.MULTILINE)
        df = df.replace('\n', '   ')
        return df

    def matching(self, file_upload):
        dfd = self.matcher_data(file_upload)
        matc = match_func(dfd)
        return matc

    def info(self, p_file):
        dic_info = {}
        pos_dict = information(p_file)
        # dic_info["owner"] = pos_dict[0]
        # dic_info["Inspection Location"] = pos_dict[1]
        # if "Appraiser Information:" in p_file[0]:
        #     dic_info["Appraiser Information"] = pos_dict[2]
        #     dic_info["Repair Facility"] = pos_dict[3]
        # else:
        #     dic_info["Repair Facility"] = pos_dict[2]
        return pos_dict

    def handle_uploaded_file(self, p_file):
        df = ""
        cla = ""
        pdf = pdftotext.PDF(p_file)
        df = re.sub(r"^\s+", "", pdf[0], flags=re.MULTILINE)
        # df = df.replace('\n', ' ')
        df = re.sub("\s{2,}", " ", df, flags=re.MULTILINE)
        df = df.replace('\n', ' ')
        wfid = re.search(r"\bWorkfile ID: \w{8}\b", df).span()
        wfid_t = df[wfid[0]:wfid[1]]
        df = re.sub(r"\bWorkfile ID: \w{8}\b", "", df)
        if re.search(r"\bClaim #: [-\w]+\b ", df[:wfid[0]]):
            cla = re.search(r"\bClaim #: [-\w]+\b ", df[:wfid[0]]).group()
            df = re.sub(r"\b Claim #: [-\w]+\b", "", df)
        if re.search(r"\bFederal ID: [-\w]+\b ", df[:275]):
            df = re.sub(r" Federal ID: [-\w]+", "", df)
        if re.search(r"\bLicense Number: [-\w]+\b ", df[:275]):
            df = re.sub(r" License Number: \w\w\s\d\d\d\d", "", df)
        df = wfid_t + " " + df

        return df, pdf, cla

    def extract_data(self, data, cla, model="final_model", new_model_name="animal", output_dir="final_model", n_iter=1000):
        # LABEL = [ "Workfile_id", "Estimate Type", "InsCo", "IndApprCo"]
        # aa, bb, cc, dd = 0, 0, 0, 0
        # ext = {"InsCo" : 0, "Estimate Type" : 0, "Workfile_id" : 0, "IndApprCo" : 0}
        # random.seed(0)
        # if model is not None:
        #     nlp = spacy.load(model)
        #     print("Loaded model '%s'" % model)
        # else:
        #     nlp = spacy.blank("en")
        #     print("Created blank 'en' model")

        # if "ner" not in nlp.pipe_names:
        #     ner = nlp.create_pipe("ner")
        #     nlp.add_pipe(ner)
        # else:
        #     ner = nlp.get_pipe("ner")

        # for label in LABEL:
        #     ner.add_label(label)

        # ner.add_label("POBOX")
        # if model is None:
        #     optimizer = nlp.begin_training()
        # else:
        #     optimizer = nlp.resume_training()

        # move_names = list(ner.move_names)
        # pipe_exceptions = ["ner", "trf_wordpiecer", "trf_tok2vec"]
        # other_pipes = [pipe for pipe in nlp.pipe_names if pipe not in pipe_exceptions]

        ext = extracting(data, cla)
        # test_text = data
        return ext
    def build_csv(self, ext_data, lis, info_dic, table, file_name):
        fil = file_name.split("/")[-1].split(".")[0]
        with open(f"static/build_csv/{fil}.csv", 'w', newline="") as csv_file:
            writer = csv.writer(csv_file)
            for key, value in ext_data.items():
                writer.writerow([key, value])
            for key, value in lis.items():
                if ": " in value:
                    frnt, bck = value.split(": ")
                    writer.writerow([frnt, bck])
                else:
                    writer.writerow([value])
            for key, value in info_dic.items():
                if ": " in value:
                    frnt, bck = value.split(": ")
                    writer.writerow([frnt, bck])
                else:
                    writer.writerow([value])
            for key, value in table.items():
                if ": " in value:
                    frnt, bck = value.split(": ")
                    writer.writerow([frnt, bck])
                else:
                    writer.writerow([value])


    def post(self):
        # form = UploadPdfForm(request.POST, request.FILES)
        # if form.is_valid():
        # estimation = form.save(commit=False)
        # estimation.user = request.user
        # uploaded_file = request.FILES["upload_pdf"]
        # import pdb; pdb.set_trace()
        data, pdf_data, cla = self.handle_uploaded_file(self.upl_fl)
        ext_data = self.extract_data(data, cla)
        lis = self.matching(pdf_data)
        table = table_first(pdf_data)
        info_dic = self.info(pdf_data)
        #To get Data, Header Data is in ext_data and Other data is in lis
        # if request.POST.get('estimate'):
        #     if not request.POST.get('estimate_type') and request.POST.get('estimate'):
        #         estimation.estimate_type = request.POST.get('estimate')
        # else:
        #     if not request.POST.get('estimate_type') and ext_data["Estimate Type"] != "":
        #         estimation.estimate_type = ext_data["Estimate Type"]
        # if request.POST.get('company'):
        #     if not request.POST.get('insurance_company') and request.POST.get('company'):
        #         estimation.insurance_company = request.POST.get('company')
        # else:
        #     if not request.POST.get('insurance_company') and ext_data["InsCo"]:
        #         estimation.insurance_company = ext_data["InsCo"]
        # if request.POST.get('appraisal'):
        #     if not request.POST.get('insurance_appraisal') and request.POST.get('appraisal'):
        #         estimation.insurance_appraisal = request.POST.get('appraisal')
        # else:
        #     if not request.POST.get('insurance_appraisal') and ext_data["IndApprCo"]:
        #         estimation.insurance_appraisal = ext_data["IndApprCo"]
        # if not request.POST.get('workfile_id') and ext_data["Workfile_id"] != "":
        #     estimation.workfile_id = ext_data["Workfile_id"]
        #     # estimation.workfile_id = ext_data["Workfile_id"][13:]
        # # import pdb; pdb.set_trace()
        # print(lis)
        # print(table)
        # print(info_dic)

        # estimation.jobnumber = lis.get('Job Number')
        # estimation.writtenby = lis.get('Written By')
        # estimation.license_number = lis.get('License Number')
        # estimation.adjuster = lis.get('Adjuster')
        # estimation.typeofloss = lis.get('Type Of Loss')
        # estimation.dateofloss = lis.get('Date Of Loss')
        # estimation.dayofrepair = lis.get('Days To Repair')
        # estimation.pointofimpact = lis.get('Point of Impact')
        # estimation.vin = lis.get('VIN')
        # estimation.state = lis.get('State')
        # estimation.odometer = lis.get('Odometer')
        # estimation.license = lis.get('License')
        # estimation.production_date = lis.get('Production')
        # estimation.vehicle_detail = lis.get('vehicle_detail')
        # estimation.insured = lis.get('Insured')
        # estimation.deductible = lis.get('Deductible')
        # estimation.claim = lis.get('Claim')
        # estimation.policy = lis.get('Policy')
        # estimation.condition = lis.get('Condition')
        # estimation.exteriorcolor = lis.get('Exterior Color')
        # estimation.interiorcolor = lis.get('Interior Color')
        # estimation.mileagein = lis.get('Mileage_In')
        # estimation.mileageout = lis.get('Mileage_Out')
        # estimation.owner = lis.get('Owner')
        # estimation.vehicle_parts = json.dumps(table)
        # estimation.insured_locations = json.dumps(info_dic)
        # estimation.save()
        # import pdb; pdb.set_trace()
        table1, table2 = table_extract(self.upl_fl_obj)
        # import pdb; pdb.set_trace()
        # self.build_csv(ext_data, lis, info_dic, table, estimation.upload_pdf.path)
        # messages.success(request, 'Pdf successfully uploaded')

        return ext_data, lis, info_dic, table, table1, table2
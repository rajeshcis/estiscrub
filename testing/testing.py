#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 19:31:48 2020

@author: aman
"""
import json
import os
import pdftotext
import re
from view import UploadPdfView

files_list = os.listdir("CCC_files")
f_name = []
total = {}
for f in files_list:
    with open("/media/aman/698F6E1531797531/Data/testing/CCC_files/8063 VU s1.pdf", 'rb') as file:
        file_obj = "/media/aman/698F6E1531797531/Data/testing/CCC_files/8063 VU s1.pdf"
        try:
            ul_pdf = UploadPdfView(file_obj, file)
            ext_data, lis, info_dic, table, table1, table2 = ul_pdf.post()
            total[f] = {"ext_data": ext_data, "lis":lis, "info_dic":info_dic, "table":table, "table1":table1, "table2":table2}
            f_name.append(f)
            print("Sucess")
        except:
            print("Fail")

with open("testing_fi.json", "a") as e:
    for ke, va in total.items():
        abc = json.dumps([{"File Name": ke, k:v} for k,v in va.items()], indent=4)
        e.writelines(abc)
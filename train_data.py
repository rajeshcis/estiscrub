#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 15:38:55 2020

@author: cis
"""


LABEL = ["InsCo", "IndApprCo", "Estimate Type", "Workfile_id"]

# training data
# Note: If you're using an existing model, make sure to mix in examples of
# other entity types that spaCy correctly recognized before. Otherwise, your
# model might learn the new type, but "forget" what it previously knew.
# https://explosion.ai/blog/pseudo-rehearsal-catastrophic-forgetting
TRAIN_DATA = [
   (
         'COMMERCE INSURANCE MAPFRE | Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0NPZ2R-KRAHA5 Webster, MA 01570 Workfile ID: 8aeae0ce Estimate of Record Written By: PAUL GEOTIS, License Number: 2685, 11/29/2016 6:36:12 AM',
         {"entities": [(0, 25, LABEL[0]), (172, 193, LABEL[2]), (194, 212, LABEL[1])]},
    ),
    (
         "GEICO GEICO MA/RI Request a Supplement at Partners.Geico.com Visit Us Online At GEICO.COM 300 Crosspoint Parkway Getzville, NY 14068 Phone: (508) 958-1843 Claim #: 0666364240000001-01 Fax: (516) 213-7460 Workfile ID: 976ec849 Estimate of Record Written By: ROBERT LYNCH, License Number: 016573, 11/1/2019 8:48:45 AM Adjuster: Lynch, Robert, (774) 479-8834 Business Insured:",
         {"entities": [(0, 17, LABEL[0]), (204, 225, LABEL[2]), (226, 315, LABEL[1])]},
    ),
    (
         "TRAVELERS OF MASSACHUSETTS New England Claim Department - 452 FOR SUPPLEMENTS PLEASE CALL (888) 299-7456 PROMPT 2 P.O. Box 1450 Middleboro, Ma, MA 02344 Phone: (800) 422-3340 Claim #: HXQ0772003 Fax: (877) 786-5584 Workfile ID: 08cdf8c5 Estimate of Record Written By: MARK ARIDANO, License Number: 015437, 1/27/2016 6:34:26 PM Adjuster: LESSA, CHRISTOPHER, (508) 946-6383 Business Insured: DONALD KENADEK",
         {"entities": [(0, 26, LABEL[0]), (215, 236, LABEL[2]), (237, 326, LABEL[1])]},
    ),
    (
         "CITIZENS INS CO OF AMERICA The Hanover Insurance Group-01 Worcester Claim Office Office # 800-628-0250 440 Lincoln St. WORCESTER, MA 01653 Phone: (508) 751-2190 Claim #: 15-00-486089-1-1 Fax: (508) 926-4970 Workfile ID: eaf1f6b6 Estimate of Record Written By: JOE STRANIERI, License Number: 006890, 2/12/2015 2:40:51 PM ",
         {"entities": [(0, 26, LABEL[0]), (207, 228, LABEL[2]), (229, 319, LABEL[1])]},
    ),
    (
         "GEICO MA Toll Free 800-841-3000 EMAIL SUPPLEMENTS TO: R8ADSUPPNE@GEICO.COM VISIT US ONLINE AT GEICO.COM 300 Crosspoint Pkwy Claim #: 0380121500101021-01 Getzville, NY 14068 Workfile ID: 994a9b66 Estimate of Record Written By: AIMEE WALIGORA, License Number: 15475, 2/18/2015 2:51:47 PM Adjuster: Waligora, Aimee Insured: John Aleles Owner Policy #: 4174379299 Claim #: 0380121500101021-01 ",
         {"entities": [(0, 8, LABEL[0]), (173, 194, LABEL[2]), (195, 285, LABEL[1])]},
    ),
    (
         "Workfile ID: 95dcf434 290 AUTO BODY INC. Federal ID: 300510528 We do it better. License Number: RS 4614 73 WEST BOYLSTON DRIVE, WORCESTER, MA 01606 Phone: (508) 363-2902 FAX: (508) 340-4977 Preliminary Supplement 1 with Summary Customer: SUAREZ, ANDREA Job Number: 7925 Written By: Justin Forkuo ",
         {"entities": [(0, 21, LABEL[2]), (22, 40, LABEL[0])]},
    ),
    (
         "AMICA MUTUAL INSURANCE COMPANY CENTRAL MASSACHUSETTS OFFICE PO Box 9690 Providence, RI 02940 Phone: (888) 702-6422 Claim #: 60002173657-2-1 Fax: (866) 381-3239 Workfile ID: 204801e3 Estimate of Record Written By: WILLIAM HOMMEL, License Number: 12048, 6/23/2015 9:50:52 AM Adjuster: BICKFORD, KARYN, (888) 702-6422 x46320 Day Insured: Ayla Lari",
         {"entities": [(0, 30, LABEL[0]), (160, 181, LABEL[2]), (182, 272, LABEL[1])]},
    ),
    (
         "USAA 1010 Please visit us @ USAA.com PO Box 33490 San Antonio, TX 78265 Claim #: 019453387000000015001 Phone: (800) 531-8722 Workfile ID: 23de521e Estimate of Record Written By: RAYMOND CHAPIN, License Number: 014258, 5/8/2015 3:34:25 PM Adjuster: USAA, Auto Claims, (800) 531-8722 Business Insured: CPL JUAN PULECIO Owner Policy #: 019453387 Claim #: 019453387000000015001 Type of Loss: Collision Date of Loss: 03/19/2015 12:00 AM Days to Repair: 6 Point of Impact: 04 Right",
         {"entities": [(0, 4, LABEL[0]), (125, 146, LABEL[2]), (147, 237, LABEL[1])]},
    ),
    (
         "CITIZENS INS CO OF AMERICA The Hanover Insurance Group-01 Worcester Claim Office Office # 800-628-0250 440 Lincoln St Worcester, MA 01653 Phone: (508) 450-5372 Claim #: 15-00-563682-1-1 Fax: (508) 635-6043 Workfile ID: 107d2600 Estimate of Record Written By: PETER BERGSTROM, License Number: 014636, 6/8/2015 2:46:52 PM Adjuster: ORTIZ, LAURA, (800) 628-0250",
         {"entities": [(0, 26, LABEL[0]), (206, 228, LABEL[2]), (228, 319, LABEL[1])]},
    ),
    (
         "ALLSTATE INSURANCE COMPANY New England Auto 55 Capital Blvd. 3rd Floor Rocky Hill, CT 06067 Claim #: 000366477222D01 Phone: (800) 726-2235 Workfile ID: 798555ab Estimate of Record Written By: MICHAEL DAVIS, License Number: 02112605, 4/27/2015 1:46:29 PM Adjuster: DAVIS, MICHAEL, (860) 885-4384 Business ",
         {"entities": [(0, 26, LABEL[0]), (139, 160, LABEL[2]), (161, 253, LABEL[1])]},
    ),
    (
         "GEICO GEICO RI/MA EMAIL SUPPLEMENTS TO: R8ADSUPPMARI@GEICO.COM Visit us at geico.com 300 Cross Point Parkway Getzville, NY 14068 Phone: (508) 450-4771 Claim #: 0378840060101055-01 Fax: (855) 705-9782 Workfile ID: 44be3df8 Estimate of Record Written By: TIMOTHY QUAGLIAROLI, License Number: 016049, 4/28/2015 1:58:39 PM Adjuster: Quagliaroli, Tim, (508) 450-4771 Cellular Insured: Katie Tucker Owner Policy ",
         {"entities": [(0, 5, LABEL[0]), (200, 221, LABEL[2]), (222, 318, LABEL[1])]},
    ),
    (
         "COMMERCE INSURANCE MAPFRE | Commerce Insurance For Claims Questions: Call 1-800-221-1605 Appraisal Questions: Contact Appraiser 11 Gore Rd Claim #: A0N0C7G-JAXHC2 Webster, MA 01570 Workfile ID: daa027a0 Estimate of Record Written By: ANTHONY BONAVITA, License Number: 10975, 7/28/2015 3:52:39 PM Adjuster: HOPKINS, JASON, (800) 221-1605 Business Insured: RYAN ",
         {"entities": [(0, 25, LABEL[0]), (181, 202, LABEL[2]), (203, 295, LABEL[1])]},
    ),
    (
         "TRAVELERS Auto Express Claim Center-244 Email Supplements:supplementrequest@travelers. com By Phone: 888-299-7456 PROMPT 2 PO BOX 4947 Orlando, FL 30328 Phone: (877) 411-0768 Claim #: H3R3334001 Fax: (877) 786-5584 Workfile ID: a8f9149f Estimate of Record Written By: JAMES MINEO, 9/13/2018 3:06:59 PM Adjuster: PASTORMERLO, KIETH, (860) 756-9698 Evening Insured: NICHOLAS SPRING Owner Policy #: PT5010A6019811212321 Claim #: H3R3334001 Type of Loss",
         {"entities": [(0, 9, LABEL[0]), #(33, 57, LABEL[2]), (108, 137, LABEL[2]), (139, 146, LABEL[3]),
                       (148, 168, LABEL[4])]},
    ),
    (
         "AMICA MUTUAL INSURANCE COMPANY CENTRAL MASSACHUSETTS OFFICE PO Box 9690 Providence, RI 02940 Phone: (888) 702-6422 Fax: (866) 381-3239",
         {"entities": [(0, 30, LABEL[0]), (31, 59, LABEL[1]), (60, 71, LABEL[9]), (84, 92, LABEL[3]),
                       (93, 114, LABEL[4]), (115, 134, LABEL[5])]},
    ),
    (
         "ARBELLA MUTUAL INSURANCE COMPANY 1100 CROWN COLONY DR PO BOX 699225, QUINCY, MA 02269-9225 (800) 272-3552 Fax: (617) 773-4760",
         {"entities": [(0, 32, LABEL[0]), (33, 53, LABEL[2]), (54, 67, LABEL[9]), (69, 75, LABEL[1]),
                       (77, 90, LABEL[3]), (91, 105, LABEL[4]), (106, 125, LABEL[5])]},
    ),
    (
         "LIBERTY MUTUAL INSURANCE COMPANY Westborough Claim Office RENTAL AND SUPPLEMENT INFORMATION IS LISTED BELOW 114 Turnpike Road Westborough, MA 01581 Phone: (800) 251-7447",
         {"entities": [(0, 32, LABEL[0]), (33, 53, LABEL[2]), (54, 67, LABEL[9]), (69, 75, LABEL[1]),
                       (77, 90, LABEL[3]), (91, 105, LABEL[4]), (106, 125, LABEL[5])]},
    )
]

# LABEL = ["ORG", "CITY", "STREET", "STATE", "PHONE", "FAX", "THEME", "EMAIL", "WEBSITE", "POBOX"]
#         ["0",     "1",     "2",     "3",      "4",   "5",    "6",     "7",      "8",      "9"]
